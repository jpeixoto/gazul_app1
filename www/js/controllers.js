angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicSlideBoxDelegate, $ionicPopup) {

  $scope.nextSlide = function() {
    $ionicSlideBoxDelegate.next();
  };

  $scope.prevSlide = function() {
    $ionicSlideBoxDelegate.previous();
  };

  $scope.class_prev = "disabled";

  $scope.slideChanged_first = function(index) {
    //desabilitar botão quando não tiver mais slides para voltar
    if(index == 0) {
      $scope.class_prev = "disabled";
    } else {
      $scope.class_prev = "enabled";
    }
    //desabilitar botão quando não tiver mais slides para seguir
    if(index == 5) {
      $scope.class_next = "disabled";
    } else {
      $scope.class_next = "enabled";
    }
  };

  $scope.slideChanged_second = function(index) {
    //desabilitar botão quando não tiver mais slides para voltar
    if(index == 0) {
      $scope.class_prev = "disabled";
    } else {
      $scope.class_prev = "enabled";
    }
    //desabilitar botão quando não tiver mais slides para seguir
    if(index == 10) {
      $scope.class_next = "disabled";
    } else {
      $scope.class_next = "enabled";
    }
  };

  $scope.slideChanged_third = function(index) {
    //desabilitar botão quando não tiver mais slides para voltar
    if(index == 0) {
      $scope.class_prev = "disabled";
    } else {
      $scope.class_prev = "enabled";
    }
    //desabilitar botão quando não tiver mais slides para seguir
    if(index == 11) {
      $scope.class_next = "disabled";
    } else {
      $scope.class_next = "enabled";
    }
  };

  $scope.slideChanged_fourth = function(index) {
    //desabilitar botão quando não tiver mais slides para voltar
    if(index == 0) {
      $scope.class_prev = "disabled";
    } else {
      $scope.class_prev = "enabled";
    }
    //desabilitar botão quando não tiver mais slides para seguir
    if(index == 20) {
      $scope.class_next = "disabled";
    } else {
      $scope.class_next = "enabled";
    }
  };

 // Um alert para instruções
 $scope.showAlert = function() {
   var alertPopup = $ionicPopup.alert({
    title: 'Fique Sabendo',
    template: '<b>O que é o Scratch?</b> É uma ferramenta gratuita para a construção de histórias interativas, jogos e animações. Foi criado pelo MIT (Instituto de Tecnologia de Massachusetts) para ensinar programação de forma simples e fácil a crianças e adultos.<br/><br/><b>O que é o Blockly?</b> É uma linguagem de programação visual criada pela Google.'
   });
 };
 
})

.controller('IntroCtrl', function($scope, $ionicSlideBoxDelegate) {
  $scope.nextBalloon = function(hideElement,showElement) {
    angular.element(hideElement).fadeOut();
    angular.element(showElement).fadeIn();
  }
})

