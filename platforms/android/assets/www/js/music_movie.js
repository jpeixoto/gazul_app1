document.addEventListener('deviceready', onDeviceReady, false);

function onDeviceReady() {
	console.log("Music: device ready - carregando musica")
	playMusic();
}

function playMusic() {
	var mp3URL = getMediaUrl("sounds/Destiny_Day.mp3");
	var media = new Media(mp3URL, null, mediaError);
	media.play();
}

function getMediaUrl(src) {
	if (device.platform.toLowerCase() === "android") return "/android_asset/www/" + src;
	return src;
}

function mediaError(error) {
	alert('Media Error');
	alert(JSON.stringify(e));
}
