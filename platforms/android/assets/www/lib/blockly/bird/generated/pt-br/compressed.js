// Automatically generated file.  Do not edit!
'use strict';var g,l=this;function aa(a){a=a.split(".");for(var b=l,c;c=a.shift();)if(null!=b[c])b=b[c];else return null;return b}function ba(){}function ca(a){a.hc=function(){return a.gi?a.gi:a.gi=new a}}
function da(a){var b=typeof a;if("object"==b)if(a){if(a instanceof Array)return"array";if(a instanceof Object)return b;var c=Object.prototype.toString.call(a);if("[object Window]"==c)return"object";if("[object Array]"==c||"number"==typeof a.length&&"undefined"!=typeof a.splice&&"undefined"!=typeof a.propertyIsEnumerable&&!a.propertyIsEnumerable("splice"))return"array";if("[object Function]"==c||"undefined"!=typeof a.call&&"undefined"!=typeof a.propertyIsEnumerable&&!a.propertyIsEnumerable("call"))return"function"}else return"null";
else if("function"==b&&"undefined"==typeof a.call)return"object";return b}function ea(a){return"array"==da(a)}function fa(a){var b=da(a);return"array"==b||"object"==b&&"number"==typeof a.length}function n(a){return"string"==typeof a}function ga(a){return"number"==typeof a}function r(a){return"function"==da(a)}function ha(a){var b=typeof a;return"object"==b&&null!=a||"function"==b}function ia(a){return a[ja]||(a[ja]=++ka)}var ja="closure_uid_"+(1E9*Math.random()>>>0),ka=0;
function la(a,b,c){return a.call.apply(a.bind,arguments)}function ma(a,b,c){if(!a)throw Error();if(2<arguments.length){var d=Array.prototype.slice.call(arguments,2);return function(){var c=Array.prototype.slice.call(arguments);Array.prototype.unshift.apply(c,d);return a.apply(b,c)}}return function(){return a.apply(b,arguments)}}function na(a,b,c){na=Function.prototype.bind&&-1!=Function.prototype.bind.toString().indexOf("native code")?la:ma;return na.apply(null,arguments)}
function oa(a,b){var c=Array.prototype.slice.call(arguments,1);return function(){var b=c.slice();b.push.apply(b,arguments);return a.apply(this,b)}}var pa=Date.now||function(){return+new Date};function s(a,b){function c(){}c.prototype=b.prototype;a.m=b.prototype;a.prototype=new c;a.prototype.constructor=a;a.Ql=function(a,c,f){return b.prototype[c].apply(a,Array.prototype.slice.call(arguments,2))}};function qa(a,b){null!=a&&this.append.apply(this,arguments)}g=qa.prototype;g.aa="";g.set=function(a){this.aa=""+a};g.append=function(a,b,c){this.aa+=a;if(null!=b)for(var d=1;d<arguments.length;d++)this.aa+=arguments[d];return this};g.clear=function(){this.aa=""};g.toString=function(){return this.aa};var ra;function sa(a){if(Error.captureStackTrace)Error.captureStackTrace(this,sa);else{var b=Error().stack;b&&(this.stack=b)}a&&(this.message=String(a))}s(sa,Error);sa.prototype.name="CustomError";function ta(a,b){for(var c=a.split("%s"),d="",e=Array.prototype.slice.call(arguments,1);e.length&&1<c.length;)d+=c.shift()+e.shift();return d+c.join("%s")}function ua(a){return a.replace(/[\t\r\n ]+/g," ").replace(/^[\t\r\n ]+|[\t\r\n ]+$/g,"")}var va=String.prototype.trim?function(a){return a.trim()}:function(a){return a.replace(/^[\s\xa0]+|[\s\xa0]+$/g,"")};function wa(a,b){var c=String(a).toLowerCase(),d=String(b).toLowerCase();return c<d?-1:c==d?0:1}
function xa(a){if(!ya.test(a))return a;-1!=a.indexOf("&")&&(a=a.replace(za,"&amp;"));-1!=a.indexOf("<")&&(a=a.replace(Ba,"&lt;"));-1!=a.indexOf(">")&&(a=a.replace(Ca,"&gt;"));-1!=a.indexOf('"')&&(a=a.replace(Da,"&quot;"));-1!=a.indexOf("'")&&(a=a.replace(Ea,"&#39;"));-1!=a.indexOf("\x00")&&(a=a.replace(Fa,"&#0;"));return a}var za=/&/g,Ba=/</g,Ca=/>/g,Da=/"/g,Ea=/'/g,Fa=/\x00/g,ya=/[\x00&<>"']/;
function Ga(a){var b={"&amp;":"&","&lt;":"<","&gt;":">","&quot;":'"'},c;c=l.document.createElement("div");return a.replace(Ha,function(a,e){var f=b[a];if(f)return f;if("#"==e.charAt(0)){var h=Number("0"+e.substr(1));isNaN(h)||(f=String.fromCharCode(h))}f||(c.innerHTML=a+" ",f=c.firstChild.nodeValue.slice(0,-1));return b[a]=f})}
function Ia(a){return a.replace(/&([^;]+);/g,function(a,c){switch(c){case "amp":return"&";case "lt":return"<";case "gt":return">";case "quot":return'"';default:if("#"==c.charAt(0)){var d=Number("0"+c.substr(1));if(!isNaN(d))return String.fromCharCode(d)}return a}})}var Ha=/&([^;\s<&]+);?/g;function Ja(a,b){return a<b?-1:a>b?1:0};function Ka(a,b){b.unshift(a);sa.call(this,ta.apply(null,b));b.shift()}s(Ka,sa);Ka.prototype.name="AssertionError";function La(a,b){throw new Ka("Failure"+(a?": "+a:""),Array.prototype.slice.call(arguments,1));};function Ma(){this.Vg="";this.kj=Na}Ma.prototype.bd=!0;Ma.prototype.$c=function(){return this.Vg};Ma.prototype.toString=function(){return"Const{"+this.Vg+"}"};function Oa(a){if(a instanceof Ma&&a.constructor===Ma&&a.kj===Na)return a.Vg;La("expected object of type Const, got '"+a+"'");return"type_error:Const"}var Na={};function Pa(){this.pc="";this.ij=Qa}g=Pa.prototype;g.bd=!0;g.$c=function(){return this.pc};g.ng=!0;g.Dd=function(){return 1};g.toString=function(){return"SafeUrl{"+this.pc+"}"};var Qa={};var Ra=Array.prototype,Sa=Ra.indexOf?function(a,b,c){return Ra.indexOf.call(a,b,c)}:function(a,b,c){c=null==c?0:0>c?Math.max(0,a.length+c):c;if(n(a))return n(b)&&1==b.length?a.indexOf(b,c):-1;for(;c<a.length;c++)if(c in a&&a[c]===b)return c;return-1},Ta=Ra.forEach?function(a,b,c){Ra.forEach.call(a,b,c)}:function(a,b,c){for(var d=a.length,e=n(a)?a.split(""):a,f=0;f<d;f++)f in e&&b.call(c,e[f],f,a)},Ua=Ra.filter?function(a,b,c){return Ra.filter.call(a,b,c)}:function(a,b,c){for(var d=a.length,e=[],f=
0,h=n(a)?a.split(""):a,k=0;k<d;k++)if(k in h){var m=h[k];b.call(c,m,k,a)&&(e[f++]=m)}return e},Va=Ra.map?function(a,b,c){return Ra.map.call(a,b,c)}:function(a,b,c){for(var d=a.length,e=Array(d),f=n(a)?a.split(""):a,h=0;h<d;h++)h in f&&(e[h]=b.call(c,f[h],h,a));return e},Wa=Ra.every?function(a,b,c){return Ra.every.call(a,b,c)}:function(a,b,c){for(var d=a.length,e=n(a)?a.split(""):a,f=0;f<d;f++)if(f in e&&!b.call(c,e[f],f,a))return!1;return!0};function Xa(a,b){return 0<=Sa(a,b)}
function Ya(a,b){var c=Sa(a,b),d;(d=0<=c)&&Ra.splice.call(a,c,1);return d}function Za(a){var b=a.length;if(0<b){for(var c=Array(b),d=0;d<b;d++)c[d]=a[d];return c}return[]}function $a(a,b,c,d){Ra.splice.apply(a,ab(arguments,1))}function ab(a,b,c){return 2>=arguments.length?Ra.slice.call(a,b):Ra.slice.call(a,b,c)};function bb(){this.hf="";this.hj=cb}bb.prototype.bd=!0;var cb={};bb.prototype.$c=function(){return this.hf};bb.prototype.toString=function(){return"SafeStyle{"+this.hf+"}"};function db(a){var b=new bb;b.hf=a;return b}var eb=db("");
function fb(a){var b="",c;for(c in a){if(!/^[-_a-zA-Z0-9]+$/.test(c))throw Error("Name allows only [-_a-zA-Z0-9], got: "+c);var d=a[c];null!=d&&(d instanceof Ma?d=Oa(d):gb.test(d)||(La("String value allows only [-.%_!# a-zA-Z0-9], got: "+d),d="zClosurez"),b+=c+":"+d+";")}return b?db(b):eb}var gb=/^[-.%_!# a-zA-Z0-9]+$/;function hb(){this.Eg="";this.lj=ib}g=hb.prototype;g.bd=!0;g.$c=function(){return this.Eg};g.ng=!0;g.Dd=function(){return 1};g.toString=function(){return"TrustedResourceUrl{"+this.Eg+"}"};var ib={};function jb(a,b){for(var c in a)b.call(void 0,a[c],c,a)}var kb="constructor hasOwnProperty isPrototypeOf propertyIsEnumerable toLocaleString toString valueOf".split(" ");function lb(a,b){for(var c,d,e=1;e<arguments.length;e++){d=arguments[e];for(c in d)a[c]=d[c];for(var f=0;f<kb.length;f++)c=kb[f],Object.prototype.hasOwnProperty.call(d,c)&&(a[c]=d[c])}}
function mb(a){var b=arguments.length;if(1==b&&ea(arguments[0]))return mb.apply(null,arguments[0]);for(var c={},d=0;d<b;d++)c[arguments[d]]=!0;return c};var nb=mb("area base br col command embed hr img input keygen link meta param source track wbr".split(" "));function ob(){this.pc="";this.gj=pb;this.Sh=null}g=ob.prototype;g.ng=!0;g.Dd=function(){return this.Sh};g.bd=!0;g.$c=function(){return this.pc};g.toString=function(){return"SafeHtml{"+this.pc+"}"};function qb(a){if(a instanceof ob&&a.constructor===ob&&a.gj===pb)return a.pc;La("expected object of type SafeHtml, got '"+a+"'");return"type_error:SafeHtml"}function rb(a){if(a instanceof ob)return a;var b=null;a.ng&&(b=a.Dd());return sb(xa(a.bd?a.$c():String(a)),b)}
var ub=/^[a-zA-Z0-9-]+$/,vb=mb("action","cite","data","formaction","href","manifest","poster","src"),wb=mb("embed","iframe","link","script","style","template");
function xb(a,b,c){if(!ub.test(a))throw Error("Invalid tag name <"+a+">.");if(a.toLowerCase()in wb)throw Error("Tag name <"+a+"> is not allowed for SafeHtml.");var d=null,e="<"+a;if(b)for(var f in b){if(!ub.test(f))throw Error('Invalid attribute name "'+f+'".');var h=b[f];if(null!=h){var k,m=a;k=f;if(h instanceof Ma)h=Oa(h);else if("style"==k.toLowerCase()){if(!ha(h))throw Error('The "style" attribute requires goog.html.SafeStyle or map of style properties, '+typeof h+" given: "+h);h instanceof bb||
(h=fb(h));h instanceof bb&&h.constructor===bb&&h.hj===cb?h=h.hf:(La("expected object of type SafeStyle, got '"+h+"'"),h="type_error:SafeStyle")}else{if(/^on/i.test(k))throw Error('Attribute "'+k+'" requires goog.string.Const value, "'+h+'" given.');if(k.toLowerCase()in vb)if(h instanceof hb)h instanceof hb&&h.constructor===hb&&h.lj===ib?h=h.Eg:(La("expected object of type TrustedResourceUrl, got '"+h+"'"),h="type_error:TrustedResourceUrl");else if(h instanceof Pa)h instanceof Pa&&h.constructor===
Pa&&h.ij===Qa?h=h.pc:(La("expected object of type SafeUrl, got '"+h+"'"),h="type_error:SafeUrl");else throw Error('Attribute "'+k+'" on tag "'+m+'" requires goog.html.SafeUrl or goog.string.Const value, "'+h+'" given.');}h.bd&&(h=h.$c());k=k+'="'+xa(String(h))+'"';e+=" "+k}}void 0!==c?ea(c)||(c=[c]):c=[];!0===nb[a.toLowerCase()]?e+=">":(d=yb(c),e+=">"+qb(d)+"</"+a+">",d=d.Dd());(a=b&&b.dir)&&(d=/^(ltr|rtl|auto)$/i.test(a)?0:null);return sb(e,d)}
function yb(a){function b(a){ea(a)?Ta(a,b):(a=rb(a),d+=qb(a),a=a.Dd(),0==c?c=a:0!=a&&c!=a&&(c=null))}var c=0,d="";Ta(arguments,b);return sb(d,c)}var pb={};function sb(a,b){var c=new ob;c.pc=a;c.Sh=b;return c}var zb=sb("",0);var Ab={nm:!0};var Bb;a:{var Cb=l.navigator;if(Cb){var Db=Cb.userAgent;if(Db){Bb=Db;break a}}Bb=""}function Eb(a){return-1!=Bb.indexOf(a)};var Fb=Eb("Opera")||Eb("OPR"),v=Eb("Trident")||Eb("MSIE"),Gb=Eb("Gecko")&&-1==Bb.toLowerCase().indexOf("webkit")&&!(Eb("Trident")||Eb("MSIE")),w=-1!=Bb.toLowerCase().indexOf("webkit"),Hb=w&&Eb("Mobile"),Ib=Eb("Macintosh"),Jb=Eb("Android"),Kb=Eb("iPhone")&&!Eb("iPod")&&!Eb("iPad"),Lb=Eb("iPad");function Mb(){var a=l.document;return a?a.documentMode:void 0}
var Nb=function(){var a="",b;if(Fb&&l.opera)return a=l.opera.version,r(a)?a():a;Gb?b=/rv\:([^\);]+)(\)|;)/:v?b=/\b(?:MSIE|rv)[: ]([^\);]+)(\)|;)/:w&&(b=/WebKit\/(\S+)/);b&&(a=(a=b.exec(Bb))?a[1]:"");return v&&(b=Mb(),b>parseFloat(a))?String(b):a}(),Ob={};
function Pb(a){var b;if(!(b=Ob[a])){b=0;for(var c=va(String(Nb)).split("."),d=va(String(a)).split("."),e=Math.max(c.length,d.length),f=0;0==b&&f<e;f++){var h=c[f]||"",k=d[f]||"",m=/(\d*)(\D*)/g,p=/(\d*)(\D*)/g;do{var q=m.exec(h)||["","",""],u=p.exec(k)||["","",""];if(0==q[0].length&&0==u[0].length)break;b=Ja(0==q[1].length?0:parseInt(q[1],10),0==u[1].length?0:parseInt(u[1],10))||Ja(0==q[2].length,0==u[2].length)||Ja(q[2],u[2])}while(0==b)}b=Ob[a]=0<=b}return b}
var Qb=l.document,Rb=Qb&&v?Mb()||("CSS1Compat"==Qb.compatMode?parseInt(Nb,10):5):void 0;function Sb(a,b){this.width=a;this.height=b}g=Sb.prototype;g.clone=function(){return new Sb(this.width,this.height)};g.toString=function(){return"("+this.width+" x "+this.height+")"};g.ii=function(){return!(this.width*this.height)};g.ceil=function(){this.width=Math.ceil(this.width);this.height=Math.ceil(this.height);return this};g.floor=function(){this.width=Math.floor(this.width);this.height=Math.floor(this.height);return this};
g.round=function(){this.width=Math.round(this.width);this.height=Math.round(this.height);return this};g.scale=function(a,b){var c=ga(b)?b:a;this.width*=a;this.height*=c;return this};var Tb=!v||v&&9<=Rb,Ub=!Gb&&!v||v&&v&&9<=Rb||Gb&&Pb("1.9.1"),Vb=v&&!Pb("9");function Wb(a){a%=360;return 0>360*a?a+360:a};function x(a,b){this.x=void 0!==a?a:0;this.y=void 0!==b?b:0}g=x.prototype;g.clone=function(){return new x(this.x,this.y)};g.toString=function(){return"("+this.x+", "+this.y+")"};function Xb(a,b){var c=a.x-b.x,d=a.y-b.y;return Math.sqrt(c*c+d*d)}g.ceil=function(){this.x=Math.ceil(this.x);this.y=Math.ceil(this.y);return this};g.floor=function(){this.x=Math.floor(this.x);this.y=Math.floor(this.y);return this};g.round=function(){this.x=Math.round(this.x);this.y=Math.round(this.y);return this};
g.translate=function(a,b){a instanceof x?(this.x+=a.x,this.y+=a.y):(this.x+=a,ga(b)&&(this.y+=b));return this};g.scale=function(a,b){var c=ga(b)?b:a;this.x*=a;this.y*=c;return this};function Yb(a){return a?new Zb($b(a)):ra||(ra=new Zb)}function ac(a,b){jb(b,function(b,d){"style"==d?a.style.cssText=b:"class"==d?a.className=b:"for"==d?a.htmlFor=b:d in bc?a.setAttribute(bc[d],b):0==d.lastIndexOf("aria-",0)||0==d.lastIndexOf("data-",0)?a.setAttribute(d,b):a[d]=b})}var bc={cellpadding:"cellPadding",cellspacing:"cellSpacing",colspan:"colSpan",frameborder:"frameBorder",height:"height",maxlength:"maxLength",role:"role",rowspan:"rowSpan",type:"type",usemap:"useMap",valign:"vAlign",width:"width"};
function cc(){var a=window.document,a="CSS1Compat"==a.compatMode?a.documentElement:a.body;return new Sb(a.clientWidth,a.clientHeight)}function dc(a,b,c){return ec(document,arguments)}
function ec(a,b){var c=b[0],d=b[1];if(!Tb&&d&&(d.name||d.type)){c=["<",c];d.name&&c.push(' name="',xa(d.name),'"');if(d.type){c.push(' type="',xa(d.type),'"');var e={};lb(e,d);delete e.type;d=e}c.push(">");c=c.join("")}c=a.createElement(c);d&&(n(d)?c.className=d:ea(d)?c.className=d.join(" "):ac(c,d));2<b.length&&fc(a,c,b,2);return c}
function fc(a,b,c,d){function e(c){c&&b.appendChild(n(c)?a.createTextNode(c):c)}for(;d<c.length;d++){var f=c[d];!fa(f)||ha(f)&&0<f.nodeType?e(f):Ta(gc(f)?Za(f):f,e)}}function hc(a){for(var b;b=a.firstChild;)a.removeChild(b)}function ic(a){var b=y.h;b.parentNode&&b.parentNode.insertBefore(a,b)}function z(a){return a&&a.parentNode?a.parentNode.removeChild(a):null}
function jc(a,b){if(a.contains&&1==b.nodeType)return a==b||a.contains(b);if("undefined"!=typeof a.compareDocumentPosition)return a==b||Boolean(a.compareDocumentPosition(b)&16);for(;b&&a!=b;)b=b.parentNode;return b==a}function $b(a){return 9==a.nodeType?a:a.ownerDocument||a.document}var kc={SCRIPT:1,STYLE:1,HEAD:1,IFRAME:1,OBJECT:1},lc={IMG:" ",BR:"\n"};function mc(a){a=a.getAttributeNode("tabindex");return null!=a&&a.specified}function nc(a){a=a.tabIndex;return ga(a)&&0<=a&&32768>a}
function oc(a){var b=[];pc(a,b,!1);return b.join("")}function pc(a,b,c){if(!(a.nodeName in kc))if(3==a.nodeType)c?b.push(String(a.nodeValue).replace(/(\r\n|\r|\n)/g,"")):b.push(a.nodeValue);else if(a.nodeName in lc)b.push(lc[a.nodeName]);else for(a=a.firstChild;a;)pc(a,b,c),a=a.nextSibling}function gc(a){if(a&&"number"==typeof a.length){if(ha(a))return"function"==typeof a.item||"string"==typeof a.item;if(r(a))return"function"==typeof a.item}return!1}
function Zb(a){this.Rb=a||l.document||document}g=Zb.prototype;g.tb=Yb;g.k=function(a){return n(a)?this.Rb.getElementById(a):a};g.I=function(a,b,c){return ec(this.Rb,arguments)};g.createElement=function(a){return this.Rb.createElement(a)};g.createTextNode=function(a){return this.Rb.createTextNode(String(a))};g.appendChild=function(a,b){a.appendChild(b)};g.append=function(a,b){fc($b(a),a,arguments,1)};g.canHaveChildren=function(a){if(1!=a.nodeType)return!1;switch(a.tagName){case "APPLET":case "AREA":case "BASE":case "BR":case "COL":case "COMMAND":case "EMBED":case "FRAME":case "HR":case "IMG":case "INPUT":case "IFRAME":case "ISINDEX":case "KEYGEN":case "LINK":case "NOFRAMES":case "NOSCRIPT":case "META":case "OBJECT":case "PARAM":case "SCRIPT":case "SOURCE":case "STYLE":case "TRACK":case "WBR":return!1}return!0};
g.Ei=hc;g.removeNode=z;g.Dc=function(a){return Ub&&void 0!=a.children?a.children:Ua(a.childNodes,function(a){return 1==a.nodeType})};g.contains=jc;g.kc=function(a){var b;(b="A"==a.tagName||"INPUT"==a.tagName||"TEXTAREA"==a.tagName||"SELECT"==a.tagName||"BUTTON"==a.tagName?!a.disabled&&(!mc(a)||nc(a)):mc(a)&&nc(a))&&v?(a=r(a.getBoundingClientRect)?a.getBoundingClientRect():{height:a.offsetHeight,width:a.offsetWidth},a=null!=a&&0<a.height&&0<a.width):a=b;return a};v&&Pb(8);function qc(a){return a&&a.Cj&&a.Cj===Ab?a.content:String(a).replace(rc,sc)}var tc={"\x00":"&#0;",'"':"&quot;","&":"&amp;","'":"&#39;","<":"&lt;",">":"&gt;","\t":"&#9;","\n":"&#10;","\x0B":"&#11;","\f":"&#12;","\r":"&#13;"," ":"&#32;","-":"&#45;","/":"&#47;","=":"&#61;","`":"&#96;","\u0085":"&#133;","\u00a0":"&#160;","\u2028":"&#8232;","\u2029":"&#8233;"};function sc(a){return tc[a]}var rc=/[\x00\x22\x26\x27\x3c\x3e]/g;var uc={};var vc={},wc,A,xc,yc,zc,Ac;
function Bc(){for(var a=Cc,b=B,c='<div style="display: none"><span id="Games_name">Blockly Games</span><span id="Games_puzzle">Quebra-Cabe\u00e7a</span><span id="Games_maze">Labirinto</span><span id="Games_bird">Bird</span><span id="Games_turtle">Tartaruga</span><span id="Games_movie">Movie</span><span id="Games_pondBasic">Pond</span><span id="Games_pondAdvanced">JS Pond</span><span id="Games_linesOfCode1">You solved this level with 1 line of JavaScript:</span><span id="Games_linesOfCode2">You solved this level with %1 lines of JavaScript:</span><span id="Games_nextLevel">Are you ready for level %1?</span><span id="Games_finalLevel">Are you ready for the next challenge?</span><span id="Games_linkTooltip">Salvar e ligar aos blocos.</span><span id="Games_runTooltip">Run the program you wrote.</span><span id="Games_runProgram">Executar o programa</span><span id="Games_resetTooltip">Stop the program and reset the level.</span><span id="Games_resetProgram">Reiniciar</span><span id="Games_help">Ajuda</span><span id="Games_dialogOk">OK</span><span id="Games_dialogCancel">Cancelar</span><span id="Games_catLogic">L\u00f3gica</span><span id="Games_catLoops">La\u00e7os</span><span id="Games_catMath">Matem\u00e1tica</span><span id="Games_catText">Texto</span><span id="Games_catLists">Listas</span><span id="Games_catColour">Cor</span><span id="Games_catVariables">Vari\u00e1veis</span><span id="Games_catProcedures">Fun\u00e7\u00f5es</span><span id="Games_httpRequestError">Houve um problema com a requisi\u00e7\u00e3o.</span><span id="Games_linkAlert">Compartilhe seus blocos com este link:\n\n%1</span><span id="Games_hashError">Desculpe, \'%1\' n\u00e3o corresponde a um programa salvo.</span><span id="Games_xmlError">N\u00e3o foi poss\u00edvel carregar seu arquivo salvo. Talvez ele tenha sido criado com uma vers\u00e3o diferente do Blockly?</span><span id="Games_listVariable">lista</span><span id="Games_textVariable">texto</span></div><div style="display: none"><span id="Bird_noWorm">does not have worm</span><span id="Bird_heading">heading</span><span id="Bird_noWormTooltip">The condition when the bird has not gotten the worm.</span><span id="Bird_headingTooltip">Move in the direction of the given angle: 0 is to the right, 90 is straight up, etc.</span><span id="Bird_positionTooltip">x and y mark the bird\'s position. When x = 0 the bird is near the left edge, when x = 100 it\'s near the right edge. When y = 0 the bird is at the bottom, when y = 100 it\'s at the top.</span></div><table width="100%"><tr><td><h1>'+('<span id="title">'+
(Dc?'<a href="index.html?lang='+qc(a)+'">':'<a href="./?lang='+qc(a)+'">')+"Desafios</a> : "+qc("Pássaro")+"</span>"),d=" &nbsp; ",e=1;11>e;e++)d+=" "+(e==b?'<span class="level_number level_done" id="level'+qc(e)+'">'+qc(e)+"</span>":10==e?'<a class="level_number" id="level'+qc(e)+'" href="?lang='+qc(a)+"&level="+qc(e)+qc("")+'">'+qc(e)+"</a>":'<a class="level_dot" id="level'+qc(e)+'" href="?lang='+qc(a)+"&level="+qc(e)+qc("")+'"></a>');return c+d+'</h1></td><td class="farSide"><select id="languageMenu"></select>&nbsp;<button id="linkButton" title="Salvar e ligar aos blocos."><img src="lib/blockly/media/1x1.gif" class="link icon21"></button><a id="conteudo" href="index.html">Conteúdo</a></td></tr></table><div id="visualization"><svg xmlns="http://www.w3.org/2000/svg" version="1.1" id="svgBird" width="400px" height="400px"></svg></div><table width="400"><tr><td style="width: 190px;"></td><td><button id="runButton" class="primary" title="Run the program you wrote."><img src="lib/blockly/media/1x1.gif" class="run icon21"> Executar o programa</button><button id="resetButton" class="primary" style="display: none" title="Stop the program and reset the level."><img src="lib/blockly/media/1x1.gif" class="stop icon21"> Reiniciar</button></td></tr></table>'+
('<xml id="toolbox" style="display: none;"><block type="bird_heading"></block>'+(2<=b?'<block type="bird_noWorm"></block>'+(4<=b?'<block type="bird_compare"><field name="OP">LT</field><value name="A"><block type="bird_position"><field name="XY">X</field></block></value><value name="B"><block type="math_number"><field name="NUM">50</field></block></value></block>'+(5<=b?'<block type="bird_compare"><field name="OP">LT</field><value name="A"><block type="bird_position"><field name="XY">Y</field></block></value><value name="B"><block type="math_number"><field name="NUM">50</field></block></value></block>'+
(8<=b?'<block type="bird_and"></block>':""):""):""):"")+"</xml>")+'<div id="blockly"></div><div id="dialogShadow" class="dialogAnimate"></div><div id="dialogBorder"></div><div id="dialog"></div><div id="dialogDone" class="dialogHiddenContent"><div style="font-size: large; margin: 1em;">Congratulations!</div><div id="dialogLinesText" style="font-size: large; margin: 1em;"></div><pre id="containerCode"></pre><div id="dialogDoneText" style="font-size: large; margin: 1em;"></div><div id="dialogDoneButtons" class="farSide" style="padding: 1ex 3ex 0"><button id="doneCancel">Cancelar</button><button id="doneOk" class="secondary">OK</button></div></div><div id="dialogAbort" class="dialogHiddenContent">This level is extremely difficult. Would you like to skip it and go onto the next game? You can always come back later.<div id="dialogAbortButtons" class="farSide" style="padding: 1ex 3ex 0"><button id="abortCancel">Cancelar</button><button id="abortOk" class="secondary">OK</button></div></div><div id="dialogStorage" class="dialogHiddenContent"><div id="containerStorage"></div><div class="farSide" style="padding: 1ex 3ex 0"><button class="secondary" onclick="BlocklyDialogs.hideDialog(true)">OK</button></div></div><div id="dialogHelp" class="dialogHiddenContent">'+
(1==b?'<table><tr><td rowspan=2><img src="lib/blockly/common/help.png"></td><td><div class="farSide"><img src="lib/blockly/bird/help_heading.png" class="mirrorImg" height=27 width=141></div></td></tr><tr><td>Change the heading angle to make the bird get the worm and land in her nest.</td></tr></table>':2==b?'<table><tr><td><img src="lib/blockly/common/help.png"></td><td>Use this block to go in one heading if you have the worm, or a different heading if you don\'t have the worm.</td><td><img src="lib/blockly/bird/help_up.png"></td></tr></table>':
4==b?"<table><tr><td><img src=\"lib/blockly/common/help.png\"></td><td>'x' is your current horizontal position. Use this block to go in one heading if 'x' is less than a number, or a different heading otherwise.</td><td><img src=\"lib/blockly/bird/help_up.png\"></td></tr></table>":5==b?'<table><tr><td><img src="lib/blockly/bird/help_up.png"></td><td>Click the icon to modify the \'if\' block.</td><td><img src="lib/blockly/common/help.png"></td></tr></table>':6==b?"<table><tr><td><img src=\"lib/blockly/bird/help_up.png\"></td><td>This level needs both an 'else if' and an 'else' block.</td><td><img src=\"lib/blockly/common/help.png\"></td></tr></table>":
8==b?'<table><tr><td><img src="lib/blockly/bird/help_up.png"></td><td>The \'and\' block is true only if both its inputs are true.</td><td><img src="lib/blockly/common/help.png"></td></tr></table>':"")+"</div>"+(5==b?'<div id="dialogMutatorHelp" class="dialogHiddenContent"><table><tr><td><img src="lib/blockly/bird/help_mutator.png" class="mirrorImg" height=58 width=107></td><td>Drag an \'else\' block into the \'if\' block.</td></tr></table></div>':"")};/*

 Visual Blocks Editor

 Copyright 2012 Google Inc.
 https://developers.google.com/blockly/

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
var Ec={},C,Fc,Gc,Hc,Ic,Jc,Kc,Lc,Mc,Nc,Oc,Pc,Qc,Rc,Sc;function Tc(a){this.Fi=Object.create(null);if(a){a=a.split(",");for(var b=0;b<a.length;b++)this.Fi[a[b]]=!0}this.reset()}Tc.prototype.reset=function(){this.Rf=Object.create(null);this.Nh=Object.create(null)};Tc.prototype.getName=function(a,b){var c=a.toLowerCase()+"_"+b;if(c in this.Rf)return this.Rf[c];var d=Uc(this,a);return this.Rf[c]=d};
function Uc(a,b){var c;(c=b)?(c=encodeURI(c.replace(/ /g,"_")).replace(/[^\w]/g,"_"),-1!="0123456789".indexOf(c[0])&&(c="my_"+c)):c="unnamed";for(var d="";a.Nh[c+d]||c+d in a.Fi;)d=d?d+1:2;c+=d;a.Nh[c]=!0;return c};/*

 Visual Blocks Editor

 Copyright 2012 Google Inc.
 https://developers.google.com/blockly/

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
function Vc(a){var b;C&&(b=a.Tb().V);var c=dc("xml");a=Wc(a,!0);for(var d=0,e;e=a[d];d++){var f=Xc(e);e=D(e);f.setAttribute("x",C?b-e.x:e.x);f.setAttribute("y",e.y);c.appendChild(f)}return c}
function Xc(a){var b=dc("block");b.setAttribute("type",a.type);b.setAttribute("id",a.id);if(a.Ze){var c=a.Ze();c&&b.appendChild(c)}for(var d=0;c=a.S[d];d++)for(var e=0,f;f=c.Ca[e];e++)if(f.name&&f.pd){var h=dc("field",null,f.ic());h.setAttribute("name",f.name);b.appendChild(h)}a.ta&&(c=dc("comment",null,a.ta.Ea()),c.setAttribute("pinned",a.ta.A()),d=a.ta.Cc(),c.setAttribute("h",d.height),c.setAttribute("w",d.width),b.appendChild(c));d=!1;for(e=0;c=a.S[e];e++){var k;f=!0;5!=c.type&&(h=E(c.p),1==c.type?
(k=dc("value"),d=!0):3==c.type&&(k=dc("statement")),h&&(k.appendChild(Xc(h)),f=!1),k.setAttribute("name",c.name),f||b.appendChild(k))}d&&b.setAttribute("inline",a.Nd);a.isCollapsed()&&b.setAttribute("collapsed",!0);a.disabled&&b.setAttribute("disabled",!0);a.ec&&!F||b.setAttribute("deletable",!1);a.Hb&&!F||b.setAttribute("movable",!1);a.fc&&!F||b.setAttribute("editable",!1);if(a=Yc(a))k=dc("next",null,Xc(a)),b.appendChild(k);return b}function Zc(a){return(new XMLSerializer).serializeToString(a)}
function $c(a){a=(new DOMParser).parseFromString(a,"text/xml");if(!a||!a.firstChild||"xml"!=a.firstChild.nodeName.toLowerCase()||a.firstChild!==a.lastChild)throw"Blockly.Xml.textToDom did not obtain a valid XML tree.";return a.firstChild}function ad(a,b){if(C)var c=a.Tb().V;for(var d=0,e;e=b.childNodes[d];d++)if("block"==e.nodeName.toLowerCase()){var f=bd(a,e),h=parseInt(e.getAttribute("x"),10);e=parseInt(e.getAttribute("y"),10);isNaN(h)||isNaN(e)||f.moveBy(C?c-h:h,e)}}
function bd(a,b,c){var d=null,e=b.getAttribute("type");if(!e)throw"Block type unspecified: \n"+b.outerHTML;var f=b.getAttribute("id");if(c&&f){d=cd(f,a);if(!d)throw"Couldn't get Block with id: "+f;f=d.getParent();d.u&&d.i(!0,!1,!0);d.fill(a,e);d.Ga=f}else d=dd(a,e);d.n||ed(d);(f=b.getAttribute("inline"))&&fd(d,"true"==f);(f=b.getAttribute("disabled"))&&gd(d,"true"==f);(f=b.getAttribute("deletable"))&&hd(d,"true"==f);if(f=b.getAttribute("movable"))d.Hb="true"==f;(f=b.getAttribute("editable"))&&id(d,
"true"==f);for(var h=null,f=0,k;k=b.childNodes[f];f++)if(3!=k.nodeType||!k.data.match(/^\s*$/)){for(var h=null,m=0,p;p=k.childNodes[m];m++)3==p.nodeType&&p.data.match(/^\s*$/)||(h=p);m=k.getAttribute("name");switch(k.nodeName.toLowerCase()){case "mutation":d.Wf&&d.Wf(k);break;case "comment":jd(d,k.textContent);var q=k.getAttribute("pinned");q&&setTimeout(function(){d.ta.N("true"==q)},1);h=parseInt(k.getAttribute("w"),10);k=parseInt(k.getAttribute("h"),10);isNaN(h)||isNaN(k)||d.ta.rc(h,k);break;case "title":case "field":kd(d,
m).bb(k.textContent);break;case "value":case "statement":k=ld(d,m);if(!k)throw"Input "+m+" does not exist in block "+e;if(h&&"block"==h.nodeName.toLowerCase())if(h=bd(a,h,c),h.K)md(k.p,h.K);else if(h.F)md(k.p,h.F);else throw"Child block does not have output or previous statement.";break;case "next":if(h&&"block"==h.nodeName.toLowerCase()){if(!d.D)throw"Next statement does not exist.";if(d.D.t)throw"Next statement is already connected.";h=bd(a,h,c);if(!h.F)throw"Next block does not have previous statement.";
md(d.D,h.F)}}}(a=b.getAttribute("collapsed"))&&d.Vd("true"==a);(a=Yc(d))?a.v():d.v();return d}function nd(a){for(var b=0,c;c=a.childNodes[b];b++)if("next"==c.nodeName.toLowerCase()){a.removeChild(c);break}}window.Blockly||(window.Blockly={});window.Blockly.Xml||(window.Blockly.Xml={});window.Blockly.Xml.domToText=Zc;window.Blockly.Xml.domToWorkspace=ad;window.Blockly.Xml.textToDom=$c;window.Blockly.Xml.workspaceToDom=Vc;/*

 Visual Blocks Editor

 Copyright 2011 Google Inc.
 https://developers.google.com/blockly/

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
function od(a){this.s=a;this.X=null;this.Gd=new pd(a,!0,!0);this.ee=new pd(a,!1,!0);this.vd=H("rect",{height:I,width:I,style:"fill: #fff"},null);qd(this.vd,a.ld)}od.prototype.i=function(){J(this.ff);this.ff=null;z(this.vd);this.X=this.s=this.vd=null;this.Gd.i();this.Gd=null;this.ee.i();this.ee=null};
od.prototype.resize=function(){var a=this.s.Tb();if(a){var b=!1,c=!1;this.X&&this.X.V==a.V&&this.X.za==a.za&&this.X.gb==a.gb&&this.X.fb==a.fb?(this.X&&this.X.Wc==a.Wc&&this.X.Oa==a.Oa&&this.X.Pb==a.Pb||(b=!0),this.X&&this.X.Va==a.Va&&this.X.Lb==a.Lb&&this.X.qb==a.qb||(c=!0)):c=b=!0;b&&this.Gd.resize(a);c&&this.ee.resize(a);this.X&&this.X.V==a.V&&this.X.fb==a.fb||this.vd.setAttribute("x",this.ee.uc);this.X&&this.X.za==a.za&&this.X.gb==a.gb||this.vd.setAttribute("y",this.Gd.yf);this.X=a}};
od.prototype.set=function(a,b){this.Gd.set(a);this.ee.set(b)};function pd(a,b,c){this.s=a;this.gf=c||!1;this.Ka=b;this.Mf();b?(this.jb.setAttribute("height",I),this.ha.setAttribute("height",I-6),this.ha.setAttribute("y",3)):(this.jb.setAttribute("width",I),this.ha.setAttribute("width",I-6),this.ha.setAttribute("x",3));this.ui=K(this.jb,"mousedown",this,this.Gk);this.wi=K(this.ha,"mousedown",this,this.Hk)}var rd,sd,I="ontouchstart"in document.documentElement?25:15;g=pd.prototype;
g.i=function(){this.ef();this.ff&&(J(this.ff),this.ff=null);J(this.ui);this.ui=null;J(this.wi);this.wi=null;z(this.h);this.s=this.ha=this.jb=this.h=null};
g.resize=function(a){if(!a&&(a=this.s.Tb(),!a))return;if(this.Ka){var b=a.V;this.gf?b-=I:this.N(b<a.Va);this.Sa=b/a.Wc;if(-Infinity===this.Sa||Infinity===this.Sa||isNaN(this.Sa))this.Sa=0;var c=a.V*this.Sa,d=(a.Oa-a.Pb)*this.Sa;this.ha.setAttribute("width",Math.max(0,c));this.uc=a.fb;this.gf&&C&&(this.uc+=a.fb+I);this.yf=a.gb+a.za-I;this.h.setAttribute("transform","translate("+this.uc+", "+this.yf+")");this.jb.setAttribute("width",Math.max(0,b));this.ha.setAttribute("x",td(this,d))}else{b=a.za;this.gf?
b-=I:this.N(b<a.Va);this.Sa=b/a.Va;if(-Infinity===this.Sa||Infinity===this.Sa||isNaN(this.Sa))this.Sa=0;c=a.za*this.Sa;d=(a.Lb-a.qb)*this.Sa;this.ha.setAttribute("height",Math.max(0,c));this.uc=a.fb;C||(this.uc+=a.V-I);this.yf=a.gb;this.h.setAttribute("transform","translate("+this.uc+", "+this.yf+")");this.jb.setAttribute("height",Math.max(0,b));this.ha.setAttribute("y",td(this,d))}ud(this)};
g.Mf=function(){this.h=H("g",{},null);this.jb=H("rect",{"class":"blocklyScrollbarBackground"},this.h);var a=Math.floor((I-6)/2);this.ha=H("rect",{"class":"blocklyScrollbarKnob",rx:a,ry:a},this.h);qd(this.h,this.s.ld)};g.A=function(){return"none"!=this.h.getAttribute("display")};g.N=function(a){if(a!=this.A()){if(this.gf)throw"Unable to toggle visibility of paired scrollbars.";a?this.h.setAttribute("display","block"):(this.s.Li({x:0,y:0}),this.h.setAttribute("display","none"))}};
g.Gk=function(a){this.ef();if(!vd(a)){var b=wd(a),b=this.Ka?b.x:b.y,c=xd(this.ha),c=this.Ka?c.x:c.y,d=parseFloat(this.ha.getAttribute(this.Ka?"width":"height")),e=parseFloat(this.ha.getAttribute(this.Ka?"x":"y")),f=.95*d;b<=c?e-=f:b>=c+d&&(e+=f);this.ha.setAttribute(this.Ka?"x":"y",td(this,e));ud(this)}a.stopPropagation()};
g.Hk=function(a){this.ef();vd(a)||(this.$k=parseFloat(this.ha.getAttribute(this.Ka?"x":"y")),this.bl=this.Ka?a.clientX:a.clientY,rd=K(document,"mouseup",this,this.ef),sd=K(document,"mousemove",this,this.Jk));a.stopPropagation()};g.Jk=function(a){this.ha.setAttribute(this.Ka?"x":"y",td(this,this.$k+((this.Ka?a.clientX:a.clientY)-this.bl)));ud(this)};g.ef=function(){yd();zd(!0);rd&&(J(rd),rd=null);sd&&(J(sd),sd=null)};
function td(a,b){if(0>=b||isNaN(b))b=0;else{var c=a.Ka?"width":"height",d=parseFloat(a.jb.getAttribute(c)),c=parseFloat(a.ha.getAttribute(c));b=Math.min(b,d-c)}return b}function ud(a){var b=parseFloat(a.ha.getAttribute(a.Ka?"x":"y")),c=parseFloat(a.jb.getAttribute(a.Ka?"width":"height")),b=b/c;isNaN(b)&&(b=0);c={};a.Ka?c.x=b:c.y=b;a.s.Li(c)}g.set=function(a){this.ha.setAttribute(this.Ka?"x":"y",a*this.Sa);ud(this)};
function qd(a,b){var c=b.nextSibling,d=b.parentNode;if(!d)throw"Reference node has no parent.";c?d.insertBefore(a,c):d.appendChild(a)};function Ad(){0!=Bd&&(Cd[ia(this)]=this);this.xd=this.xd;this.bf=this.bf}var Bd=0,Cd={};Ad.prototype.xd=!1;Ad.prototype.i=function(){if(!this.xd&&(this.xd=!0,this.ba(),0!=Bd)){var a=ia(this);delete Cd[a]}};Ad.prototype.ba=function(){if(this.bf)for(;this.bf.length;)this.bf.shift()()};var Dd="closure_listenable_"+(1E6*Math.random()|0),Ed=0;function Fd(a,b,c,d,e){this.Ic=a;this.jf=null;this.src=b;this.type=c;this.ue=!!d;this.Ke=e;this.key=++Ed;this.hd=this.te=!1}function Gd(a){a.hd=!0;a.Ic=null;a.jf=null;a.src=null;a.Ke=null};function Hd(a){this.src=a;this.La={};this.ce=0}Hd.prototype.add=function(a,b,c,d,e){var f=a.toString();a=this.La[f];a||(a=this.La[f]=[],this.ce++);var h=Id(a,b,d,e);-1<h?(b=a[h],c||(b.te=!1)):(b=new Fd(b,this.src,f,!!d,e),b.te=c,a.push(b));return b};Hd.prototype.remove=function(a,b,c,d){a=a.toString();if(!(a in this.La))return!1;var e=this.La[a];b=Id(e,b,c,d);return-1<b?(Gd(e[b]),Ra.splice.call(e,b,1),0==e.length&&(delete this.La[a],this.ce--),!0):!1};
function Jd(a,b){var c=b.type;if(!(c in a.La))return!1;var d=Ya(a.La[c],b);d&&(Gd(b),0==a.La[c].length&&(delete a.La[c],a.ce--));return d}Hd.prototype.mf=function(a){a=a&&a.toString();var b=0,c;for(c in this.La)if(!a||c==a){for(var d=this.La[c],e=0;e<d.length;e++)++b,Gd(d[e]);delete this.La[c];this.ce--}return b};Hd.prototype.Ed=function(a,b,c,d){a=this.La[a.toString()];var e=-1;a&&(e=Id(a,b,c,d));return-1<e?a[e]:null};
function Id(a,b,c,d){for(var e=0;e<a.length;++e){var f=a[e];if(!f.hd&&f.Ic==b&&f.ue==!!c&&f.Ke==d)return e}return-1};function Kd(a,b){this.type=a;this.currentTarget=this.target=b;this.defaultPrevented=this.Lc=!1;this.Hi=!0}Kd.prototype.ba=function(){};Kd.prototype.i=function(){};Kd.prototype.stopPropagation=function(){this.Lc=!0};Kd.prototype.preventDefault=function(){this.defaultPrevented=!0;this.Hi=!1};var Ld=!v||v&&9<=Rb,Md=!v||v&&9<=Rb,Nd=v&&!Pb("9");!w||Pb("528");Gb&&Pb("1.9b")||v&&Pb("8")||Fb&&Pb("9.5")||w&&Pb("528");Gb&&!Pb("8")||v&&Pb("9");var Od="ontouchstart"in l||!!(l.document&&document.documentElement&&"ontouchstart"in document.documentElement)||!(!l.navigator||!l.navigator.msMaxTouchPoints);function Pd(a){Pd[" "](a);return a}Pd[" "]=ba;function Qd(a,b){Kd.call(this,a?a.type:"");this.relatedTarget=this.currentTarget=this.target=null;this.charCode=this.keyCode=this.button=this.screenY=this.screenX=this.clientY=this.clientX=this.offsetY=this.offsetX=0;this.metaKey=this.shiftKey=this.altKey=this.ctrlKey=!1;this.state=null;this.Ag=!1;this.Sb=null;a&&this.o(a,b)}s(Qd,Kd);var Rd=[1,4,2];
Qd.prototype.o=function(a,b){var c=this.type=a.type;this.target=a.target||a.srcElement;this.currentTarget=b;var d=a.relatedTarget;if(d){if(Gb){var e;a:{try{Pd(d.nodeName);e=!0;break a}catch(f){}e=!1}e||(d=null)}}else"mouseover"==c?d=a.fromElement:"mouseout"==c&&(d=a.toElement);this.relatedTarget=d;this.offsetX=w||void 0!==a.offsetX?a.offsetX:a.layerX;this.offsetY=w||void 0!==a.offsetY?a.offsetY:a.layerY;this.clientX=void 0!==a.clientX?a.clientX:a.pageX;this.clientY=void 0!==a.clientY?a.clientY:a.pageY;
this.screenX=a.screenX||0;this.screenY=a.screenY||0;this.button=a.button;this.keyCode=a.keyCode||0;this.charCode=a.charCode||("keypress"==c?a.keyCode:0);this.ctrlKey=a.ctrlKey;this.altKey=a.altKey;this.shiftKey=a.shiftKey;this.metaKey=a.metaKey;this.Ag=Ib?a.metaKey:a.ctrlKey;this.state=a.state;this.Sb=a;a.defaultPrevented&&this.preventDefault()};function Sd(a){return Ld?0==a.Sb.button:"click"==a.type?!0:!!(a.Sb.button&Rd[0])}
Qd.prototype.stopPropagation=function(){Qd.m.stopPropagation.call(this);this.Sb.stopPropagation?this.Sb.stopPropagation():this.Sb.cancelBubble=!0};Qd.prototype.preventDefault=function(){Qd.m.preventDefault.call(this);var a=this.Sb;if(a.preventDefault)a.preventDefault();else if(a.returnValue=!1,Nd)try{if(a.ctrlKey||112<=a.keyCode&&123>=a.keyCode)a.keyCode=-1}catch(b){}};Qd.prototype.ba=function(){};var Td="closure_lm_"+(1E6*Math.random()|0),Ud={},Vd=0;function Wd(a,b,c,d,e){if(ea(b)){for(var f=0;f<b.length;f++)Wd(a,b[f],c,d,e);return null}c=Xd(c);if(a&&a[Dd])a=a.H(b,c,d,e);else{if(!b)throw Error("Invalid event type");var f=!!d,h=Yd(a);h||(a[Td]=h=new Hd(a));c=h.add(b,c,!1,d,e);c.jf||(d=Zd(),c.jf=d,d.src=a,d.Ic=c,a.addEventListener?a.addEventListener(b.toString(),d,f):a.attachEvent($d(b.toString()),d),Vd++);a=c}return a}
function Zd(){var a=ae,b=Md?function(c){return a.call(b.src,b.Ic,c)}:function(c){c=a.call(b.src,b.Ic,c);if(!c)return c};return b}function be(a,b,c,d,e){if(ea(b))for(var f=0;f<b.length;f++)be(a,b[f],c,d,e);else c=Xd(c),a&&a[Dd]?a.kb(b,c,d,e):a&&(a=Yd(a))&&(b=a.Ed(b,c,!!d,e))&&ce(b)}
function ce(a){if(ga(a)||!a||a.hd)return!1;var b=a.src;if(b&&b[Dd])return Jd(b.gc,a);var c=a.type,d=a.jf;b.removeEventListener?b.removeEventListener(c,d,a.ue):b.detachEvent&&b.detachEvent($d(c),d);Vd--;(c=Yd(b))?(Jd(c,a),0==c.ce&&(c.src=null,b[Td]=null)):Gd(a);return!0}function $d(a){return a in Ud?Ud[a]:Ud[a]="on"+a}function de(a,b,c,d){var e=1;if(a=Yd(a))if(b=a.La[b.toString()])for(b=b.concat(),a=0;a<b.length;a++){var f=b[a];f&&f.ue==c&&!f.hd&&(e&=!1!==ee(f,d))}return Boolean(e)}
function ee(a,b){var c=a.Ic,d=a.Ke||a.src;a.te&&ce(a);return c.call(d,b)}
function ae(a,b){if(a.hd)return!0;if(!Md){var c=b||aa("window.event"),d=new Qd(c,this),e=!0;if(!(0>c.keyCode||void 0!=c.returnValue)){a:{var f=!1;if(0==c.keyCode)try{c.keyCode=-1;break a}catch(h){f=!0}if(f||void 0==c.returnValue)c.returnValue=!0}c=[];for(f=d.currentTarget;f;f=f.parentNode)c.push(f);for(var f=a.type,k=c.length-1;!d.Lc&&0<=k;k--)d.currentTarget=c[k],e&=de(c[k],f,!0,d);for(k=0;!d.Lc&&k<c.length;k++)d.currentTarget=c[k],e&=de(c[k],f,!1,d)}return e}return ee(a,new Qd(b,this))}
function Yd(a){a=a[Td];return a instanceof Hd?a:null}var fe="__closure_events_fn_"+(1E9*Math.random()>>>0);function Xd(a){if(r(a))return a;a[fe]||(a[fe]=function(b){return a.handleEvent(b)});return a[fe]};function ge(){Ad.call(this);this.gc=new Hd(this);this.mj=this;this.zg=null}s(ge,Ad);ge.prototype[Dd]=!0;g=ge.prototype;g.He=function(){return this.zg};g.Ng=function(a){this.zg=a};g.addEventListener=function(a,b,c,d){Wd(this,a,b,c,d)};g.removeEventListener=function(a,b,c,d){be(this,a,b,c,d)};
g.dispatchEvent=function(a){var b,c=this.He();if(c)for(b=[];c;c=c.He())b.push(c);var c=this.mj,d=a.type||a;if(n(a))a=new Kd(a,c);else if(a instanceof Kd)a.target=a.target||c;else{var e=a;a=new Kd(d,c);lb(a,e)}var e=!0,f;if(b)for(var h=b.length-1;!a.Lc&&0<=h;h--)f=a.currentTarget=b[h],e=he(f,d,!0,a)&&e;a.Lc||(f=a.currentTarget=c,e=he(f,d,!0,a)&&e,a.Lc||(e=he(f,d,!1,a)&&e));if(b)for(h=0;!a.Lc&&h<b.length;h++)f=a.currentTarget=b[h],e=he(f,d,!1,a)&&e;return e};
g.ba=function(){ge.m.ba.call(this);this.gc&&this.gc.mf(void 0);this.zg=null};g.H=function(a,b,c,d){return this.gc.add(String(a),b,!1,c,d)};g.kb=function(a,b,c,d){return this.gc.remove(String(a),b,c,d)};function he(a,b,c,d){b=a.gc.La[String(b)];if(!b)return!0;b=b.concat();for(var e=!0,f=0;f<b.length;++f){var h=b[f];if(h&&!h.hd&&h.ue==c){var k=h.Ic,m=h.Ke||h.src;h.te&&Jd(a.gc,h);e=!1!==k.call(m,d)&&e}}return e&&0!=d.Hi}g.Ed=function(a,b,c,d){return this.gc.Ed(String(a),b,c,d)};function ie(a,b,c){if(r(a))c&&(a=na(a,c));else if(a&&"function"==typeof a.handleEvent)a=na(a.handleEvent,a);else throw Error("Invalid listener argument");return 2147483647<b?-1:l.setTimeout(a,b||0)};/*

 Visual Blocks Editor

 Copyright 2011 Google Inc.
 https://developers.google.com/blockly/

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
function je(a){this.s=a}g=je.prototype;g.td=47;g.zf=45;g.qd=15;g.bj=35;g.fh=35;g.le=25;g.Vb=!1;g.h=null;g.vf=null;g.rg=0;g.nc=0;g.li=0;g.Si=0;
g.I=function(){this.h=H("g",{filter:"url(#blocklyTrashcanShadowFilter)"},null);var a=H("clipPath",{id:"blocklyTrashBodyClipPath"},this.h);H("rect",{width:this.td,height:this.zf,y:this.qd},a);H("image",{width:ke,height:le,y:-32,"clip-path":"url(#blocklyTrashBodyClipPath)"},this.h).setAttributeNS("http://www.w3.org/1999/xlink","xlink:href",me+ne);a=H("clipPath",{id:"blocklyTrashLidClipPath"},this.h);H("rect",{width:this.td,height:this.qd},a);this.vf=H("image",{width:ke,height:le,y:-32,"clip-path":"url(#blocklyTrashLidClipPath)"},
this.h);this.vf.setAttributeNS("http://www.w3.org/1999/xlink","xlink:href",me+ne);this.Hf();return this.h};g.o=function(){oe(this,!1);this.Sd();K(window,"resize",this,this.Sd)};g.i=function(){this.h&&(z(this.h),this.h=null);this.s=this.vf=null;l.clearTimeout(this.rg)};g.Sd=function(){var a=this.s.Tb();a&&(this.li=C?this.fh:a.V+a.fb-this.td-this.fh,this.Si=a.za+a.gb-(this.zf+this.qd)-this.bj,this.h.setAttribute("transform","translate("+this.li+","+this.Si+")"))};
g.df=function(a){if(this.h){a=wd(a);var b=xd(this.h);a=a.x>b.x-this.le&&a.x<b.x+this.td+this.le&&a.y>b.y-this.le&&a.y<b.y+this.zf+this.qd+this.le;this.Vb!=a&&oe(this,a)}};function oe(a,b){a.Vb!=b&&(l.clearTimeout(a.rg),a.Vb=b,a.Hf())}
g.Hf=function(){this.nc+=this.Vb?.2:-.2;this.nc=Math.min(Math.max(this.nc,0),1);var a=45*this.nc;this.vf.setAttribute("transform","rotate("+(C?-a:a)+", "+(C?4:this.td-4)+", "+(this.qd-2)+")");this.h.style.opacity=.2+.2*this.nc;if(0<this.nc||1>this.nc)this.rg=ie(this.Hf,20,this)};g.close=function(){oe(this,!1)};/*

 Visual Blocks Editor

 Copyright 2012 Google Inc.
 https://developers.google.com/blockly/

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
function pe(a,b){this.Tb=a;this.Li=b;this.ji=!1;this.md=[];this.tg=Infinity;var c=[];c[1]=new qe;c[2]=new qe;c[3]=new qe;c[4]=new qe;this.Bj=c}g=pe.prototype;g.Xf=!1;g.scrollX=0;g.scrollY=0;g.Ua=null;g.Zf=null;g.qc=null;g.I=function(){this.h=H("g",{},null);this.Q=H("g",{},this.h);this.ld=H("g",{},this.h);re(this);return this.h};g.i=function(){this.h&&(z(this.h),this.h=null);this.ld=this.Q=null;this.Ua&&(this.Ua.i(),this.Ua=null)};
function se(){var a=y;if(Fc&&!F){a.Ua=new je(a);var b=a.Ua.I();a.h.insertBefore(b,a.Q);a.Ua.o()}}function te(a,b){a.md.push(b);ue&&a==y&&-1==ve.indexOf(b)&&ve.push(b);re(a)}function we(a,b){for(var c=!1,d,e=0;d=a.md[e];e++)if(d==b){a.md.splice(e,1);c=!0;break}if(!c)throw"Block not present in workspace's list of top-most blocks.";ue&&a==y&&ve.jm(b);re(a)}
function Wc(a,b){var c=[].concat(a.md);if(b&&1<c.length){var d=Math.sin(3*Math.PI/180);C&&(d*=-1);c.sort(function(a,b){var c=D(a),k=D(b);return c.y+d*c.x-(k.y+d*k.x)})}return c}function xe(a){a=Wc(a,!1);for(var b=0;b<a.length;b++)a.push.apply(a,a[b].Dc());return a}g.clear=function(){for(zd();this.md.length;)this.md[0].i()};g.v=function(){for(var a=xe(this),b=0,c;c=a[b];b++)c.Dc().length||c.v()};function ye(a,b){for(var c=xe(a),d=0,e;e=c[d];d++)if(e.id==b)return e;return null}
function ze(a,b){a.Zg=b;a.$g&&(J(a.$g),a.$g=null);b&&(a.$g=K(a.Q,"blocklySelectChange",a,function(){this.Zg=!1}))}function Ae(a){var b=y;b.Zg&&0!=Be&&ze(b,!1);if(b.Zg){var c=null;if(a&&(c=ye(b,a),!c))return;ze(b,!1);c?c.select():L&&Ce();setTimeout(function(){ze(b,!0)},1)}}function re(a){a.Zf&&window.clearTimeout(a.Zf);var b=a.Q;b&&(a.Zf=window.setTimeout(function(){De(b,"blocklyWorkspaceChange")},0))}function Ee(a){return Infinity==a.tg?Infinity:a.tg-xe(a).length}pe.prototype.clear=pe.prototype.clear;function Fe(a,b,c,d){this.top=a;this.right=b;this.bottom=c;this.left=d}g=Fe.prototype;g.clone=function(){return new Fe(this.top,this.right,this.bottom,this.left)};g.toString=function(){return"("+this.top+"t, "+this.right+"r, "+this.bottom+"b, "+this.left+"l)"};g.contains=function(a){return this&&a?a instanceof Fe?a.left>=this.left&&a.right<=this.right&&a.top>=this.top&&a.bottom<=this.bottom:a.x>=this.left&&a.x<=this.right&&a.y>=this.top&&a.y<=this.bottom:!1};
g.expand=function(a,b,c,d){ha(a)?(this.top-=a.top,this.right+=a.right,this.bottom+=a.bottom,this.left-=a.left):(this.top-=a,this.right+=b,this.bottom+=c,this.left-=d);return this};g.ceil=function(){this.top=Math.ceil(this.top);this.right=Math.ceil(this.right);this.bottom=Math.ceil(this.bottom);this.left=Math.ceil(this.left);return this};g.floor=function(){this.top=Math.floor(this.top);this.right=Math.floor(this.right);this.bottom=Math.floor(this.bottom);this.left=Math.floor(this.left);return this};
g.round=function(){this.top=Math.round(this.top);this.right=Math.round(this.right);this.bottom=Math.round(this.bottom);this.left=Math.round(this.left);return this};g.translate=function(a,b){a instanceof x?(this.left+=a.x,this.right+=a.x,this.top+=a.y,this.bottom+=a.y):(this.left+=a,this.right+=a,ga(b)&&(this.top+=b,this.bottom+=b));return this};g.scale=function(a,b){var c=ga(b)?b:a;this.left*=a;this.right*=a;this.top*=c;this.bottom*=c;return this};function Ge(a,b,c,d){this.left=a;this.top=b;this.width=c;this.height=d}g=Ge.prototype;g.clone=function(){return new Ge(this.left,this.top,this.width,this.height)};g.toString=function(){return"("+this.left+", "+this.top+" - "+this.width+"w x "+this.height+"h)"};g.contains=function(a){return a instanceof Ge?this.left<=a.left&&this.left+this.width>=a.left+a.width&&this.top<=a.top&&this.top+this.height>=a.top+a.height:a.x>=this.left&&a.x<=this.left+this.width&&a.y>=this.top&&a.y<=this.top+this.height};
g.Zh=function(){return new Sb(this.width,this.height)};g.ceil=function(){this.left=Math.ceil(this.left);this.top=Math.ceil(this.top);this.width=Math.ceil(this.width);this.height=Math.ceil(this.height);return this};g.floor=function(){this.left=Math.floor(this.left);this.top=Math.floor(this.top);this.width=Math.floor(this.width);this.height=Math.floor(this.height);return this};
g.round=function(){this.left=Math.round(this.left);this.top=Math.round(this.top);this.width=Math.round(this.width);this.height=Math.round(this.height);return this};g.translate=function(a,b){a instanceof x?(this.left+=a.x,this.top+=a.y):(this.left+=a,ga(b)&&(this.top+=b));return this};g.scale=function(a,b){var c=ga(b)?b:a;this.left*=a;this.width*=a;this.top*=c;this.height*=c;return this};function He(a,b){var c=$b(a);return c.defaultView&&c.defaultView.getComputedStyle&&(c=c.defaultView.getComputedStyle(a,null))?c[b]||c.getPropertyValue(b)||"":""}function Ie(a,b){return He(a,b)||(a.currentStyle?a.currentStyle[b]:null)||a.style&&a.style[b]}function Je(){var a=document,b=a.body,a=a.documentElement;return new x(b.scrollLeft||a.scrollLeft,b.scrollTop||a.scrollTop)}
function Ke(a){var b;try{b=a.getBoundingClientRect()}catch(c){return{left:0,top:0,right:0,bottom:0}}v&&a.ownerDocument.body&&(a=a.ownerDocument,b.left-=a.documentElement.clientLeft+a.body.clientLeft,b.top-=a.documentElement.clientTop+a.body.clientTop);return b}
function Le(a){if(v&&!(v&&8<=Rb))return a.offsetParent;var b=$b(a),c=Ie(a,"position"),d="fixed"==c||"absolute"==c;for(a=a.parentNode;a&&a!=b;a=a.parentNode)if(c=Ie(a,"position"),d=d&&"static"==c&&a!=b.documentElement&&a!=b.body,!d&&(a.scrollWidth>a.clientWidth||a.scrollHeight>a.clientHeight||"fixed"==c||"absolute"==c||"relative"==c))return a;return null}
function Me(a){var b,c=$b(a),d=Ie(a,"position"),e=Gb&&c.getBoxObjectFor&&!a.getBoundingClientRect&&"absolute"==d&&(b=c.getBoxObjectFor(a))&&(0>b.screenX||0>b.screenY),f=new x(0,0),h;b=c?$b(c):document;(h=!v||v&&9<=Rb)||(h="CSS1Compat"==Yb(b).Rb.compatMode);h=h?b.documentElement:b.body;if(a==h)return f;if(a.getBoundingClientRect)b=Ke(a),c=Yb(c).Rb,a=w||"CSS1Compat"!=c.compatMode?c.body||c.documentElement:c.documentElement,c=c.parentWindow||c.defaultView,a=v&&Pb("10")&&c.pageYOffset!=a.scrollTop?new x(a.scrollLeft,
a.scrollTop):new x(c.pageXOffset||a.scrollLeft,c.pageYOffset||a.scrollTop),f.x=b.left+a.x,f.y=b.top+a.y;else if(c.getBoxObjectFor&&!e)b=c.getBoxObjectFor(a),a=c.getBoxObjectFor(h),f.x=b.screenX-a.screenX,f.y=b.screenY-a.screenY;else{b=a;do{f.x+=b.offsetLeft;f.y+=b.offsetTop;b!=a&&(f.x+=b.clientLeft||0,f.y+=b.clientTop||0);if(w&&"fixed"==Ie(b,"position")){f.x+=c.body.scrollLeft;f.y+=c.body.scrollTop;break}b=b.offsetParent}while(b&&b!=a);if(Fb||w&&"absolute"==d)f.y-=c.body.offsetTop;for(b=a;(b=Le(b))&&
b!=c.body&&b!=h;)f.x-=b.scrollLeft,Fb&&"TR"==b.tagName||(f.y-=b.scrollTop)}return f}function Ne(a){var b=Oe;if("none"!=Ie(a,"display"))return b(a);var c=a.style,d=c.display,e=c.visibility,f=c.position;c.visibility="hidden";c.position="absolute";c.display="inline";a=b(a);c.display=d;c.position=f;c.visibility=e;return a}function Oe(a){var b=a.offsetWidth,c=a.offsetHeight,d=w&&!b&&!c;return(void 0===b||d)&&a.getBoundingClientRect?(a=Ke(a),new Sb(a.right-a.left,a.bottom-a.top)):new Sb(b,c)}
function Pe(a){var b=Me(a);a=Ne(a);return new Ge(b.x,b.y,a.width,a.height)}function Qe(a,b){a.style.display=b?"":"none"}var Re=Gb?"MozUserSelect":w?"WebkitUserSelect":null;function Se(a,b,c){c=c?null:a.getElementsByTagName("*");if(Re){if(b=b?"none":"",a.style[Re]=b,c){a=0;for(var d;d=c[a];a++)d.style[Re]=b}}else if(v||Fb)if(b=b?"on":"",a.setAttribute("unselectable",b),c)for(a=0;d=c[a];a++)d.setAttribute("unselectable",b)}var Te={thin:2,medium:4,thick:6};
function Ue(a,b){if("none"==(a.currentStyle?a.currentStyle[b+"Style"]:null))return 0;var c=a.currentStyle?a.currentStyle[b+"Width"]:null,d;if(c in Te)d=Te[c];else if(/^\d+px?$/.test(c))d=parseInt(c,10);else{d=a.style.left;var e=a.runtimeStyle.left;a.runtimeStyle.left=a.currentStyle.left;a.style.left=c;c=a.style.pixelLeft;a.style.left=d;a.runtimeStyle.left=e;d=c}return d}
function Ve(a){if(v&&!(v&&9<=Rb)){var b=Ue(a,"borderLeft"),c=Ue(a,"borderRight"),d=Ue(a,"borderTop");a=Ue(a,"borderBottom");return new Fe(d,c,a,b)}b=He(a,"borderLeftWidth");c=He(a,"borderRightWidth");d=He(a,"borderTopWidth");a=He(a,"borderBottomWidth");return new Fe(parseFloat(d),parseFloat(c),parseFloat(a),parseFloat(b))};function We(a){Ad.call(this);this.ci=a;this.Ue={}}s(We,Ad);var Xe=[];g=We.prototype;g.H=function(a,b,c,d){ea(b)||(b&&(Xe[0]=b.toString()),b=Xe);for(var e=0;e<b.length;e++){var f=Wd(a,b[e],c||this.handleEvent,d||!1,this.ci||this);if(!f)break;this.Ue[f.key]=f}return this};
g.kb=function(a,b,c,d,e){if(ea(b))for(var f=0;f<b.length;f++)this.kb(a,b[f],c,d,e);else c=c||this.handleEvent,e=e||this.ci||this,c=Xd(c),d=!!d,b=a&&a[Dd]?a.Ed(b,c,d,e):a?(a=Yd(a))?a.Ed(b,c,d,e):null:null,b&&(ce(b),delete this.Ue[b.key]);return this};g.mf=function(){jb(this.Ue,ce);this.Ue={}};g.ba=function(){We.m.ba.call(this);this.mf()};g.handleEvent=function(){throw Error("EventHandler.handleEvent not implemented");};function Ye(){}ca(Ye);Ye.prototype.Bk=0;function Ze(a){ge.call(this);this.ye=a||Yb();this.qf=$e;this.Ne=null;this.G=!1;this.w=null;this.jc=void 0;this.bc=this.Y=this.Ga=this.Ye=null;this.ll=!1}s(Ze,ge);Ze.prototype.kk=Ye.hc();var $e=null;
function af(a,b){switch(a){case 1:return b?"disable":"enable";case 2:return b?"highlight":"unhighlight";case 4:return b?"activate":"deactivate";case 8:return b?"select":"unselect";case 16:return b?"check":"uncheck";case 32:return b?"focus":"blur";case 64:return b?"open":"close"}throw Error("Invalid component state");}function bf(a){return a.Ne||(a.Ne=":"+(a.kk.Bk++).toString(36))}g=Ze.prototype;g.k=function(){return this.w};function cf(a){a.jc||(a.jc=new We(a));return a.jc}
g.$a=function(a){if(this==a)throw Error("Unable to set parent component");if(a&&this.Ga&&this.Ne&&df(this.Ga,this.Ne)&&this.Ga!=a)throw Error("Unable to set parent component");this.Ga=a;Ze.m.Ng.call(this,a)};g.getParent=function(){return this.Ga};g.Ng=function(a){if(this.Ga&&this.Ga!=a)throw Error("Method not supported");Ze.m.Ng.call(this,a)};g.tb=function(){return this.ye};g.I=function(){this.w=this.ye.createElement("div")};g.v=function(a){this.Ud(a)};
g.Ud=function(a,b){if(this.G)throw Error("Component already rendered");this.w||this.I();a?a.insertBefore(this.w,b||null):this.ye.Rb.body.appendChild(this.w);this.Ga&&!this.Ga.G||this.va()};g.va=function(){this.G=!0;ef(this,function(a){!a.G&&a.k()&&a.va()})};g.hb=function(){ef(this,function(a){a.G&&a.hb()});this.jc&&this.jc.mf();this.G=!1};
g.ba=function(){this.G&&this.hb();this.jc&&(this.jc.i(),delete this.jc);ef(this,function(a){a.i()});!this.ll&&this.w&&z(this.w);this.Ga=this.Ye=this.w=this.bc=this.Y=null;Ze.m.ba.call(this)};g.pe=function(a,b){this.Uc(a,ff(this),b)};
g.Uc=function(a,b,c){if(a.G&&(c||!this.G))throw Error("Component already rendered");if(0>b||b>ff(this))throw Error("Child component index out of bounds");this.bc&&this.Y||(this.bc={},this.Y=[]);if(a.getParent()==this){var d=bf(a);this.bc[d]=a;Ya(this.Y,a)}else{var d=this.bc,e=bf(a);if(e in d)throw Error('The object already contains the key "'+e+'"');d[e]=a}a.$a(this);$a(this.Y,b,0,a);a.G&&this.G&&a.getParent()==this?(c=this.sb(),c.insertBefore(a.k(),c.childNodes[b]||null)):c?(this.w||this.I(),b=M(this,
b+1),a.Ud(this.sb(),b?b.w:null)):this.G&&!a.G&&a.w&&a.w.parentNode&&1==a.w.parentNode.nodeType&&a.va()};g.sb=function(){return this.w};function gf(a){null==a.qf&&(a.qf="rtl"==Ie(a.G?a.w:a.ye.Rb.body,"direction"));return a.qf}g.Xd=function(a){if(this.G)throw Error("Component already rendered");this.qf=a};function hf(a){return!!a.Y&&0!=a.Y.length}function ff(a){return a.Y?a.Y.length:0}function df(a,b){var c;a.bc&&b?(c=a.bc,c=(b in c?c[b]:void 0)||null):c=null;return c}
function M(a,b){return a.Y?a.Y[b]||null:null}function ef(a,b,c){a.Y&&Ta(a.Y,b,c)}function jf(a,b){return a.Y&&b?Sa(a.Y,b):-1}g.removeChild=function(a,b){if(a){var c=n(a)?a:bf(a);a=df(this,c);if(c&&a){var d=this.bc;c in d&&delete d[c];Ya(this.Y,a);b&&(a.hb(),a.w&&z(a.w));a.$a(null)}}if(!a)throw Error("Child is not in parent component");return a};g.Ei=function(a){for(var b=[];hf(this);)b.push(this.removeChild(M(this,0),a));return b};function kf(a){if(a.classList)return a.classList;a=a.className;return n(a)&&a.match(/\S+/g)||[]}function lf(a,b){return a.classList?a.classList.contains(b):Xa(kf(a),b)}function mf(a,b){a.classList?a.classList.add(b):lf(a,b)||(a.className+=0<a.className.length?" "+b:b)}function nf(a,b){if(a.classList)Ta(b,function(b){mf(a,b)});else{var c={};Ta(kf(a),function(a){c[a]=!0});Ta(b,function(a){c[a]=!0});a.className="";for(var d in c)a.className+=0<a.className.length?" "+d:d}}
function of(a,b){a.classList?a.classList.remove(b):lf(a,b)&&(a.className=Ua(kf(a),function(a){return a!=b}).join(" "))}function pf(a,b){a.classList?Ta(b,function(b){of(a,b)}):a.className=Ua(kf(a),function(a){return!Xa(b,a)}).join(" ")};function qf(a,b){if(!a)throw Error("Invalid class name "+a);if(!r(b))throw Error("Invalid decorator function "+b);}var rf={};var sf;function tf(a,b){b?a.setAttribute("role",b):a.removeAttribute("role")}function uf(a,b,c){ea(c)&&(c=c.join(" "));var d="aria-"+b;""===c||void 0==c?(sf||(sf={atomic:!1,autocomplete:"none",dropeffect:"none",haspopup:!1,live:"off",multiline:!1,multiselectable:!1,orientation:"vertical",readonly:!1,relevant:"additions text",required:!1,sort:"none",busy:!1,disabled:!1,hidden:!1,invalid:"false"}),c=sf,b in c?a.setAttribute(d,c[b]):a.removeAttribute(d)):a.setAttribute(d,c)};function vf(){}var wf;ca(vf);var xf={button:"pressed",checkbox:"checked",menuitem:"selected",menuitemcheckbox:"checked",menuitemradio:"checked",radio:"checked",tab:"selected",treeitem:"selected"};g=vf.prototype;g.Ae=function(){};g.I=function(a){var b=a.tb().I("div",this.De(a).join(" "),a.Qb);yf(a,b);return b};g.sb=function(a){return a};g.yd=function(a,b,c){if(a=a.k?a.k():a){var d=[b];v&&!Pb("7")&&(d=zf(kf(a),b),d.push(b));(c?nf:pf)(a,d)}};
g.Md=function(a){gf(a)&&this.Xd(a.k(),!0);a.isEnabled()&&this.Pc(a,a.A())};function Af(a,b,c){if(a=c||a.Ae())c=b.getAttribute("role")||null,a!=c&&tf(b,a)}function yf(a,b){a.A()||uf(b,"hidden",!a.A());a.isEnabled()||Bf(b,1,!a.isEnabled());a.$&8&&Bf(b,8,a.Re());a.$&16&&Bf(b,16,!!(a.ga&16));a.$&64&&Bf(b,64,a.Vb())}g.Kg=function(a,b){Se(a,!b,!v&&!Fb)};g.Xd=function(a,b){this.yd(a,this.Da()+"-rtl",b)};g.kc=function(a){var b;return a.$&32&&(b=a.ra())?mc(b)&&nc(b):!1};
g.Pc=function(a,b){var c;if(a.$&32&&(c=a.ra())){if(!b&&a.ga&32){try{c.blur()}catch(d){}a.ga&32&&a.Hd(null)}(mc(c)&&nc(c))!=b&&(b?c.tabIndex=0:(c.tabIndex=-1,c.removeAttribute("tabIndex")))}};g.N=function(a,b){Qe(a,b);a&&uf(a,"hidden",!b)};g.zb=function(a,b,c){var d=a.k();if(d){var e=this.Ce(b);e&&this.yd(a,e,c);Bf(d,b,c)}};
function Bf(a,b,c){wf||(wf={1:"disabled",8:"selected",16:"checked",64:"expanded"});b=wf[b];var d=a.getAttribute("role")||null;d&&(d=xf[d]||b,b="checked"==b||"selected"==b?d:b);b&&uf(a,b,c)}g.ra=function(a){return a.k()};g.Da=function(){return"goog-control"};g.De=function(a){var b=this.Da(),c=[b],d=this.Da();d!=b&&c.push(d);b=a.ga;for(d=[];b;){var e=b&-b;d.push(this.Ce(e));b&=~e}c.push.apply(c,d);(a=a.Db)&&c.push.apply(c,a);v&&!Pb("7")&&c.push.apply(c,zf(c));return c};
function zf(a,b){var c=[];b&&(a=a.concat([b]));Ta([],function(d){!Wa(d,oa(Xa,a))||b&&!Xa(d,b)||c.push(d.join("_"))});return c}g.Ce=function(a){if(!this.xh){var b=this.Da();b.replace(/\xa0|\s/g," ");this.xh={1:b+"-disabled",2:b+"-hover",4:b+"-active",8:b+"-selected",16:b+"-checked",32:b+"-focused",64:b+"-open"}}return this.xh[a]};function Cf(a,b,c,d,e){if(!(v||w&&Pb("525")))return!0;if(Ib&&e)return Df(a);if(e&&!d)return!1;ga(b)&&(b=Ef(b));if(!c&&(17==b||18==b||Ib&&91==b))return!1;if(w&&d&&c)switch(a){case 220:case 219:case 221:case 192:case 186:case 189:case 187:case 188:case 190:case 191:case 192:case 222:return!1}if(v&&d&&b==a)return!1;switch(a){case 13:return!0;case 27:return!w}return Df(a)}
function Df(a){if(48<=a&&57>=a||96<=a&&106>=a||65<=a&&90>=a||w&&0==a)return!0;switch(a){case 32:case 63:case 107:case 109:case 110:case 111:case 186:case 59:case 189:case 187:case 61:case 188:case 190:case 191:case 192:case 222:case 219:case 220:case 221:return!0;default:return!1}}function Ef(a){if(Gb)a=Ff(a);else if(Ib&&w)a:switch(a){case 93:a=91;break a}return a}
function Ff(a){switch(a){case 61:return 187;case 59:return 186;case 173:return 189;case 224:return 91;case 0:return 224;default:return a}};function Gf(a,b){ge.call(this);a&&Hf(this,a,b)}s(Gf,ge);g=Gf.prototype;g.w=null;g.Se=null;g.pg=null;g.Te=null;g.Xa=-1;g.mc=-1;g.Ff=!1;
var Jf={3:13,12:144,63232:38,63233:40,63234:37,63235:39,63236:112,63237:113,63238:114,63239:115,63240:116,63241:117,63242:118,63243:119,63244:120,63245:121,63246:122,63247:123,63248:44,63272:46,63273:36,63275:35,63276:33,63277:34,63289:144,63302:45},Kf={Up:38,Down:40,Left:37,Right:39,Enter:13,F1:112,F2:113,F3:114,F4:115,F5:116,F6:117,F7:118,F8:119,F9:120,F10:121,F11:122,F12:123,"U+007F":46,Home:36,End:35,PageUp:33,PageDown:34,Insert:45},Lf=v||w&&Pb("525"),Mf=Ib&&Gb;g=Gf.prototype;
g.dk=function(a){w&&(17==this.Xa&&!a.ctrlKey||18==this.Xa&&!a.altKey||Ib&&91==this.Xa&&!a.metaKey)&&(this.mc=this.Xa=-1);-1==this.Xa&&(a.ctrlKey&&17!=a.keyCode?this.Xa=17:a.altKey&&18!=a.keyCode?this.Xa=18:a.metaKey&&91!=a.keyCode&&(this.Xa=91));Lf&&!Cf(a.keyCode,this.Xa,a.shiftKey,a.ctrlKey,a.altKey)?this.handleEvent(a):(this.mc=Ef(a.keyCode),Mf&&(this.Ff=a.altKey))};g.ek=function(a){this.mc=this.Xa=-1;this.Ff=a.altKey};
g.handleEvent=function(a){var b=a.Sb,c,d,e=b.altKey;v&&"keypress"==a.type?(c=this.mc,d=13!=c&&27!=c?b.keyCode:0):w&&"keypress"==a.type?(c=this.mc,d=0<=b.charCode&&63232>b.charCode&&Df(c)?b.charCode:0):Fb?(c=this.mc,d=Df(c)?b.keyCode:0):(c=b.keyCode||this.mc,d=b.charCode||0,Mf&&(e=this.Ff),Ib&&63==d&&224==c&&(c=191));var f=c=Ef(c),h=b.keyIdentifier;c?63232<=c&&c in Jf?f=Jf[c]:25==c&&a.shiftKey&&(f=9):h&&h in Kf&&(f=Kf[h]);a=f==this.Xa;this.Xa=f;b=new Nf(f,d,a,b);b.altKey=e;this.dispatchEvent(b)};
g.k=function(){return this.w};function Hf(a,b,c){a.Te&&a.detach();a.w=b;a.Se=Wd(a.w,"keypress",a,c);a.pg=Wd(a.w,"keydown",a.dk,c,a);a.Te=Wd(a.w,"keyup",a.ek,c,a)}g.detach=function(){this.Se&&(ce(this.Se),ce(this.pg),ce(this.Te),this.Te=this.pg=this.Se=null);this.w=null;this.mc=this.Xa=-1};g.ba=function(){Gf.m.ba.call(this);this.detach()};function Nf(a,b,c,d){Qd.call(this,d);this.type="key";this.keyCode=a;this.charCode=b;this.repeat=c}s(Nf,Qd);function N(a,b,c){Ze.call(this,c);if(!b){b=this.constructor;for(var d;b;){d=ia(b);if(d=rf[d])break;b=b.m?b.m.constructor:null}b=d?r(d.hc)?d.hc():new d:null}this.M=b;this.Xk(void 0!==a?a:null)}s(N,Ze);g=N.prototype;g.Qb=null;g.ga=0;g.$=39;g.tj=255;g.$d=0;g.na=!0;g.Db=null;g.Jd=!0;g.Ef=!1;g.Ok=null;g.ra=function(){return this.M.ra(this)};g.Ge=function(){return this.Fa||(this.Fa=new Gf)};
g.yd=function(a,b){b?a&&(this.Db?Xa(this.Db,a)||this.Db.push(a):this.Db=[a],this.M.yd(this,a,!0)):a&&this.Db&&Ya(this.Db,a)&&(0==this.Db.length&&(this.Db=null),this.M.yd(this,a,!1))};g.I=function(){var a=this.M.I(this);this.w=a;Af(this.M,a,this.Ie());this.Ef||this.M.Kg(a,!1);this.A()||this.M.N(a,!1)};g.Ie=function(){return this.Ok};g.sb=function(){return this.M.sb(this.k())};
g.va=function(){N.m.va.call(this);this.M.Md(this);if(this.$&-2&&(this.Jd&&Of(this,!0),this.$&32)){var a=this.ra();if(a){var b=this.Ge();Hf(b,a);cf(this).H(b,"key",this.ub).H(a,"focus",this.Je).H(a,"blur",this.Hd)}}};
function Of(a,b){var c=cf(a),d=a.k();b?(c.H(d,"mouseover",a.jg).H(d,"mousedown",a.ad).H(d,"mouseup",a.Kd).H(d,"mouseout",a.ig),a.Id!=ba&&c.H(d,"contextmenu",a.Id),v&&c.H(d,"dblclick",a.ai)):(c.kb(d,"mouseover",a.jg).kb(d,"mousedown",a.ad).kb(d,"mouseup",a.Kd).kb(d,"mouseout",a.ig),a.Id!=ba&&c.kb(d,"contextmenu",a.Id),v&&c.kb(d,"dblclick",a.ai))}g.hb=function(){N.m.hb.call(this);this.Fa&&this.Fa.detach();this.A()&&this.isEnabled()&&this.M.Pc(this,!1)};
g.ba=function(){N.m.ba.call(this);this.Fa&&(this.Fa.i(),delete this.Fa);delete this.M;this.Db=this.Qb=null};g.Xk=function(a){this.Qb=a};g.cg=function(){var a=this.Qb;if(!a)return"";if(!n(a))if(ea(a))a=Va(a,oc).join("");else{if(Vb&&"innerText"in a)a=a.innerText.replace(/(\r\n|\r|\n)/g,"\n");else{var b=[];pc(a,b,!0);a=b.join("")}a=a.replace(/ \xAD /g," ").replace(/\xAD/g,"");a=a.replace(/\u200B/g,"");Vb||(a=a.replace(/ +/g," "));" "!=a&&(a=a.replace(/^\s*/,""))}return ua(a)};
g.Xd=function(a){N.m.Xd.call(this,a);var b=this.k();b&&this.M.Xd(b,a)};g.Kg=function(a){this.Ef=a;var b=this.k();b&&this.M.Kg(b,a)};g.A=function(){return this.na};g.N=function(a,b){if(b||this.na!=a&&this.dispatchEvent(a?"show":"hide")){var c=this.k();c&&this.M.N(c,a);this.isEnabled()&&this.M.Pc(this,a);this.na=a;return!0}return!1};g.isEnabled=function(){return!(this.ga&1)};
g.Wd=function(a){var b=this.getParent();b&&"function"==typeof b.isEnabled&&!b.isEnabled()||!Pf(this,1,!a)||(a||(this.setActive(!1),this.yb(!1)),this.A()&&this.M.Pc(this,a),this.zb(1,!a,!0))};g.yb=function(a){Pf(this,2,a)&&this.zb(2,a)};g.setActive=function(a){Pf(this,4,a)&&this.zb(4,a)};g.Re=function(){return!!(this.ga&8)};g.Yk=function(){Pf(this,8,!0)&&this.zb(8,!0)};function Qf(a,b){Pf(a,16,b)&&a.zb(16,b)}g.Vb=function(){return!!(this.ga&64)};function Rf(a,b){Pf(a,64,b)&&a.zb(64,b)}
g.zb=function(a,b,c){c||1!=a?this.$&a&&b!=!!(this.ga&a)&&(this.M.zb(this,a,b),this.ga=b?this.ga|a:this.ga&~a):this.Wd(!b)};g.ab=function(a,b){if(this.G&&this.ga&a&&!b)throw Error("Component already rendered");!b&&this.ga&a&&this.zb(a,!1);this.$=b?this.$|a:this.$&~a};function Sf(a,b){return!!(a.tj&b)&&!!(a.$&b)}function Pf(a,b,c){return!!(a.$&b)&&!!(a.ga&b)!=c&&(!(a.$d&b)||a.dispatchEvent(af(b,c)))&&!a.xd}
g.jg=function(a){!Tf(a,this.k())&&this.dispatchEvent("enter")&&this.isEnabled()&&Sf(this,2)&&this.yb(!0)};g.ig=function(a){!Tf(a,this.k())&&this.dispatchEvent("leave")&&(Sf(this,4)&&this.setActive(!1),Sf(this,2)&&this.yb(!1))};g.Id=ba;function Tf(a,b){return!!a.relatedTarget&&jc(b,a.relatedTarget)}g.ad=function(a){this.isEnabled()&&(Sf(this,2)&&this.yb(!0),!Sd(a)||w&&Ib&&a.ctrlKey||(Sf(this,4)&&this.setActive(!0),this.M.kc(this)&&this.ra().focus()));this.Ef||!Sd(a)||w&&Ib&&a.ctrlKey||a.preventDefault()};
g.Kd=function(a){this.isEnabled()&&(Sf(this,2)&&this.yb(!0),this.ga&4&&this.Rd(a)&&Sf(this,4)&&this.setActive(!1))};g.ai=function(a){this.isEnabled()&&this.Rd(a)};g.Rd=function(a){Sf(this,16)&&Qf(this,!(this.ga&16));Sf(this,8)&&this.Yk();Sf(this,64)&&Rf(this,!this.Vb());var b=new Kd("action",this);a&&(b.altKey=a.altKey,b.ctrlKey=a.ctrlKey,b.metaKey=a.metaKey,b.shiftKey=a.shiftKey,b.Ag=a.Ag);return this.dispatchEvent(b)};g.Je=function(){Sf(this,32)&&Pf(this,32,!0)&&this.zb(32,!0)};
g.Hd=function(){Sf(this,4)&&this.setActive(!1);Sf(this,32)&&Pf(this,32,!1)&&this.zb(32,!1)};g.ub=function(a){return this.A()&&this.isEnabled()&&this.Ec(a)?(a.preventDefault(),a.stopPropagation(),!0):!1};g.Ec=function(a){return 13==a.keyCode&&this.Rd(a)};if(!r(N))throw Error("Invalid component class "+N);if(!r(vf))throw Error("Invalid renderer class "+vf);var Uf=ia(N);rf[Uf]=vf;qf("goog-control",function(){return new N(null)});function Vf(){this.yh=[]}s(Vf,vf);ca(Vf);function Wf(a,b){var c=a.yh[b];if(!c){switch(b){case 0:c=a.Da()+"-highlight";break;case 1:c=a.Da()+"-checkbox";break;case 2:c=a.Da()+"-content"}a.yh[b]=c}return c}g=Vf.prototype;g.Ae=function(){return"menuitem"};g.I=function(a){var b=a.tb().I("div",this.De(a).join(" "),Xf(this,a.Qb,a.tb()));Yf(this,a,b,!!(a.$&8)||!!(a.$&16));return b};g.sb=function(a){return a&&a.firstChild};function Xf(a,b,c){a=Wf(a,2);return c.I("div",a,b)}
g.Mi=function(a,b,c){a&&b&&Yf(this,a,b,c)};g.Lg=function(a,b,c){a&&b&&Yf(this,a,b,c)};function Yf(a,b,c,d){Af(a,c,b.Ie());yf(b,c);var e;if(e=a.sb(c)){e=e.firstChild;var f=Wf(a,1);e=!!e&&ha(e)&&1==e.nodeType&&lf(e,f)}else e=!1;d!=e&&(d?mf(c,"goog-option"):of(c,"goog-option"),c=a.sb(c),d?(a=Wf(a,1),c.insertBefore(b.tb().I("div",a),c.firstChild||null)):c.removeChild(c.firstChild))}
g.Ce=function(a){switch(a){case 2:return Wf(this,0);case 16:case 8:return"goog-option-selected";default:return Vf.m.Ce.call(this,a)}};g.Da=function(){return"goog-menuitem"};function Zf(a,b,c,d){N.call(this,a,d||Vf.hc(),c);this.bb(b)}s(Zf,N);g=Zf.prototype;g.ic=function(){var a=this.Ye;return null!=a?a:this.cg()};g.bb=function(a){this.Ye=a};g.ab=function(a,b){Zf.m.ab.call(this,a,b);switch(a){case 8:this.ga&16&&!b&&Qf(this,!1);var c=this.k();c&&this.M.Mi(this,c,b);break;case 16:(c=this.k())&&this.M.Lg(this,c,b)}};g.Mi=function(a){this.ab(8,a)};g.Lg=function(a){this.ab(16,a)};
g.cg=function(){var a=this.Qb;return ea(a)?(a=Va(a,function(a){return ha(a)&&1==a.nodeType&&(lf(a,"goog-menuitem-accel")||lf(a,"goog-menuitem-mnemonic-separator"))?"":oc(a)}).join(""),ua(a)):Zf.m.cg.call(this)};g.Kd=function(a){var b=this.getParent();if(b){var c=b.xi;b.xi=null;if(b=c&&ga(a.clientX))b=new x(a.clientX,a.clientY),b=c==b?!0:c&&b?c.x==b.x&&c.y==b.y:!1;if(b)return}Zf.m.Kd.call(this,a)};g.Ec=function(a){return a.keyCode==this.oi&&this.Rd(a)?!0:Zf.m.Ec.call(this,a)};g.Xj=function(){return this.oi};
qf("goog-menuitem",function(){return new Zf(null)});Zf.prototype.Ie=function(){return this.$&16?"menuitemcheckbox":this.$&8?"menuitemradio":Zf.m.Ie.call(this)};Zf.prototype.getParent=function(){return N.prototype.getParent.call(this)};Zf.prototype.He=function(){return N.prototype.He.call(this)};/*

 Visual Blocks Editor

 Copyright 2012 Google Inc.
 https://developers.google.com/blockly/

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
function $f(a){this.g=a;this.h=H("g",{},null);this.wf=H("path",{"class":"blocklyPathDark",transform:"translate(1, 1)"},this.h);this.Kb=H("path",{"class":"blocklyPath"},this.h);this.xf=H("path",{"class":"blocklyPathLight"},this.h);this.Kb.cb=this.g;ag(this.Kb);bg(this)}$f.prototype.height=0;$f.prototype.width=0;$f.prototype.o=function(){var a=this.g;this.tc();for(var b=0,c;c=a.S[b];b++)c.o();a.wb&&cg(a.wb)};function bg(a){a.g.Hb&&!F?dg(a.h,"blocklyDraggable"):eg(a.h,"blocklyDraggable")}
$f.prototype.wa=function(){return this.h};var fg=7*(1-Math.SQRT1_2)+1,gg=9*(1-Math.SQRT1_2)-1,hg="m "+fg+","+fg,ig="a 9,9 0 0,0 "+(-gg-1)+","+(8-gg),jg="a 9,9 0 0,0 "+(8-gg)+","+(gg+1);g=$f.prototype;g.i=function(){z(this.h);this.g=this.wf=this.xf=this.Kb=this.h=null};function kg(a){var b=(new Date-a.Sg)/150;1<b?z(a):(a.setAttribute("transform","translate("+(a.Ti+(C?-1:1)*a.rh.width/2*b+", "+(a.Ui+a.rh.height*b))+") scale("+(1-b)+")"),window.setTimeout(function(){kg(a)},10))}
function lg(a){var b=(new Date-a.Sg)/150;1<b?z(a):(a.setAttribute("r",25*b),a.style.opacity=1-b,window.setTimeout(function(){lg(a)},10))}
g.tc=function(){if(!this.g.disabled){var a=mg(ng(this.g.Lf)),b,c;c=a;if(!og.test(c))throw Error("'"+c+"' is not a valid hex color");4==c.length&&(c=c.replace(pg,"#$1$1$2$2$3$3"));c=c.toLowerCase();b=[parseInt(c.substr(1,2),16),parseInt(c.substr(3,2),16),parseInt(c.substr(5,2),16)];c=qg([255,255,255],b,.3);b=qg([0,0,0],b,.4);this.xf.setAttribute("stroke",mg(c));this.wf.setAttribute("fill",mg(b));this.Kb.setAttribute("fill",a)}};
function rg(a){a.g.disabled||sg(a.g)?(dg(a.h,"blocklyDisabled"),a.Kb.setAttribute("fill","url(#blocklyDisabledPattern)")):(eg(a.h,"blocklyDisabled"),a.tc());a=a.g.Dc();for(var b=0,c;c=a[b];b++)rg(c.n)}g.Cf=function(){dg(this.h,"blocklySelected");this.h.parentNode.appendChild(this.h)};g.nf=function(){eg(this.h,"blocklySelected")};
g.v=function(){this.g.L=!0;var a=10;C&&(a=-a);for(var b=tg(this.g),c=0;c<b.length;c++){var d=b[c];d.g.isCollapsed()?d.xa.setAttribute("display","none"):(d.xa.setAttribute("display","block"),C&&(a-=16),d.xa.setAttribute("transform","translate("+a+", 5)"),ug(d),a=C?a-10:a+26)}var e=a+=C?10:-10,f=this.g.S,b=[];b.Z=e+20;if(this.g.F||this.g.D)b.Z=Math.max(b.Z,40);for(var d=c=0,h=!1,k=!1,m=!1,p=void 0,q=this.g.Nd&&!this.g.isCollapsed(),u=0,t;t=f[u];u++)if(t.A()){var G;q&&p&&3!=p&&3!=t.type?G=b[b.length-
1]:(p=t.type,G=[],G.type=q&&3!=t.type?-1:t.type,G.height=0,b.push(G));G.push(t);t.Nc=25;t.ya=q&&1==t.type?20.5:0;if(t.p&&t.p.t){var Aa=vg(E(t.p));t.Nc=Math.max(t.Nc,Aa.height);t.ya=Math.max(t.ya,Aa.width)}u==f.length-1&&t.Nc--;G.height=Math.max(G.height,t.Nc);t.rb=0;1==b.length&&(t.rb+=C?-e:e);for(var Aa=!1,If=0,tb;tb=t.Ca[If];If++){0!=If&&(t.rb+=10);var mi=tb.Zh();tb.ya=mi.width;tb.of=Aa&&tb.pd?10:0;t.rb+=tb.ya+tb.of;G.height=Math.max(G.height,mi.height);Aa=tb.pd}-1!=G.type&&(3==G.type?(k=!0,d=Math.max(d,
t.rb)):(1==G.type?h=!0:5==G.type&&(m=!0),c=Math.max(c,t.rb)))}for(e=0;G=b[e];e++)if(G.Ri=!1,-1==G.type)for(f=0;t=G[f];f++)if(1==t.type){G.height+=10;G.Ri=!0;break}b.uf=20+d;k&&(b.Z=Math.max(b.Z,b.uf+30));h?b.Z=Math.max(b.Z,c+20+8):m&&(b.Z=Math.max(b.Z,c+20));b.ik=h;b.bm=k;b.am=m;d=a;this.g.K?this.Rg=this.tf=!0:(this.Rg=this.tf=!1,this.g.F&&(a=E(this.g.F))&&Yc(a)==this.g&&(this.tf=!0),Yc(this.g)&&(this.Rg=!0));h=D(this.g);k=[];m=[];a=[];c=[];t=b.Z;this.tf?(k.push("m 0,0"),a.push("m 1,1")):(k.push("m 0,8"),
a.push(C?hg:"m 1,7"),k.push("A 8,8 0 0,1 8,0"),a.push("A 7,7 0 0,1 8,1"));this.g.F&&(k.push("H",15),a.push("H",15),k.push("l 6,4 3,0 6,-4"),a.push("l 6.5,4 2,0 6.5,-4"),this.g.F.moveTo(h.x+(C?-30:30),h.y));k.push("H",t);a.push("H",t+(C?-1:0));this.width=t;for(G=t=0;e=b[G];G++){q=10;0==G&&(q+=C?-d:d);a.push("M",b.Z-1+","+(t+1));if(this.g.isCollapsed())f=e[0],u=t+18,wg(f.Ca,q,u),k.push("l 8,0 0,4 8,4 -16,8 8,4"),C?a.push("l 8,0 0,3.8 7,3.2 m -14.5,9 l 8,4"):a.push("h 8"),f=e.height-20,k.push("v",f),
C&&a.push("v",f-2),this.width+=15;else if(-1==e.type){for(p=0;f=e[p];p++)u=t+18,e.Ri&&(u+=5),q=wg(f.Ca,q,u),5!=f.type&&(q+=f.ya+10),1==f.type&&(m.push("M",q-10+","+(t+5)),m.push("h",6-f.ya),m.push("v 5 c 0,10 -8,-8 -8,7.5 s 8,-2.5 8,7.5"),m.push("v",f.Nc+1-20),m.push("h",f.ya+2-8),m.push("z"),C?(c.push("M",q-10-3+8-f.ya+","+(t+5+1)),c.push("v 6.5 m -7.84,2.5 q -0.4,10 2.16,10 m 5.68,-2.5 v 1.5"),c.push("v",f.Nc-20+3),c.push("h",f.ya-8+1)):(c.push("M",q-10+1+","+(t+5+1)),c.push("v",f.Nc+1),c.push("h",
6-f.ya),c.push("M",q-f.ya-10+.8+","+(t+5+20-.4)),c.push("l","3.36,-1.8")),u=C?h.x-q-8+10+f.ya+1:h.x+q+8-10-f.ya-1,Aa=h.y+t+5+1,f.p.moveTo(u,Aa),f.p.t&&xg(f.p));q=Math.max(q,b.Z);this.width=Math.max(this.width,q);k.push("H",q);a.push("H",q+(C?-1:0));k.push("v",e.height);C&&a.push("v",e.height-2)}else 1==e.type?(f=e[0],u=t+18,-1!=f.align&&(p=b.Z-f.rb-8-20,1==f.align?q+=p:0==f.align&&(q+=(p+q)/2)),wg(f.Ca,q,u),k.push("v 5 c 0,10 -8,-8 -8,7.5 s 8,-2.5 8,7.5"),p=e.height-20,k.push("v",p),C?(a.push("v 6.5 m -7.84,2.5 q -0.4,10 2.16,10 m 5.68,-2.5 v 1.5"),
a.push("v",p)):(a.push("M",b.Z-4.2+","+(t+20-.4)),a.push("l","3.36,-1.8")),u=h.x+(C?-b.Z-1:b.Z+1),Aa=h.y+t,f.p.moveTo(u,Aa),f.p.t&&(xg(f.p),this.width=Math.max(this.width,b.Z+vg(E(f.p)).width-8+1))):5==e.type?(f=e[0],u=t+18,-1!=f.align&&(p=b.Z-f.rb-20,b.ik&&(p-=8),1==f.align?q+=p:0==f.align&&(q+=(p+q)/2)),wg(f.Ca,q,u),k.push("v",e.height),C&&a.push("v",e.height-2)):3==e.type&&(f=e[0],0==G&&(k.push("v",10),C&&a.push("v",9),t+=10),u=t+18,-1!=f.align&&(p=b.uf-f.rb-20,1==f.align?q+=p:0==f.align&&(q+=
(p+q)/2)),wg(f.Ca,q,u),q=b.uf+30,k.push("H",q),k.push("l -6,4 -3,0 -6,-4 h -7 a 8,8 0 0,0 -8,8"),k.push("v",e.height-16),k.push("a 8,8 0 0,0 8,8"),k.push("H",b.Z),C?(a.push("M",q-30+gg+","+(t+gg)),a.push(ig),a.push("v",e.height-16),a.push("a 9,9 0 0,0 9,9"),a.push("H",b.Z-1)):(a.push("M",q-30+gg+","+(t+e.height-gg)),a.push(jg),a.push("H",b.Z)),u=h.x+(C?-q:q),Aa=h.y+t+1,f.p.moveTo(u,Aa),f.p.t&&(xg(f.p),this.width=Math.max(this.width,b.uf+vg(E(f.p)).width)),G==b.length-1||3==b[G+1].type)&&(k.push("v",
10),C&&a.push("v",9),t+=10);t+=e.height}b.length||(t=25,k.push("V",t),C&&a.push("V",t-1));b=t;this.height=b+1;this.g.D&&(k.push("H","30 l -6,4 -3,0 -6,-4"),this.g.D.moveTo(C?h.x-30:h.x+30,h.y+b+1),this.g.D.t&&xg(this.g.D),this.height+=4);this.Rg?(k.push("H 0"),C||a.push("M","1,"+b)):(k.push("H",8),k.push("a","8,8 0 0,1 -8,-8"),C||(a.push("M",fg+","+(b-fg)),a.push("A","7,7 0 0,1 1,"+(b-8))));this.g.K?(this.g.K.moveTo(h.x,h.y),k.push("V",20),k.push("c 0,-10 -8,8 -8,-7.5 s 8,2.5 8,-7.5"),C?(a.push("M",
"-2.4,8.9"),a.push("l","-3.6,-2.1")):(a.push("V",19),a.push("m","-7.36,-1 q -1.52,-5.5 0,-11"),a.push("m","7.36,1 V 1 H 2")),this.width+=8):C||(this.tf?a.push("V",1):a.push("V",8));k.push("z");b=k.join(" ")+"\n"+m.join(" ");this.Kb.setAttribute("d",b);this.wf.setAttribute("d",b);b=a.join(" ")+"\n"+c.join(" ");this.xf.setAttribute("d",b);C&&(this.Kb.setAttribute("transform","scale(-1 1)"),this.xf.setAttribute("transform","scale(-1 1)"),this.wf.setAttribute("transform","translate(1,1) scale(-1 1)"));
(b=this.g.getParent())?b.v():De(window,"resize")};function wg(a,b,c){C&&(b=-b);for(var d=0,e;e=a[d];d++)C?(b-=e.of+e.ya,e.wa().setAttribute("transform","translate("+b+", "+c+")"),e.ya&&(b-=10)):(e.wa().setAttribute("transform","translate("+(b+e.of)+", "+c+")"),e.ya&&(b+=e.of+e.ya+10));return C?-b:b};/*

 Visual Blocks Editor

 Copyright 2012 Google Inc.
 https://developers.google.com/blockly/

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
function yg(a){this.j=null;this.Qa=H("g",{},null);this.wc=H("rect",{rx:4,ry:4,x:-5,y:-12,height:16},this.Qa);this.ma=H("text",{"class":"blocklyText"},this.Qa);this.kd={height:25,width:0};this.T(a);this.na=!0}g=yg.prototype;g.clone=function(){La("There should never be an instance of Field, only its derived classes.")};g.pd=!0;g.o=function(a){if(this.j)throw"Field has already been initialized once.";this.j=a;this.Zb();O(a).appendChild(this.Qa);this.ug=K(this.Qa,"mouseup",this,this.yg);this.T(null)};
g.i=function(){this.ug&&(J(this.ug),this.ug=null);this.j=null;z(this.Qa);this.wc=this.ma=this.Qa=null};g.Zb=function(){this.pd&&(this.j.fc&&!F?(dg(this.Qa,"blocklyEditableText"),eg(this.Qa,"blocklyNoNEditableText"),this.Qa.style.cursor=this.dh):(dg(this.Qa,"blocklyNonEditableText"),eg(this.Qa,"blocklyEditableText"),this.Qa.style.cursor=""))};g.A=function(){return this.na};g.N=function(a){this.na=a;this.wa().style.display=a?"block":"none";this.Ud()};g.wa=function(){return this.Qa};
g.Ud=function(){try{var a=this.ma.getComputedTextLength()}catch(b){a=8*this.ma.childNodes[0].length}this.wc&&this.wc.setAttribute("width",a+10);this.kd.width=a};g.Zh=function(){this.kd.width||this.Ud();return this.kd};g.Ea=function(){return this.Ma};g.T=function(a){null!==a&&a!==this.Ma&&(this.Ma=a,zg(this),this.j&&this.j.L&&(this.j.v(),this.j.Pa(),re(this.j.u)))};
function zg(a){var b=a.Ma;hc(a.ma);b=b.replace(/\s/g,"\u00a0");C&&b&&(b+="\u200f");b||(b="\u00a0");a.ma.appendChild(document.createTextNode(b));a.kd.width=0}g.ic=function(){return this.Ea()};g.bb=function(a){this.T(a)};g.yg=function(a){if(!Kb&&!Lb||0===a.layerX||0===a.layerY)vd(a)||2!=Be&&this.j.fc&&!F&&this.sf()};g.B=function(){};function Ag(a){this.qh=a}ca(Ag);g=Ag.prototype;g.Ae=function(){return this.qh};function Bg(a,b){a&&(a.tabIndex=b?0:-1)}g.I=function(a){return a.tb().I("div",this.De(a).join(" "))};g.sb=function(a){return a};g.Md=function(a){a=a.k();Se(a,!0,Gb);v&&(a.hideFocus=!0);var b=this.Ae();b&&tf(a,b)};g.ra=function(a){return a.k()};g.Da=function(){return"goog-container"};g.De=function(a){var b=this.Da(),c=[b,a.fd==Cg?b+"-horizontal":b+"-vertical"];a.isEnabled()||c.push(b+"-disabled");return c};function Dg(){}s(Dg,vf);ca(Dg);Dg.prototype.I=function(a){return a.tb().I("div",this.Da())};Dg.prototype.Da=function(){return"goog-menuseparator"};function Eg(a,b){N.call(this,null,a||Dg.hc(),b);this.ab(1,!1);this.ab(2,!1);this.ab(4,!1);this.ab(32,!1);this.ga=1}s(Eg,N);Eg.prototype.va=function(){Eg.m.va.call(this);var a=this.k();tf(a,"separator")};qf("goog-menuseparator",function(){return new Eg});function Fg(a){this.qh=a||"menu"}s(Fg,Ag);ca(Fg);Fg.prototype.Da=function(){return"goog-menu"};Fg.prototype.Md=function(a){Fg.m.Md.call(this,a);a=a.k();uf(a,"haspopup","true")};qf("goog-menuseparator",function(){return new Eg});function Gg(a,b,c){Ze.call(this,c);this.M=b||Ag.hc();this.fd=a||Hg}s(Gg,Ze);var Cg="horizontal",Hg="vertical";g=Gg.prototype;g.qg=null;g.Fa=null;g.M=null;g.fd=null;g.na=!0;g.Ac=!0;g.ag=!0;g.R=-1;g.ea=null;g.ed=!1;g.oj=!1;g.Mk=!0;g.Ob=null;g.ra=function(){return this.qg||this.M.ra(this)};g.Ge=function(){return this.Fa||(this.Fa=new Gf(this.ra()))};g.I=function(){this.w=this.M.I(this)};g.sb=function(){return this.M.sb(this.k())};
g.va=function(){Gg.m.va.call(this);ef(this,function(a){a.G&&Ig(this,a)},this);var a=this.k();this.M.Md(this);this.N(this.na,!0);cf(this).H(this,"enter",this.gg).H(this,"highlight",this.ck).H(this,"unhighlight",this.hk).H(this,"open",this.fk).H(this,"close",this.$j).H(a,"mousedown",this.ad).H($b(a),"mouseup",this.ak).H(a,["mousedown","mouseup","mouseover","mouseout","contextmenu"],this.Zj);this.kc()&&Jg(this,!0)};
function Jg(a,b){var c=cf(a),d=a.ra();b?c.H(d,"focus",a.Je).H(d,"blur",a.Hd).H(a.Ge(),"key",a.ub):c.kb(d,"focus",a.Je).kb(d,"blur",a.Hd).kb(a.Ge(),"key",a.ub)}g.hb=function(){this.jd(-1);this.ea&&Rf(this.ea,!1);this.ed=!1;Gg.m.hb.call(this)};g.ba=function(){Gg.m.ba.call(this);this.Fa&&(this.Fa.i(),this.Fa=null);this.M=this.ea=this.Ob=this.qg=null};g.gg=function(){return!0};
g.ck=function(a){var b=jf(this,a.target);if(-1<b&&b!=this.R){var c=M(this,this.R);c&&c.yb(!1);this.R=b;c=M(this,this.R);this.ed&&c.setActive(!0);this.Mk&&this.ea&&c!=this.ea&&(c.$&64?Rf(c,!0):Rf(this.ea,!1))}b=this.k();null!=a.target.k()&&uf(b,"activedescendant",a.target.k().id)};g.hk=function(a){a.target==M(this,this.R)&&(this.R=-1);this.k().removeAttribute("aria-activedescendant")};g.fk=function(a){(a=a.target)&&a!=this.ea&&a.getParent()==this&&(this.ea&&Rf(this.ea,!1),this.ea=a)};
g.$j=function(a){a.target==this.ea&&(this.ea=null)};g.ad=function(a){this.Ac&&(this.ed=!0);var b=this.ra();b&&mc(b)&&nc(b)?b.focus():a.preventDefault()};g.ak=function(){this.ed=!1};g.Zj=function(a){var b=Kg(this,a.target);if(b)switch(a.type){case "mousedown":b.ad(a);break;case "mouseup":b.Kd(a);break;case "mouseover":b.jg(a);break;case "mouseout":b.ig(a);break;case "contextmenu":b.Id(a)}};
function Kg(a,b){if(a.Ob)for(var c=a.k();b&&b!==c;){var d=b.id;if(d in a.Ob)return a.Ob[d];b=b.parentNode}return null}g.Je=function(){};g.Hd=function(){this.jd(-1);this.ed=!1;this.ea&&Rf(this.ea,!1)};g.ub=function(a){return this.isEnabled()&&this.A()&&(0!=ff(this)||this.qg)&&this.Ec(a)?(a.preventDefault(),a.stopPropagation(),!0):!1};
g.Ec=function(a){var b=M(this,this.R);if(b&&"function"==typeof b.ub&&b.ub(a)||this.ea&&this.ea!=b&&"function"==typeof this.ea.ub&&this.ea.ub(a))return!0;if(a.shiftKey||a.ctrlKey||a.metaKey||a.altKey)return!1;switch(a.keyCode){case 27:if(this.kc())this.ra().blur();else return!1;break;case 36:Lg(this);break;case 35:Mg(this);break;case 38:if(this.fd==Hg)Ng(this);else return!1;break;case 37:if(this.fd==Cg)gf(this)?Og(this):Ng(this);else return!1;break;case 40:if(this.fd==Hg)Og(this);else return!1;break;
case 39:if(this.fd==Cg)gf(this)?Ng(this):Og(this);else return!1;break;default:return!1}return!0};function Ig(a,b){var c=b.k(),c=c.id||(c.id=bf(b));a.Ob||(a.Ob={});a.Ob[c]=b}g.pe=function(a,b){Gg.m.pe.call(this,a,b)};
g.Uc=function(a,b,c){a.$d|=2;a.$d|=64;!this.kc()&&this.oj||a.ab(32,!1);a.G&&0!=a.Jd&&Of(a,!1);a.Jd=!1;var d=a.getParent()==this?jf(this,a):-1;Gg.m.Uc.call(this,a,b,c);a.G&&this.G&&Ig(this,a);a=d;-1==a&&(a=ff(this));a==this.R?this.R=Math.min(ff(this)-1,b):a>this.R&&b<=this.R?this.R++:a<this.R&&b>this.R&&this.R--};
g.removeChild=function(a,b){if(a=n(a)?df(this,a):a){var c=jf(this,a);-1!=c&&(c==this.R?(a.yb(!1),this.R=-1):c<this.R&&this.R--);var d=a.k();d&&d.id&&this.Ob&&(c=this.Ob,d=d.id,d in c&&delete c[d])}c=a=Gg.m.removeChild.call(this,a,b);c.G&&1!=c.Jd&&Of(c,!0);c.Jd=!0;return a};g.A=function(){return this.na};
g.N=function(a,b){if(b||this.na!=a&&this.dispatchEvent(a?"show":"hide")){this.na=a;var c=this.k();c&&(Qe(c,a),this.kc()&&Bg(this.ra(),this.Ac&&this.na),b||this.dispatchEvent(this.na?"aftershow":"afterhide"));return!0}return!1};g.isEnabled=function(){return this.Ac};
g.Wd=function(a){this.Ac!=a&&this.dispatchEvent(a?"enable":"disable")&&(a?(this.Ac=!0,ef(this,function(a){a.Xi?delete a.Xi:a.Wd(!0)})):(ef(this,function(a){a.isEnabled()?a.Wd(!1):a.Xi=!0}),this.ed=this.Ac=!1),this.kc()&&Bg(this.ra(),a&&this.na))};g.kc=function(){return this.ag};g.Pc=function(a){a!=this.ag&&this.G&&Jg(this,a);this.ag=a;this.Ac&&this.na&&Bg(this.ra(),a)};g.jd=function(a){(a=M(this,a))?a.yb(!0):-1<this.R&&M(this,this.R).yb(!1)};g.yb=function(a){this.jd(jf(this,a))};
function Lg(a){Pg(a,function(a,c){return(a+1)%c},ff(a)-1)}function Mg(a){Pg(a,function(a,c){a--;return 0>a?c-1:a},0)}function Og(a){Pg(a,function(a,c){return(a+1)%c},a.R)}function Ng(a){Pg(a,function(a,c){a--;return 0>a?c-1:a},a.R)}function Pg(a,b,c){c=0>c?jf(a,a.ea):c;var d=ff(a);c=b.call(a,c,d);for(var e=0;e<=d;){var f=M(a,c);if(f&&a.wh(f)){a.jd(c);break}e++;c=b.call(a,c,d)}}g.wh=function(a){return a.A()&&a.isEnabled()&&!!(a.$&2)};function Qg(){}s(Qg,vf);ca(Qg);Qg.prototype.Da=function(){return"goog-menuheader"};function Rg(a,b,c){N.call(this,a,c||Qg.hc(),b);this.ab(1,!1);this.ab(2,!1);this.ab(4,!1);this.ab(32,!1);this.ga=1}s(Rg,N);qf("goog-menuheader",function(){return new Rg(null)});function Sg(a,b){Gg.call(this,Hg,b||Fg.hc(),a);this.Pc(!1)}s(Sg,Gg);g=Sg.prototype;g.Df=!0;g.pj=!1;g.Da=function(){return this.M.Da()};g.removeItem=function(a){(a=this.removeChild(a,!0))&&a.i()};function Tg(a){a.Df=!0;a.Pc(!0)}g.N=function(a,b,c){(b=Sg.m.N.call(this,a,b))&&a&&this.G&&this.Df&&this.ra().focus();this.xi=a&&c&&ga(c.clientX)?new x(c.clientX,c.clientY):null;return b};g.gg=function(a){this.Df&&this.ra().focus();return Sg.m.gg.call(this,a)};
g.wh=function(a){return(this.pj||a.isEnabled())&&a.A()&&!!(a.$&2)};g.Ec=function(a){var b=Sg.m.Ec.call(this,a);b||ef(this,function(c){!b&&c.Xj&&c.oi==a.keyCode&&(this.isEnabled()&&this.yb(c),b=c.ub(a))},this);return b};
g.jd=function(a){Sg.m.jd.call(this,a);if(a=M(this,a)){var b=a.k();a=this.k();var c=Me(b),d=Me(a),e=Ve(a),f=c.x-d.x-e.left,c=c.y-d.y-e.top,d=a.clientHeight-b.offsetHeight,e=a.scrollLeft,h=a.scrollTop,e=e+Math.min(f,Math.max(f-(a.clientWidth-b.offsetWidth),0)),h=h+Math.min(c,Math.max(c-d,0)),b=new x(e,h);a.scrollLeft=b.x;a.scrollTop=b.y}};/*

 Visual Blocks Editor

 Copyright 2012 Google Inc.
 https://developers.google.com/blockly/

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
function P(a,b){this.dd=a;this.Ia=b;Ug(this);var c=Vg(this)[0];this.Na=c[1];this.qe=H("tspan",{},null);this.qe.appendChild(document.createTextNode(C?Wg+" ":" "+Wg));P.m.constructor.call(this,c[0])}s(P,yg);var Wg=Jb?"\u25bc":"\u25be";g=P.prototype;g.clone=function(){return new P(this.dd,this.Ia)};g.dh="default";
g.sf=function(){Xg(this,null);for(var a=this,b=new Sg,c=Vg(this),d=0;d<c.length;d++){var e=c[d][1],f=new Zf(c[d][0]);f.bb(e);f.Lg(!0);b.pe(f,!0);Qf(f,e==this.Na)}Wd(b,"action",function(b){if(b=b.target){b=b.ic();if(a.Ia){var c=a.Ia(b);void 0!==c&&(b=c)}null!==b&&a.bb(b)}Yg==a&&Zg()});cf(b).H(b.k(),"touchstart",function(a){Kg(this,a.target).ad(a)});cf(b).H(b.k(),"touchend",function(a){Kg(this,a.target).Rd(a)});c=cc();d=Je();e=$g(this.wc);f=this.wc.getBBox();b.v(ah);var h=b.k();dg(h,"blocklyDropdownMenu");
var k=Ne(h);e.y=e.y+k.height+f.height>=c.height+d.y?e.y-k.height:e.y+f.height;C?(e.x+=f.width,e.x+=25,e.x<d.x+k.width&&(e.x=d.x+k.width)):(e.x-=25,e.x>c.width+d.x-k.width&&(e.x=c.width+d.x-k.width));bh(e.x,e.y,c,d);Tg(b);h.focus()};
function Ug(a){a.Bg=null;a.Wg=null;var b=a.dd;if(ea(b)&&!(2>b.length)){var c=b.map(function(a){return a[0]}),d=ch(c),e=dh(c,d),f=eh(c,d);if((e||f)&&!(d<=e+f)){e&&(a.Bg=c[0].substring(0,e-1));f&&(a.Wg=c[0].substr(1-f));c=[];for(d=0;d<b.length;d++){var h=b[d][0],k=b[d][1],h=h.substring(e,h.length-f);c[d]=[h,k]}a.dd=c}}}function Vg(a){return r(a.dd)?a.dd.call(a):a.dd}g.ic=function(){return this.Na};g.bb=function(a){this.Na=a;for(var b=Vg(this),c=0;c<b.length;c++)if(b[c][1]==a){this.T(b[c][0]);return}this.T(a)};
g.T=function(a){this.j&&(this.qe.style.fill=mg(ng(this.j.Lf)));null!==a&&a!==this.Ma&&(this.Ma=a,zg(this),C?this.ma.insertBefore(this.qe,this.ma.firstChild):this.ma.appendChild(this.qe),this.j&&this.j.L&&(this.j.v(),this.j.Pa(),re(this.j.u)))};g.i=function(){Yg==this&&Zg();P.m.i.call(this)};/*

 Visual Blocks Editor

 Copyright 2013 Google Inc.
 https://developers.google.com/blockly/

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
function fh(a,b){a.innerHTML=qb(b)};function gh(a,b,c){Ze.call(this,c);this.ua=b||hh;this.kg=a instanceof ob?a:sb(a,null)}s(gh,Ze);var ih={};g=gh.prototype;g.Jg=!1;g.zd=!1;g.kl=null;g.nj=zb;g.Od=!0;g.we=-1;g.ba=function(){gh.m.ba.call(this);this.Tc&&(this.Tc.removeNode(this),this.Tc=null);this.w=null};
g.Pe=function(){var a=this.k();if(a){var b=jh(this);b&&!b.id&&(b.id=bf(this)+".label");tf(a,"treeitem");uf(a,"selected",!1);uf(a,"expanded",!1);uf(a,"level",this.Yc());b&&uf(a,"labelledby",b.id);(a=this.Fe())&&tf(a,"presentation");(a=this.Ee())&&tf(a,"presentation");if(a=kh(this))if(tf(a,"group"),a.hasChildNodes())for(a=ff(this),b=1;b<=a;b++){var c=M(this,b-1).k();uf(c,"setsize",a);uf(c,"posinset",b)}}};
g.I=function(){var a=this.tb(),b=qb(this.Yg());var c=a.Rb,a=c.createElement("div");v?(a.innerHTML="<br>"+b,a.removeChild(a.firstChild)):a.innerHTML=b;if(1==a.childNodes.length)b=a.removeChild(a.firstChild);else for(b=c.createDocumentFragment();a.firstChild;)b.appendChild(a.firstChild);this.w=b};g.va=function(){gh.m.va.call(this);ih[bf(this)]=this;this.Pe()};g.hb=function(){gh.m.hb.call(this);delete ih[bf(this)]};
g.Uc=function(a,b){var c=M(this,b-1),d=M(this,b);gh.m.Uc.call(this,a,b);a.Kc=c;a.xb=d;c?c.xb=a:this.Xh=a;d?d.Kc=a:this.ki=a;var e=this.Ja();e&&lh(a,e);mh(a,this.Yc()+1);if(this.k()&&(this.nd(),this.Ra())){e=kh(this);a.k()||a.I();var f=a.k(),h=d&&d.k();e.insertBefore(f,h);this.G&&a.va();d||(c?c.nd():(Qe(e,!0),this.Yb(this.Ra())))}};g.add=function(a,b){a.getParent()&&a.getParent().removeChild(a);this.Uc(a,b?jf(this,b):ff(this));return a};
g.removeChild=function(a){var b=this.Ja(),c=b?b.Za:null;if(c==a||a.contains(c))b.hasFocus()?(this.select(),ie(this.Kk,10,this)):this.select();gh.m.removeChild.call(this,a);this.ki==a&&(this.ki=a.Kc);this.Xh==a&&(this.Xh=a.xb);a.Kc&&(a.Kc.xb=a.xb);a.xb&&(a.xb.Kc=a.Kc);c=!a.xb;a.Tc=null;a.we=-1;if(b&&(b.removeNode(this),this.G)){b=kh(this);if(a.G){var d=a.k();b.removeChild(d);a.hb()}c&&(c=M(this,ff(this)-1))&&c.nd();hf(this)||(b.style.display="none",this.nd(),this.Fe().className=this.Be())}return a};
g.remove=gh.prototype.removeChild;g.Kk=function(){this.select()};g.Yc=function(){var a=this.we;0>a&&(a=(a=this.getParent())?a.Yc()+1:0,mh(this,a));return a};function mh(a,b){if(b!=a.we){a.we=b;var c=nh(a);if(c){var d=oh(a)+"px";gf(a)?c.style.paddingRight=d:c.style.paddingLeft=d}ef(a,function(a){mh(a,b+1)})}}g.contains=function(a){for(;a;){if(a==this)return!0;a=a.getParent()}return!1};g.Dc=function(){var a=[];ef(this,function(b){a.push(b)});return a};g.Re=function(){return this.Jg};
g.select=function(){var a=this.Ja();a&&a.Qc(this)};function ph(a,b){if(a.Jg!=b){a.Jg=b;qh(a);var c=a.k();c&&(uf(c,"selected",b),b&&(c=a.Ja().k(),uf(c,"activedescendant",bf(a))))}}g.Ra=function(){return this.zd};
g.Yb=function(a){var b=a!=this.zd;if(!b||this.dispatchEvent(a?"beforeexpand":"beforecollapse")){var c;this.zd=a;c=this.Ja();var d=this.k();if(hf(this)){if(!a&&c&&this.contains(c.Za)&&this.select(),d){if(c=kh(this))if(Qe(c,a),a&&this.G&&!c.hasChildNodes()){var e=[];ef(this,function(a){e.push(a.Yg())});fh(c,yb(e));ef(this,function(a){a.va()})}this.nd()}}else(c=kh(this))&&Qe(c,!1);d&&(this.Fe().className=this.Be(),uf(d,"expanded",a));b&&this.dispatchEvent(a?"expand":"collapse")}};g.toggle=function(){this.Yb(!this.Ra())};
g.expand=function(){this.Yb(!0)};g.collapse=function(){this.Yb(!1)};g.Hg=function(){var a=this.getParent();a&&(a.Yb(!0),a.Hg())};g.Yg=function(){var a=this.Ja(),b=!a.Yd||a==this.getParent()&&!a.Pg?this.ua.Bh:this.ua.Ah,a=this.Ra()&&hf(this),b={"class":b,style:rh(this)},c=[];a&&ef(this,function(a){c.push(a.Yg())});a=xb("div",b,c);return xb("div",{"class":this.ua.Jh,id:bf(this)},[sh(this),a])};function oh(a){return Math.max(0,(a.Yc()-1)*a.ua.og)}
function sh(a){var b={};b["padding-"+(gf(a)?"right":"left")]=oh(a)+"px";var b={"class":a.Fd(),style:b},c=a.dg(),d=xb("span",{style:{display:"inline-block"},"class":a.Be()}),e=xb("span",{"class":a.ua.Kh,title:a.kl||null},a.kg);a=yb(e,xb("span",{},a.nj));return xb("div",b,[c,d,a])}g.Fd=function(){return this.ua.Qf+(this.Re()?" "+this.ua.Mh:"")};g.dg=function(){return xb("span",{type:"expand",style:{display:"inline-block"},"class":th(this)})};
function th(a){var b=a.Ja(),c=!b.Yd||b==a.getParent()&&!b.Pg,d=a.ua,e=new qa;e.append(d.zc," ",d.Dj," ");if(hf(a)){var f=0;b.Og&&a.Od&&(f=a.Ra()?2:1);c||(f=a.xb?f+8:f+4);switch(f){case 1:e.append(d.Hj);break;case 2:e.append(d.Gj);break;case 4:e.append(d.Fh);break;case 5:e.append(d.Fj);break;case 6:e.append(d.Ej);break;case 8:e.append(d.Gh);break;case 9:e.append(d.Jj);break;case 10:e.append(d.Ij);break;default:e.append(d.Eh)}}else c?e.append(d.Eh):a.xb?e.append(d.Gh):e.append(d.Fh);return e.toString()}
function rh(a){var b=a.Ra()&&hf(a);return fb({"background-position":uh(a),display:b?null:"none"})}function uh(a){return(a.xb?(a.Yc()-1)*a.ua.og:"-100")+"px 0"}g.k=function(){var a=gh.m.k.call(this);a||(this.w=a=this.tb().k(bf(this)));return a};function nh(a){return(a=a.k())?a.firstChild:null}g.Ee=function(){var a=nh(this);return a?a.firstChild:null};g.Fe=function(){var a=nh(this);return a?a.childNodes[1]:null};function jh(a){return(a=nh(a))&&a.lastChild?a.lastChild.previousSibling:null}
function kh(a){return(a=a.k())?a.lastChild:null}g.T=function(a){this.kg=a=rb(a);var b=jh(this);b&&fh(b,a);(a=this.Ja())&&vh(a,this)};g.Ea=function(){var a=qb(this.kg);return-1!=a.indexOf("&")?"document"in l?Ga(a):Ia(a):a};function qh(a){var b=nh(a);b&&(b.className=a.Fd())}g.nd=function(){var a=this.Ee();a&&(a.className=th(this));if(a=kh(this))a.style.backgroundPosition=uh(this)};g.wg=function(a){"expand"==a.target.getAttribute("type")&&hf(this)?this.Od&&this.toggle():(this.select(),qh(this))};
g.qi=function(a){"expand"==a.target.getAttribute("type")&&hf(this)||this.Od&&this.toggle()};function wh(a){return a.Ra()&&hf(a)?wh(M(a,ff(a)-1)):a}function lh(a,b){a.Tc!=b&&(a.Tc=b,vh(b,a),ef(a,function(a){lh(a,b)}))}
var hh={og:19,Lh:"goog-tree-root goog-tree-item",Ih:"goog-tree-hide-root",Jh:"goog-tree-item",Ah:"goog-tree-children",Bh:"goog-tree-children-nolines",Qf:"goog-tree-row",Kh:"goog-tree-item-label",zc:"goog-tree-icon",Dj:"goog-tree-expand-icon",Hj:"goog-tree-expand-icon-plus",Gj:"goog-tree-expand-icon-minus",Jj:"goog-tree-expand-icon-tplus",Ij:"goog-tree-expand-icon-tminus",Fj:"goog-tree-expand-icon-lplus",Ej:"goog-tree-expand-icon-lminus",Gh:"goog-tree-expand-icon-t",Fh:"goog-tree-expand-icon-l",Eh:"goog-tree-expand-icon-blank",
Of:"goog-tree-expanded-folder-icon",Ch:"goog-tree-collapsed-folder-icon",Pf:"goog-tree-file-icon",Hh:"goog-tree-expanded-folder-icon",Dh:"goog-tree-collapsed-folder-icon",Mh:"selected"};function xh(a,b,c){gh.call(this,a,b,c)}s(xh,gh);xh.prototype.Ja=function(){if(this.Tc)return this.Tc;var a=this.getParent();return a&&(a=a.Ja())?(lh(this,a),a):null};xh.prototype.Be=function(){var a=this.Ra(),b=this.Sj;if(a&&b)return b;b=this.jk;if(!a&&b)return b;b=this.ua;if(hf(this)){if(a&&b.Of)return b.zc+" "+b.Of;if(!a&&b.Ch)return b.zc+" "+b.Ch}else if(b.Pf)return b.zc+" "+b.Pf;return""};/*

 Visual Blocks Editor

 Copyright 2011 Google Inc.
 https://developers.google.com/blockly/

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
var yh={ve:null,show:function(a,b){Xg(yh,null);if(b.length){for(var c=new Sg,d=0,e;e=b[d];d++){var f=new Zf(e.text);c.pe(f,!0);f.Wd(e.enabled);e.enabled&&Wd(f,"action",function(a){return function(){a()}}(e.pb))}Wd(c,"action",yh.Ub);e=cc();f=Je();c.v(ah);var h=c.k();dg(h,"blocklyContextMenu");var k=Ne(h),d=a.clientX+f.x,m=a.clientY+f.y;a.clientY+k.height>=e.height&&(m-=k.height);C?k.width>=a.clientX&&(d+=k.width):a.clientX+k.width>=e.width&&(d-=k.width);bh(d,m,e,f);Tg(c);setTimeout(function(){h.focus()},
1);yh.ve=null}else yh.Ub()},Ub:function(){Yg==yh&&Zg();yh.ve=null},Tl:function(a,b){return function(){var c=bd(a.u,b),d=D(a);d.x=C?d.x-zh:d.x+zh;d.y+=2*zh;c.moveBy(d.x,d.y);c.select()}}};/*

 Visual Blocks Editor

 Copyright 2012 Google Inc.
 https://developers.google.com/blockly/

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
function Ah(a,b,c,d,e,f,h){var k=Bh;C&&(k=-k);this.rj=k*Math.PI/180;this.s=a;this.Qb=b;this.Ni=c;a.ld.appendChild(this.Mf(b,!(!f||!h)));Ch(this,d,e);f&&h||(a=this.Qb.getBBox(),f=a.width+2*Dh,h=a.height+2*Dh);this.rc(f,h);Eh(this);Fh(this);this.Gg=!0;F||(K(this.re,"mousedown",this,this.vj),this.Wb&&K(this.Wb,"mousedown",this,this.Uk))}var Dh=6,Bh=20,Gh=null,Hh=null;function Ih(){Gh&&(J(Gh),Gh=null);Hh&&(J(Hh),Hh=null)}g=Ah.prototype;g.Gg=!1;g.mb=0;g.Gf=0;g.Mc=0;g.Td=0;g.O=0;g.ib=0;g.Jf=!0;
g.Mf=function(a,b){this.ob=H("g",{},null);var c=H("g",{filter:"url(#blocklyEmboss)"},this.ob);this.sh=H("path",{},c);this.re=H("rect",{"class":"blocklyDraggable",x:0,y:0,rx:Dh,ry:Dh},c);b?(this.Wb=H("g",{"class":C?"blocklyResizeSW":"blocklyResizeSE"},this.ob),c=2*Dh,H("polygon",{points:"0,x x,x x,0".replace(/x/g,c.toString())},this.Wb),H("line",{"class":"blocklyResizeLine",x1:c/3,y1:c-1,x2:c-1,y2:c/3},this.Wb),H("line",{"class":"blocklyResizeLine",x1:2*c/3,y1:c-1,x2:c-1,y2:2*c/3},this.Wb)):this.Wb=
null;this.ob.appendChild(a);return this.ob};g.vj=function(a){Jh(this);Ih();vd(a)||Kh(a)||(Lh(!0),this.Th=C?this.Mc+a.clientX:this.Mc-a.clientX,this.Qj=this.Td-a.clientY,Gh=K(document,"mouseup",this,Ih),Hh=K(document,"mousemove",this,this.wj),zd(),a.stopPropagation())};g.wj=function(a){this.Jf=!1;this.Mc=C?this.Th-a.clientX:this.Th+a.clientX;this.Td=this.Qj+a.clientY;Eh(this);Fh(this)};
g.Uk=function(a){Jh(this);Ih();vd(a)||(Lh(!0),this.Tk=C?this.O+a.clientX:this.O-a.clientX,this.Sk=this.ib-a.clientY,Gh=K(document,"mouseup",this,Ih),Hh=K(document,"mousemove",this,this.Vk),zd(),a.stopPropagation())};g.Vk=function(a){this.Jf=!1;var b=this.Tk,c=this.Sk+a.clientY,b=C?b-a.clientX:b+a.clientX;this.rc(b,c);C&&Eh(this)};function Jh(a){a.ob.parentNode.appendChild(a.ob)}function Ch(a,b,c){a.mb=b;a.Gf=c;a.Gg&&Eh(a)}
function Eh(a){a.ob.setAttribute("transform","translate("+(C?a.mb-a.Mc-a.O:a.mb+a.Mc)+", "+(a.Td+a.Gf)+")")}g.Cc=function(){return{width:this.O,height:this.ib}};
g.rc=function(a,b){var c=2*Dh;a=Math.max(a,c+45);b=Math.max(b,c+18);this.O=a;this.ib=b;this.re.setAttribute("width",a);this.re.setAttribute("height",b);this.Wb&&(C?this.Wb.setAttribute("transform","translate("+2*Dh+", "+(b-c)+") scale(-1 1)"):this.Wb.setAttribute("transform","translate("+(a-c)+", "+(b-c)+")"));if(this.Gg){if(this.Jf){var c=-this.O/4,d=-this.ib-25,e=this.s.Tb();C?this.mb-e.Oa-c-this.O<I?c=this.mb-e.Oa-this.O-I:this.mb-e.Oa-c>e.V&&(c=this.mb-e.Oa-e.V):this.mb+c<e.Oa?c=e.Oa-this.mb:
e.Oa+e.V<this.mb+c+this.O+10+I&&(c=e.Oa+e.V-this.mb-this.O-I);this.Gf+d<e.Lb&&(d=this.Ni.getBBox().height);this.Mc=c;this.Td=d}Eh(this);Fh(this)}De(this.ob,"resize")};
function Fh(a){var b=[],c=a.O/2,d=a.ib/2,e=-a.Mc,f=-a.Td;if(c==e&&d==f)b.push("M "+c+","+d);else{f-=d;e-=c;C&&(e*=-1);var h=Math.sqrt(f*f+e*e),k=Math.acos(e/h);0>f&&(k=2*Math.PI-k);var m=k+Math.PI/2;m>2*Math.PI&&(m-=2*Math.PI);var p=Math.sin(m),q=Math.cos(m),u=a.Cc(),m=(u.width+u.height)/10,m=Math.min(m,u.width,u.height)/2,u=1-8/h,e=c+u*e,f=d+u*f,u=c+m*q,t=d+m*p,c=c-m*q,d=d-m*p,p=k+a.rj;p>2*Math.PI&&(p-=2*Math.PI);k=Math.sin(p)*h/4;h=Math.cos(p)*h/4;b.push("M"+u+","+t);b.push("C"+(u+h)+","+(t+k)+
" "+e+","+f+" "+e+","+f);b.push("C"+e+","+f+" "+(c+h)+","+(d+k)+" "+c+","+d)}b.push("z");a.sh.setAttribute("d",b.join(" "))}g.C=function(a){this.re.setAttribute("fill",a);this.sh.setAttribute("fill",a)};g.i=function(){Ih();z(this.ob);this.Ni=this.Qb=this.s=this.ob=null};/*

 Visual Blocks Editor

 Copyright 2013 Google Inc.
 https://developers.google.com/blockly/

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
function Mh(a){this.g=a}g=Mh.prototype;g.W=null;g.Fc=0;g.Gc=0;g.yc=function(){this.xa=H("g",{},null);O(this.g).appendChild(this.xa);K(this.xa,"mouseup",this,this.lg);this.Zb()};g.i=function(){z(this.xa);this.xa=null;this.N(!1);this.g=null};g.Zb=function(){this.g.lc?eg(this.xa,"blocklyIconGroup"):dg(this.xa,"blocklyIconGroup")};g.A=function(){return!!this.W};g.lg=function(){this.g.lc||this.N(!this.A())};g.tc=function(){if(this.A()){var a=mg(ng(this.g.Lf));this.W.C(a)}};
function ug(a){var b=D(a.g),c=Nh(a.xa),d=b.x+c.x+8,b=b.y+c.y+8;if(d!==a.Fc||b!==a.Gc)a.Fc=d,a.Gc=b,a.A()&&Ch(a.W,d,b)};/*

 Visual Blocks Editor

 Copyright 2012 Google Inc.
 https://developers.google.com/blockly/

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
function Oh(a){Oh.m.constructor.call(this,null);this.Ai=[];for(var b=0;b<a.length;b++){var c=dc("block",{type:a[b]});this.Ai[b]=c}}s(Oh,Mh);g=Oh.prototype;g.od=0;g.ie=0;function cg(a){Mh.prototype.yc.call(a);H("rect",{"class":"blocklyIconShield",width:16,height:16,rx:4,ry:4},a.xa);a.Ld=H("text",{"class":"blocklyIconMark",x:8,y:12},a.xa);a.Ld.appendChild(document.createTextNode("\u2605"))}g.lg=function(a){this.g.fc&&!F&&Mh.prototype.lg.call(this,a)};
g.Nf=function(){this.Sc=H("svg",{x:Dh,y:Dh},null);H("rect",{"class":"blocklyMutatorBackground",height:"100%",width:"100%"},this.Sc);var a=this;this.s=new pe(function(){var b=0;C&&(b+=a.od);return{za:a.ie,V:0,gb:0,fb:b}},null);this.qa=new Ph;this.qa.ud=!1;this.Sc.appendChild(this.qa.I());this.Sc.appendChild(this.s.I());return this.Sc};g.Zb=function(){this.g.fc&&!F?Mh.prototype.Zb.call(this):(this.N(!1),eg(this.xa,"blocklyIconGroup"))};
g.pf=function(){var a=2*Dh,b=this.s.Q.getBBox(),c=Qh(this.qa),d;d=C?-b.x:b.width+b.x;b=Math.max(b.height+3*a,c.Va+20);d+=3*a;if(Math.abs(this.od-d)>a||Math.abs(this.ie-b)>a)this.od=d,this.ie=b,this.W.rc(d+a,b+a),this.Sc.setAttribute("width",this.od),this.Sc.setAttribute("height",this.ie);C&&this.s.Q.setAttribute("transform","translate("+this.od+",0)")};
g.N=function(a){if(a!=this.A())if(a){this.W=new Ah(this.g.u,this.Nf(),this.g.n.Kb,this.Fc,this.Gc,null,null);var b=this;this.qa.o(this.s);this.qa.show(this.Ai);this.Xb=this.g.Lj(this.s);a=Rh(this.Xb);for(var c=0,d;d=a[c];c++)d.v();this.Xb.Hb=!1;hd(this.Xb,!1);a=2*this.qa.sa;c=this.qa.O+a;C&&(c=-c);this.Xb.moveBy(c,a);this.g.Ig&&(this.g.Ig(this.Xb),this.Qg=K(this.g.u.Q,"blocklyWorkspaceChange",this.g,function(){b.g.Ig(b.Xb)}));this.pf();K(this.s.Q,"blocklyWorkspaceChange",this.g,function(){if(0==Be)for(var a=
Wc(b.s,!1),c=0,d;d=a[c];c++){var k=D(d),m=vg(d);d.ec&&!F&&(C?k.x>-b.qa.O+20:k.x<b.qa.O-20)?d.i(!1,!0):20>k.y+m.height&&d.moveBy(0,20-m.height-k.y)}b.Xb.u==b.s&&(a=b.g.L,b.g.L=!1,b.g.zj(b.Xb),b.g.L=a,b.g.L&&b.g.v(),b.pf(),re(b.g.u))});this.tc()}else this.Sc=null,this.qa.i(),this.qa=null,this.s.i(),this.Xb=this.s=null,this.W.i(),this.W=null,this.ie=this.od=0,this.Qg&&(J(this.Qg),this.Qg=null)};g.i=function(){this.g.wb=null;Mh.prototype.i.call(this)};/*

 Visual Blocks Editor

 Copyright 2011 Google Inc.
 https://developers.google.com/blockly/

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
function Sh(a,b){this.j=a;this.t=null;this.type=b;this.U=this.Ab=0;this.Fb=!1;this.dc=this.j.u.Bj}g=Sh.prototype;g.i=function(){if(this.t)throw"Disconnect connection before disposing of it.";this.Fb&&Th(this.dc[this.type],this);this.Fb=!1;Uh==this&&(Uh=null);Vh==this&&(Vh=null)};function Wh(a){return 1==a.type||3==a.type}
function md(a,b){if(a.j==b.j)throw"Attempted to connect a block to itself.";if(a.j.u!==b.j.u)throw"Blocks are on different workspaces.";if(Xh[a.type]!=b.type)throw"Attempt to connect incompatible types.";if(1==a.type||2==a.type){if(a.t)throw"Source connection already connected (value).";if(b.t){var c=E(b);c.$a(null);if(!c.K)throw"Orphan block does not have an output connection.";for(var d=a.j;d=Yh(d,c);)if(E(d))d=E(d);else{md(d,c.K);c=null;break}c&&window.setTimeout(function(){Zh(c.K,b)},$h)}}else{if(a.t)throw"Source connection already connected (block).";
if(b.t){if(4!=a.type)throw"Can only do a mid-stack connection with the top of a block.";c=E(b);c.$a(null);if(!c.F)throw"Orphan block does not have a previous connection.";for(d=a.j;d.D;)if(d.D.t)d=Yc(d);else{ai(c.F,d.D)&&(md(d.D,c.F),c=null);break}c&&window.setTimeout(function(){Zh(c.F,b)},$h)}}var e;Wh(a)?(d=a.j,e=b.j):(d=b.j,e=a.j);a.t=b;b.t=a;e.$a(d);d.L&&rg(d.n);e.L&&rg(e.n);d.L&&e.L&&(3==a.type||4==a.type?e.v():d.v())}
function Yh(a,b){for(var c=!1,d=0;d<a.S.length;d++){var e=a.S[d].p;if(e&&1==e.type&&ai(b.K,e)){if(c)return null;c=e}}return c}g.disconnect=function(){var a=this.t;if(!a)throw"Source connection not connected.";if(a.t!=this)throw"Target connection not connected to source connection.";this.t=a.t=null;var b;Wh(this)?(b=this.j,a=a.j):(b=a.j,a=this.j);b.L&&b.v();a.L&&(rg(a.n),a.v())};function E(a){return a.t?a.t.j:null}
function Zh(a,b){if(0==Be){var c=bi(a.j);if(!c.lc){var d=!1;if(!c.Hb||F){c=bi(b.j);if(!c.Hb||F)return;b=a;d=!0}O(c).parentNode.appendChild(O(c));var e=b.Ab+zh-a.Ab,f=b.U+zh-a.U;d&&(f=-f);C&&(e=-e);c.moveBy(e,f)}}}g.moveTo=function(a,b){this.Fb&&Th(this.dc[this.type],this);this.Ab=a;this.U=b;ci(this.dc[this.type],this)};g.moveBy=function(a,b){this.moveTo(this.Ab+a,this.U+b)};
g.Le=function(){var a;1==this.type||2==this.type?(a=C?-8:8,a="m 0,0 v 5 c 0,10 "+-a+",-8 "+-a+",7.5 s "+a+",-2.5 "+a+",7.5 v 5"):a=C?"m 20,0 h -5 l -6,4 -3,0 -6,-4 h -5":"m -20,0 h 5 l 6,4 3,0 6,-4 h 5";var b=D(this.j);Sh.Me=H("path",{"class":"blocklyHighlightedConnectionPath",d:a,transform:"translate("+(this.Ab-b.x)+", "+(this.U-b.y)+")"},O(this.j))};
function xg(a){var b=Math.round(a.t.Ab-a.Ab),c=Math.round(a.t.U-a.U);if(0!=b||0!=c){a=E(a);var d=O(a);if(!d)throw"block is not rendered.";d=Nh(d);O(a).setAttribute("transform","translate("+(d.x-b)+", "+(d.y-c)+")");di(a,-b,-c)}}
function ei(a,b,c,d){function e(a){var c=f[a];if((2==c.type||4==c.type)&&c.t||1==c.type&&c.t&&(!E(c).Hb||F)||!ai(u,c))return!0;c=c.j;do{if(q==c)return!0;c=c.getParent()}while(c);var d=h-f[a].Ab,c=k-f[a].U,d=Math.sqrt(d*d+c*c);d<=b&&(p=f[a],b=d);return c<b}if(a.t)return{p:null,Bi:b};var f=a.dc[Xh[a.type]],h=a.Ab+c,k=a.U+d;c=0;for(var m=d=f.length-2;c<m;)f[m].U<k?c=m:d=m,m=Math.floor((c+d)/2);d=c=m;var p=null,q=a.j,u=a;if(f.length){for(;0<=c&&e(c);)c--;do d++;while(d<f.length&&e(d))}return{p:p,Bi:b}}
function ai(a,b){if(!a.Vc||!b.Vc)return!0;for(var c=0;c<a.Vc.length;c++)if(-1!=b.Vc.indexOf(a.Vc[c]))return!0;return!1}g.P=function(a){a?(ea(a)||(a=[a]),this.Vc=a,this.t&&!ai(this,this.t)&&(Wh(this)?E(this).$a(null):this.j.$a(null),this.j.Pa())):this.Vc=null;return this};
function fi(a){var b=zh;function c(a){var c=e-d[a].Ab,h=f-d[a].U;Math.sqrt(c*c+h*h)<=b&&m.push(d[a]);return h<b}var d=a.dc[Xh[a.type]],e=a.Ab,f=a.U;a=0;for(var h=d.length-2,k=h;a<k;)d[k].U<f?a=k:h=k,k=Math.floor((a+h)/2);var h=a=k,m=[];if(d.length){for(;0<=a&&c(a);)a--;do h++;while(h<d.length&&c(h))}return m}
function gi(a){a.Fb||ci(a.dc[a.type],a);var b=[];if(1!=a.type&&3!=a.type)return b;if(a=E(a)){var c;a.isCollapsed()?(c=[],a.K&&c.push(a.K),a.D&&c.push(a.D),a.F&&c.push(a.F)):c=hi(a,!0);for(var d=0;d<c.length;d++)b.push.apply(b,gi(c[d]));0==b.length&&(b[0]=a)}return b}function qe(){}qe.prototype=[];function ci(a,b){if(b.Fb)throw"Connection already in database.";for(var c=0,d=a.length;c<d;){var e=Math.floor((c+d)/2);if(a[e].U<b.U)c=e+1;else if(a[e].U>b.U)d=e;else{c=e;break}}a.splice(c,0,b);b.Fb=!0}
function Th(a,b){if(!b.Fb)throw"Connection not in database.";b.Fb=!1;for(var c=0,d=a.length-2,e=d;c<e;)a[e].U<b.U?c=e:d=e,e=Math.floor((c+d)/2);for(d=c=e;0<=c&&a[c].U==b.U;){if(a[c]==b){a.splice(c,1);return}c--}do{if(a[d]==b){a.splice(d,1);return}d++}while(d<a.length&&a[d].U==b.U);throw"Unable to find connection in connectionDB.";};/*

 Visual Blocks Editor

 Copyright 2013 Google Inc.
 https://developers.google.com/blockly/

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
var R={Ol:function(a){var b={o:function(){var b=this;this.C(a.Ul);this.J=a.J;"string"==typeof a.cb?this.B(a.cb):"function"==typeof a.cb&&this.B(function(){return a.cb(b)});"undefined"!=a.Nk?Q(this,a.Nk):(ii(this,"undefined"==typeof a.Pk?!0:a.Pk),ji(this,"undefined"==typeof a.Ck?!0:a.Ck));var d=[];d.push(a.text);a.qj&&a.qj.forEach(function(a){"undefined"==a.type||1==a.type?d.push([a.name,a.check,"undefined"==typeof a.align?1:a.align]):La("addTemplate() can only handle value inputs.")});d.push(1);a.ok&&
this.qm(a.ok);ki.prototype.vb.apply(this,d)}};b.Ze=a.sm?function(){var b=a.zk?a.fm():document.createElement("mutation");b.setAttribute("is_statement",this.isStatement||!1);return b}:a.zk;R[a.Sl]=b}};/*

 Visual Blocks Editor

 Copyright 2011 Google Inc.
 https://developers.google.com/blockly/

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
function li(a){li.m.constructor.call(this,a);this.yc()}s(li,Mh);g=li.prototype;g.Ma="";g.O=160;g.ib=80;g.yc=function(){Mh.prototype.yc.call(this);H("circle",{"class":"blocklyIconShield",r:8,cx:8,cy:8},this.xa);this.Ld=H("text",{"class":"blocklyIconMark",x:8,y:13},this.xa);this.Ld.appendChild(document.createTextNode("?"))};
g.Nf=function(){this.Bd=H("foreignObject",{x:Dh,y:Dh},null);var a=document.createElementNS("http://www.w3.org/1999/xhtml","body");a.setAttribute("xmlns","http://www.w3.org/1999/xhtml");a.className="blocklyMinimalBody";this.Ta=document.createElementNS("http://www.w3.org/1999/xhtml","textarea");this.Ta.className="blocklyCommentTextarea";this.Ta.setAttribute("dir",C?"RTL":"LTR");a.appendChild(this.Ta);this.Bd.appendChild(a);K(this.Ta,"mouseup",this,this.jl);return this.Bd};
g.Zb=function(){this.A()&&(this.N(!1),this.N(!0));Mh.prototype.Zb.call(this)};g.pf=function(){var a=this.W.Cc(),b=2*Dh;this.Bd.setAttribute("width",a.width-b);this.Bd.setAttribute("height",a.height-b);this.Ta.style.width=a.width-b-4+"px";this.Ta.style.height=a.height-b-4+"px"};
g.N=function(a){if(a!=this.A())if((!this.g.fc||F)&&!this.Ta||v)ni.prototype.N.call(this,a);else{var b=this.Ea(),c=this.Cc();a?(this.W=new Ah(this.g.u,this.Nf(),this.g.n.Kb,this.Fc,this.Gc,this.O,this.ib),K(this.W.ob,"resize",this,this.pf),this.tc(),this.Ma=null):(this.W.i(),this.Bd=this.Ta=this.W=null);this.T(b);this.rc(c.width,c.height)}};g.jl=function(){Jh(this.W);this.Ta.focus()};g.Cc=function(){return this.A()?this.W.Cc():{width:this.O,height:this.ib}};
g.rc=function(a,b){this.Ta?this.W.rc(a,b):(this.O=a,this.ib=b)};g.Ea=function(){return this.Ta?this.Ta.value:this.Ma};g.T=function(a){this.Ta?this.Ta.value=a:this.Ma=a};g.i=function(){this.g.ta=null;Mh.prototype.i.call(this)};/*

 Visual Blocks Editor

 Copyright 2011 Google Inc.
 https://developers.google.com/blockly/

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
var oi=!1,pi=0,qi=0,ri={x:0,y:0},si=null,ti=null,ui=null,vi=null,wi=null,xi=null;function yi(){var a=H("g",{"class":"blocklyHidden"},null);ui=a;xi=H("rect",{"class":"blocklyTooltipShadow",x:2,y:2},a);wi=H("rect",{"class":"blocklyTooltipBackground"},a);vi=H("text",{"class":"blocklyTooltipText"},a);return a}function ag(a){K(a,"mouseover",null,zi);K(a,"mouseout",null,Ai);K(a,"mousemove",null,Bi)}
function zi(a){for(a=a.target;!n(a.cb)&&!r(a.cb);)a=a.cb;si!=a&&(Ci(),ti=null,si=a);window.clearTimeout(pi)}function Ai(){pi=window.setTimeout(function(){ti=si=null;Ci()},1);window.clearTimeout(qi)}function Bi(a){si&&si.cb&&0==Be&&!Yg&&(oi?(a=wd(a),10<Math.sqrt(Math.pow(ri.x-a.x,2)+Math.pow(ri.y-a.y,2))&&Ci()):ti!=si&&(window.clearTimeout(qi),ri=wd(a),qi=window.setTimeout(Di,1E3)))}function Ci(){oi&&(oi=!1,ui&&(ui.style.display="none"));window.clearTimeout(qi)}
function Di(){ti=si;if(ui){hc(vi);var a=si.cb;r(a)&&(a=a());var b=a,a=50;if(b.length<=a)a=b;else{for(var c=b.trim().split(/\s+/),d=0;d<c.length;d++)c[d].length>a&&(a=c[d].length);var e,d=-Infinity,f,h=1;do{e=d;f=b;for(var b=[],k=c.length/h,m=1,d=0;d<c.length-1;d++)m<(d+1.5)/k?(m++,b[d]=!0):b[d]=!1;for(var b=Ei(c,b,a),d=Fi(c,b,a),k=c,m=[],p=0;p<k.length;p++)m.push(k[p]),void 0!==b[p]&&m.push(b[p]?"\n":" ");b=m.join("");h++}while(d>e);a=f}a=a.split("\n");for(c=0;c<a.length;c++)H("tspan",{dy:"1em",x:5},
vi).appendChild(document.createTextNode(a[c]));oi=!0;ui.style.display="block";a=vi.getBBox();c=10+a.width;e=a.height;wi.setAttribute("width",c);wi.setAttribute("height",e);xi.setAttribute("width",c);xi.setAttribute("height",e);if(C)for(e=a.width,f=0;h=vi.childNodes[f];f++)h.setAttribute("text-anchor","end"),h.setAttribute("x",e+5);e=ri.x;e=C?e-(0+c):e+0;c=ri.y+10;f=Gi();c+a.height>f.height&&(c-=a.height+20);C?e=Math.max(5,e):e+a.width>f.width-10&&(e=f.width-a.width-10);ui.setAttribute("transform",
"translate("+e+","+c+")")}}function Fi(a,b,c){for(var d=[0],e=[],f=0;f<a.length;f++)d[d.length-1]+=a[f].length,!0===b[f]?(d.push(0),e.push(a[f].charAt(a[f].length-1))):!1===b[f]&&d[d.length-1]++;a=Math.max.apply(Math,d);for(f=b=0;f<d.length;f++)b-=2*Math.pow(Math.abs(c-d[f]),1.5),b-=Math.pow(a-d[f],1.5),-1!=".?!".indexOf(e[f])?b+=c/3:-1!=",;)]}".indexOf(e[f])&&(b+=c/4);1<d.length&&d[d.length-1]<=d[d.length-2]&&(b+=.5);return b}
function Ei(a,b,c){for(var d=Fi(a,b,c),e,f=0;f<b.length-1;f++)if(b[f]!=b[f+1]){var h=[].concat(b);h[f]=!h[f];h[f+1]=!h[f+1];var k=Fi(a,h,c);k>d&&(d=k,e=h)}return e?Ei(a,e,c):b};/*

 Visual Blocks Editor

 Copyright 2012 Google Inc.
 https://developers.google.com/blockly/

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
function Hi(a){this.j=null;this.ma=H("text",{"class":"blocklyText"},null);this.kd={height:25,width:0};this.T(a)}s(Hi,yg);g=Hi.prototype;g.clone=function(){return new Hi(this.Ea())};g.pd=!1;g.o=function(a){if(this.j)throw"Text has already been initialized once.";this.j=a;O(a).appendChild(this.ma);this.ma.cb=this.j;ag(this.ma)};g.i=function(){z(this.ma);this.ma=null};g.wa=function(){return this.ma};g.B=function(a){this.ma.cb=a};/*

 Visual Blocks Editor

 Copyright 2012 Google Inc.
 https://developers.google.com/blockly/

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
function Ii(a,b,c,d){this.type=a;this.name=b;this.j=c;this.p=d;this.Ca=[];this.align=-1;this.na=!0}function S(a,b,c){if(!b&&!c)return a;n(b)&&(b=new Hi(b));a.j.n&&b.o(a.j);b.name=c;b.Bg&&S(a,b.Bg);a.Ca.push(b);b.Wg&&S(a,b.Wg);a.j.L&&(a.j.v(),a.j.Pa());return a}g=Ii.prototype;g.A=function(){return this.na};
g.N=function(a){var b=[];if(this.na==a)return b;for(var c=(this.na=a)?"block":"none",d=0,e;e=this.Ca[d];d++)e.N(a);if(this.p){if(a)b=gi(this.p);else if(d=this.p,d.Fb&&Th(d.dc[d.type],d),d.t){e=Rh(E(d));for(var f=0;f<e.length;f++){for(var h=e[f],k=hi(h,!0),m=0;m<k.length;m++){var p=k[m];p.Fb&&Th(d.dc[p.type],p)}h=tg(h);for(k=0;k<h.length;k++)h[k].N(!1)}}if(d=E(this.p))d.n.wa().style.display=c,a||(d.L=!1)}return b};
g.P=function(a){if(!this.p)throw"This input does not have a connection.";this.p.P(a);return this};function Ji(a,b){a.align=b;a.j.L&&a.j.v();return a}g.o=function(){for(var a=0;a<this.Ca.length;a++)this.Ca[a].o(this.j)};g.i=function(){for(var a=0,b;b=this.Ca[a];a++)b.i();this.p&&this.p.i();this.j=null};/*

 Visual Blocks Editor

 Copyright 2012 Google Inc.
 https://developers.google.com/blockly/

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
function ni(a){ni.m.constructor.call(this,a);this.yc()}s(ni,Mh);g=ni.prototype;g.Ma="";g.yc=function(){Mh.prototype.yc.call(this);H("path",{"class":"blocklyIconShield",d:"M 2,15 Q -1,15 0.5,12 L 6.5,1.7 Q 8,-1 9.5,1.7 L 15.5,12 Q 17,15 14,15 z"},this.xa);this.Ld=H("text",{"class":"blocklyIconMark",x:8,y:13},this.xa);this.Ld.appendChild(document.createTextNode("!"))};
g.N=function(a){if(a!=this.A())if(a){var b=this.Ma;a=H("text",{"class":"blocklyText blocklyBubbleText",y:Dh},null);for(var b=b.split("\n"),c=0;c<b.length;c++)H("tspan",{dy:"1em",x:Dh},a).appendChild(document.createTextNode(b[c]));this.W=new Ah(this.g.u,a,this.g.n.Kb,this.Fc,this.Gc,null,null);if(C)for(var b=a.getBBox().width,c=0,d;d=a.childNodes[c];c++)d.setAttribute("text-anchor","end"),d.setAttribute("x",b+Dh);this.tc();a=this.W.Cc();this.W.rc(a.width,a.height)}else this.W.i(),this.W=null};
g.T=function(a){this.Ma!=a&&(this.Ma=a,this.A()&&(this.N(!1),this.N(!0)))};g.i=function(){this.g.ge=null;Mh.prototype.i.call(this)};/*

 Visual Blocks Editor

 Copyright 2011 Google Inc.
 https://developers.google.com/blockly/

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
var Ki=0;function ki(){}function dd(a,b){if(ue)return Li.create(ki,a,b);var c=new ki;c.initialize(a,b);return c}g=ki.prototype;g.initialize=function(a,b){var c=(++Ki).toString();this.id=ue?Mi(c):c;te(a,this);this.fill(a,b);r(this.onchange)&&K(a.Q,"blocklyWorkspaceChange",this,this.onchange)};
g.fill=function(a,b){this.F=this.D=this.K=null;this.S=[];this.disabled=this.L=this.Nd=!1;this.cb="";this.contextMenu=!0;this.gd=null;this.Bb=[];this.fc=this.Hb=this.ec=!0;this.xc=!1;this.u=a;this.lc=a.ji;if(b){this.type=b;var c=R[b],d;for(d in c)this[d]=c[d]}r(this.o)&&this.o()};function cd(a,b){return ue?Ni.get(a):ye(b,a)}g.n=null;g.wb=null;g.ta=null;g.ge=null;function tg(a){var b=[];a.wb&&b.push(a.wb);a.ta&&b.push(a.ta);a.ge&&b.push(a.ge);return b}
function ed(a){a.n=new $f(a);a.n.o();F||K(a.n.wa(),"mousedown",a,a.cf);a.u.Q.appendChild(a.n.wa())}function O(a){return a.n&&a.n.wa()}var Be=0,Oi=null,Pi=null;g=ki.prototype;g.select=function(){L&&Ce();L=this;this.n.Cf();De(this.u.Q,"blocklySelectChange")};function Ce(){var a=L;L=null;a.n.nf();De(a.u.Q,"blocklySelectChange")}
g.i=function(a,b,c){this.L=!1;var d;d=!1;if(this.K)this.K.t&&this.$a(null);else{var e=null;this.F&&this.F.t&&(e=this.F.t,this.$a(null));var f=Yc(this);a&&f&&(a=this.D.t,f.$a(null),e&&ai(e,a)&&md(e,a))}d&&this.moveBy(zh*(C?-1:1),2*zh);b&&this.n&&(d=this.n,Qi("delete"),b=xd(d.h),d=d.h.cloneNode(!0),d.Ti=b.x,d.Ui=b.y,d.setAttribute("transform","translate("+d.Ti+","+d.Ui+")"),Gc.appendChild(d),d.rh=d.getBBox(),d.Sg=new Date,kg(d));this.u&&!c&&(we(this.u,this),this.u=null);L==this&&(L=null,Ri());yh.ve==
this&&yh.Ub();for(c=this.Bb.length-1;0<=c;c--)this.Bb[c].i(!1);b=tg(this);for(c=0;c<b.length;c++)b[c].i();for(c=0;b=this.S[c];c++)b.i();this.S=[];b=hi(this,!0);for(c=0;c<b.length;c++)d=b[c],d.t&&d.disconnect(),b[c].i();this.n&&(this.n.i(),this.n=null);if(ue&&!Si)Ni["delete"](this.id.toString())};function D(a){var b=0,c=0;if(a.n){var d=a.n.wa();do var e=Nh(d),b=b+e.x,c=c+e.y,d=d.parentNode;while(d&&d!=a.u.Q)}return{x:b,y:c}}
g.moveBy=function(a,b){var c=D(this);this.n.wa().setAttribute("transform","translate("+(c.x+a)+", "+(c.y+b)+")");di(this,a,b);Ti(this)};function vg(a){var b=a.n.height,c=a.n.width;if(a=Yc(a))a=vg(a),b+=a.height-4,c=Math.max(c,a.width);return{height:b,width:c}}
g.cf=function(a){if(!this.lc){Ui();Ri();this.select();zd();if(vd(a))Vi(this,a);else if(this.Hb&&!F){yd();Lh(!0);var b=D(this);this.Pi=b.x;this.Qi=b.y;this.Tg=a.clientX;this.Ug=a.clientY;Be=1;Oi=K(document,"mouseup",this,this.yg);Pi=K(document,"mousemove",this,this.xg);this.ze=[];for(var b=Rh(this),c=0,d;d=b[c];c++){d=tg(d);for(var e=0;e<d.length;e++){var f;f=d[e];f={x:f.Fc,y:f.Gc};f.uj=d[e];this.ze.push(f)}}}else return;a.stopPropagation()}};
g.yg=function(){var a=this;Wi(function(){Ri();if(L&&Uh){md(Vh,Uh);if(a.n){var b=(Wh(Vh)?Uh:Vh).j.n;Qi("click");var c=xd(b.h);b.g.K?(c.x+=C?3:-3,c.y+=13):b.g.F&&(c.x+=C?-23:23,c.y+=3);b=H("circle",{cx:c.x,cy:c.y,r:0,fill:"none",stroke:"#888","stroke-width":10},Gc);b.Sg=new Date;lg(b)}a.u.Ua&&a.u.Ua.Vb&&a.u.Ua.close()}else a.u.Ua&&a.u.Ua.Vb&&(b=a.u.Ua,ie(b.close,100,b),L.i(!1,!0),De(window,"resize"));Uh&&(z(Sh.Me),delete Sh.Me,Uh=null)})};
function Vi(a,b){if(!F&&a.contextMenu){var c=[];if(a.ec&&!F&&a.Hb&&!F&&!a.lc){var d={text:Xi,enabled:!0,pb:function(){var b=Xc(a);nd(b);var b=bd(a.u,b),c=D(a);c.x=C?c.x-zh:c.x+zh;c.y+=2*zh;b.moveBy(c.x,c.y);b.select()}};Rh(a).length>Ee(a.u)&&(d.enabled=!1);c.push(d);a.fc&&!F&&!a.xc&&Hc&&(d={enabled:!0},a.ta?(d.text=Yi,d.pb=function(){jd(a,null)}):(d.text=Zi,d.pb=function(){jd(a,"")}),c.push(d));if(!a.xc)for(d=0;d<a.S.length;d++)if(1==a.S[d].type){d={enabled:!0};d.text=a.Nd?$i:aj;d.pb=function(){fd(a,
!a.Nd)};c.push(d);break}Ic&&(a.xc?(d={enabled:!0},d.text=bj,d.pb=function(){a.Vd(!1)}):(d={enabled:!0},d.text=cj,d.pb=function(){a.Vd(!0)}),c.push(d));Jc&&(d={text:a.disabled?dj:ej,enabled:!sg(a),pb:function(){gd(a,!a.disabled)}},c.push(d));var d=Rh(a).length,e=Yc(a);e&&(d-=Rh(e).length);d={text:1==d?fj:gj.replace("%1",String(d)),enabled:!0,pb:function(){a.i(!0,!0)}};c.push(d)}d={enabled:!(r(a.J)?!a.J():!a.J)};d.text=hj;d.pb=function(){var b=r(a.J)?a.J():a.J;b&&window.open(b)};c.push(d);a.Kj&&!a.lc&&
a.Kj(c);yh.show(b,c);yh.ve=a}}function hi(a,b){var c=[];if(b||a.L)if(a.K&&c.push(a.K),a.D&&c.push(a.D),a.F&&c.push(a.F),b||!a.xc)for(var d=0,e;e=a.S[d];d++)e.p&&c.push(e.p);return c}function di(a,b,c){if(a.L){for(var d=hi(a,!1),e=0;e<d.length;e++)d[e].moveBy(b,c);d=tg(a);for(e=0;e<d.length;e++)ug(d[e]);for(e=0;e<a.Bb.length;e++)di(a.Bb[e],b,c)}}function ij(a,b){b?dg(a.n.h,"blocklyDragging"):eg(a.n.h,"blocklyDragging");for(var c=0;c<a.Bb.length;c++)ij(a.Bb[c],b)}
g.xg=function(a){var b=this;Wi(function(){if(!("mousemove"==a.type&&1>=a.clientX&&0==a.clientY&&0==a.button)){yd();var c=a.clientX-b.Tg,d=a.clientY-b.Ug;1==Be&&Math.sqrt(Math.pow(c,2)+Math.pow(d,2))>jj&&(Be=2,b.$a(null),ij(b,!0));if(2==Be){b.n.wa().setAttribute("transform","translate("+(b.Pi+c)+", "+(b.Qi+d)+")");for(var e=0;e<b.ze.length;e++){var f=b.ze[e],h=f.uj,k=f.x+c,f=f.y+d;h.Fc=k;h.Gc=f;h.A()&&Ch(h.W,k,f)}for(var h=hi(b,!1),f=k=null,m=zh,e=0;e<h.length;e++){var p=h[e],q=ei(p,m,c,d);q.p&&(k=
q.p,f=p,m=q.Bi)}Uh&&Uh!=k&&(z(Sh.Me),delete Sh.Me,Vh=Uh=null);k&&k!=Uh&&(k.Le(),Uh=k,Vh=f);b.u.Ua&&b.ec&&!F&&b.u.Ua.df(a)}}a.stopPropagation()})};g.Pa=function(){if(0==Be){var a=bi(this);if(!a.lc)for(var b=hi(this,!1),c=0;c<b.length;c++){var d=b[c];d.t&&Wh(d)&&E(d).Pa();for(var e=fi(d),f=0;f<e.length;f++){var h=e[f];d.t&&h.t||bi(h.j)!=a&&(Wh(d)?Zh(h,d):Zh(d,h))}}}};g.getParent=function(){return this.gd};function Yc(a){return a.D&&E(a.D)}function bi(a){var b=a;do a=b,b=a.gd;while(b);return a}
g.Dc=function(){return this.Bb};
g.$a=function(a){if(this.gd){for(var b=this.gd.Bb,c,d=0;c=b[d];d++)if(c==this){b.splice(d,1);break}b=D(this);this.u.Q.appendChild(this.n.wa());this.n.wa().setAttribute("transform","translate("+b.x+", "+b.y+")");this.gd=null;this.F&&this.F.t&&this.F.disconnect();this.K&&this.K.t&&this.K.disconnect()}else Xa(Wc(this.u,!1),this)&&we(this.u,this);(this.gd=a)?(a.Bb.push(this),b=D(this),a.n&&this.n&&a.n.wa().appendChild(this.n.wa()),a=D(this),di(this,a.x-b.x,a.y-b.y)):te(this.u,this)};
function Rh(a){for(var b=[a],c,d=0;c=a.Bb[d];d++)b.push.apply(b,Rh(c));return b}function hd(a,b){a.ec=b;a.n&&bg(a.n)}function id(a,b){a.fc=b;for(var c=0,d;d=a.S[c];c++)for(var e=0,f;f=d.Ca[e];e++)f.Zb();d=tg(a);for(c=0;c<d.length;c++)d[c].Zb()}g.C=function(a){this.Lf=a;this.n&&this.n.tc();var b=tg(this);for(a=0;a<b.length;a++)b[a].tc();if(this.L){for(a=0;b=this.S[a];a++)for(var c=0,d;d=b.Ca[c];c++)d.T(null);this.v()}};
function kd(a,b){for(var c=0,d;d=a.S[c];c++)for(var e=0,f;f=d.Ca[e];e++)if(f.name===b)return f;return null}function T(a,b){var c=kd(a,b);return c?c.ic():null}g.B=function(a){this.cb=a};function ii(a,b){var c;a.F&&(a.F.i(),a.F=null);b&&(void 0===c&&(c=null),a.F=new Sh(a,4),a.F.P(c));a.L&&(a.v(),a.Pa())}function ji(a,b){var c;a.D&&(a.D.i(),a.D=null);b&&(void 0===c&&(c=null),a.D=new Sh(a,3),a.D.P(c));a.L&&(a.v(),a.Pa())}
function Q(a,b){a.K&&(a.K.i(),a.K=null);void 0===b&&(b=null);a.K=new Sh(a,2);a.K.P(b);a.L&&(a.v(),a.Pa())}function fd(a,b){a.Nd=b;a.L&&(a.v(),a.Pa(),re(a.u))}function gd(a,b){a.disabled!=b&&(a.disabled=b,rg(a.n),re(a.u))}function sg(a){for(;;){a:for(;;){do{var b=a;a=a.getParent();if(!a){a=null;break a}}while(Yc(a)==b);break a}if(!a)return!1;if(a.disabled)return!0}}g.isCollapsed=function(){return this.xc};
g.Vd=function(a){if(this.xc!=a){this.xc=a;for(var b=[],c=0,d;d=this.S[c];c++)b.push.apply(b,d.N(!a));if(a){a=tg(this);for(c=0;c<a.length;c++)a[c].N(!1);c=this.toString(kj);S(lj(this,"_TEMP_COLLAPSED_INPUT"),c)}else mj(this,"_TEMP_COLLAPSED_INPUT");b.length||(b[0]=this);if(this.L){for(c=0;a=b[c];c++)a.v();this.Pa()}}};
g.toString=function(a){for(var b=[],c=0,d;d=this.S[c];c++){for(var e=0,f;f=d.Ca[e];e++)b.push(f.Ea());d.p&&((d=E(d.p))?b.push(d.toString()):b.push("?"))}b=va(b.join(" "))||"???";a&&b.length>a&&(b=b.substring(0,a-3)+"...");return b};function U(a,b){return nj(a,1,b)}function lj(a,b){return nj(a,5,b||"")}
g.vb=function(a,b){function c(a){a instanceof yg?S(this,a):S(this,a[1],a[0])}var d=arguments[arguments.length-1];--arguments.length;for(var e=a.split(this.vb.jj),f=[],h=0;h<e.length;h+=2){var k=va(e[h]),m=void 0;k&&f.push(new Hi(k));if((k=e[h+1])&&"%"==k.charAt(0)){var k=parseInt(k.substring(1),10),p=arguments[k];p[1]instanceof yg?f.push([p[0],p[1]]):m=Ji(U(this,p[0]).P(p[1]),p[2]);arguments[k]=null}else"\n"==k&&f.length&&(m=lj(this));m&&f.length&&(f.forEach(c,m),f=[])}f.length&&(m=Ji(lj(this),d),
f.forEach(c,m));for(h=1;h<arguments.length-1;h++);fd(this,!a.match(this.vb.aj))};g.vb.jj=/(%\d+|\n)/;g.vb.aj=/%1\s*$/;function nj(a,b,c){var d=null;if(1==b||3==b)d=new Sh(a,b);b=new Ii(b,c,a,d);a.S.push(b);a.L&&(a.v(),a.Pa());return b}function mj(a,b){for(var c=0,d;d=a.S[c];c++)if(d.name==b){d.p&&d.p.t&&E(d.p).$a(null);d.i();a.S.splice(c,1);a.L&&(a.v(),a.Pa());return}La('Input "%s" not found.',b)}function ld(a,b){for(var c=0,d;d=a.S[c];c++)if(d.name==b)return d;return null}
function oj(a,b){var c=ld(a,b);return c&&c.p&&E(c.p)}function pj(a){var b=new Oh(["controls_if_elseif","controls_if_else"]);a.wb&&a.wb!==b&&a.wb.i();b&&(b.g=a,a.wb=b,a.n&&cg(b))}function qj(a){return a.ta?a.ta.Ea().replace(/\s+$/,"").replace(/ +\n/g,"\n"):""}function jd(a,b){var c=!1;n(b)?(a.ta||(a.ta=new li(a),c=!0),a.ta.T(b)):a.ta&&(a.ta.i(),c=!0);a.L&&(a.v(),c&&a.Pa())}g.v=function(){this.n.v();Ti(this)};/*

 Visual Blocks Editor

 Copyright 2011 Google Inc.
 https://developers.google.com/blockly/

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
function Ph(){var a=this;this.s=new pe(function(){return Qh(a)},function(b){var c=Qh(a);c&&(ga(b.y)&&(a.s.scrollY=-c.Va*b.y-c.qb),a.s.Q.setAttribute("transform","translate(0,"+(a.s.scrollY+c.gb)+")"))});this.s.ji=!0;this.Uh=[];this.ib=this.O=0;this.se=[];this.Gb=[]}var rj,sj,tj,uj,vj,wj;g=Ph.prototype;g.ud=!0;g.sa=8;g.I=function(){this.h=H("g",{},null);this.jb=H("path",{"class":"blocklyFlyoutBackground"},this.h);this.h.appendChild(this.s.I());return this.h};
g.i=function(){this.Ub();J(this.Uh);this.Uh.length=0;this.Oc&&(this.Oc.i(),this.Oc=null);this.s=null;this.h&&(z(this.h),this.h=null);this.ae=this.jb=null};function Qh(a){if(!a.A())return null;var b=a.ib-2*a.sa,c=a.O;try{var d=a.s.Q.getBBox()}catch(e){d={height:0,y:0}}return{za:b,V:c,Va:d.height+d.y,Lb:-a.s.scrollY,qb:0,gb:a.sa,fb:0}}
g.o=function(a){this.ae=a;this.Oc=new pd(this.s,!1,!1);this.Ub();K(window,"resize",this,this.Sd);this.Sd();K(this.h,"wheel",this,this.Yi);K(this.h,"mousewheel",this,this.Yi);K(this.ae.Q,"blocklyWorkspaceChange",this,this.Yf);K(this.h,"mousedown",this,this.cf)};
g.Sd=function(){if(this.A()){var a=this.ae.Tb();if(a){var b=this.O-this.sa;C&&(b*=-1);var c=["M "+(C?this.O:0)+",0"];c.push("h",b);c.push("a",this.sa,this.sa,0,0,C?0:1,C?-this.sa:this.sa,this.sa);c.push("v",Math.max(0,a.za-2*this.sa));c.push("a",this.sa,this.sa,0,0,C?0:1,C?this.sa:-this.sa,this.sa);c.push("h",-b);c.push("z");this.jb.setAttribute("d",c.join(" "));b=a.fb;C&&(b+=a.V,b-=this.O);this.h.setAttribute("transform","translate("+b+","+a.gb+")");this.ib=a.za;this.Oc&&this.Oc.resize()}}};
g.Yi=function(a){var b=a.deltaY||-a.wheelDeltaY;if(b){Gb&&(b*=10);var c=Qh(this),b=c.Lb+b,b=Math.min(b,c.Va-c.za),b=Math.max(b,0);this.Oc.set(b);a.preventDefault()}};g.A=function(){return this.h&&"block"==this.h.style.display};g.Ub=function(){if(this.A()){this.h.style.display="none";for(var a=0,b;b=this.Gb[a];a++)J(b);this.Gb.length=0;this.Fg&&(J(this.Fg),this.Fg=null)}};
g.show=function(a){this.Ub();for(var b=Wc(this.s,!1),c=0,d;d=b[c];c++)d.u==this.s&&d.i(!1,!1);for(var c=0,e;e=this.se[c];c++)z(e);this.se.length=0;var f=this.sa;this.h.style.display="block";var b=[],h=[];if(a==xj)yj(b,h,f,this.s);else if(a==zj)Aj(b,h,f,this.s);else for(var k=0;d=a[k];k++)d.tagName&&"BLOCK"==d.tagName.toUpperCase()&&(d=bd(this.s,d),b.push(d),h.push(3*f));a=f;for(k=0;d=b[k];k++){c=Rh(d);e=0;for(var m;m=c[e];e++)m.lc=!0,jd(m,null);d.v();m=O(d);e=vg(d);c=C?0:f+8;d.moveBy(c,a);a+=e.height+
h[k];e=H("rect",{"fill-opacity":0},null);this.s.Q.insertBefore(e,O(d));d.Ad=e;this.se[k]=e;this.ud?this.Gb.push(K(m,"mousedown",null,Bj(this,d))):this.Gb.push(K(m,"mousedown",null,Cj(this,d)));this.Gb.push(K(m,"mouseover",d.n,d.n.Cf));this.Gb.push(K(m,"mouseout",d.n,d.n.nf));this.Gb.push(K(e,"mousedown",null,Bj(this,d)));this.Gb.push(K(e,"mouseover",d.n,d.n.Cf));this.Gb.push(K(e,"mouseout",d.n,d.n.nf))}this.Gb.push(K(this.jb,"mouseover",this,function(){for(var a=Wc(this.s,!1),b=0,c;c=a[b];b++)c.n.nf()}));
this.O=0;this.Ci();this.Yf();Dj(window,"resize");this.Fg=K(this.s.Q,"blocklyWorkspaceChange",this,this.Ci);re(this.s)};g.Ci=function(){for(var a=0,b=this.sa,c=Wc(this.s,!1),d=0,e;e=c[d];d++)var f=vg(e),a=Math.max(a,f.width);a+=b+8+b/2+I;if(this.O!=a){for(d=0;e=c[d];d++){var f=vg(e),h=D(e);if(C){var k=a-b-8-h.x;e.moveBy(k,0);h.x+=k}e.Ad&&(e.Ad.setAttribute("width",f.width),e.Ad.setAttribute("height",f.height),e.Ad.setAttribute("x",C?h.x-f.width:h.x),e.Ad.setAttribute("y",h.y))}this.O=a;De(window,"resize")}};
ki.prototype.moveTo=function(a,b){var c=D(this);this.n.wa().setAttribute("transform","translate("+a+", "+b+")");di(this,a-c.x,b-c.y)};function Cj(a,b){return function(c){Ri();zd();vd(c)?Vi(b,c):(yd(),Lh(!0),rj=c,sj=b,tj=a,uj=K(document,"mouseup",this,Ri),vj=K(document,"mousemove",this,a.Ik));c.stopPropagation()}}Ph.prototype.cf=function(a){vd(a)||(zd(!0),Ej(),this.Oi=a.clientY,wj=K(document,"mousemove",this,this.xg),uj=K(document,"mouseup",this,Ej),a.preventDefault(),a.stopPropagation())};
Ph.prototype.xg=function(a){var b=a.clientY-this.Oi;this.Oi=a.clientY;a=Qh(this);b=a.Lb-b;b=Math.min(b,a.Va-a.za);b=Math.max(b,0);this.Oc.set(b)};Ph.prototype.Ik=function(a){"mousemove"==a.type&&1>=a.clientX&&0==a.clientY&&0==a.button?a.stopPropagation():(yd(),Math.sqrt(Math.pow(a.clientX-rj.clientX,2)+Math.pow(a.clientY-rj.clientY,2))>jj&&Bj(tj,sj)(rj))};
function Bj(a,b){return function(c){if(!vd(c)&&!b.disabled){var d=Xc(b),d=bd(a.ae,d),e=O(b);if(!e)throw"originBlock is not rendered.";var e=xd(e),f=O(d);if(!f)throw"block is not rendered.";f=xd(f);d.moveBy(e.x-f.x,e.y-f.y);a.ud?a.Ub():a.Yf();d.cf(c)}}}Ph.prototype.Yf=function(){for(var a=Ee(this.ae),b=Wc(this.s,!1),c=0,d;d=b[c];c++){var e=Rh(d).length>a;gd(d,e)}};function Ej(){uj&&(J(uj),uj=null);vj&&(J(vj),vj=null);wj&&(J(wj),wj=null);uj&&(J(uj),uj=null);tj=sj=rj=null};function Fj(a){if("function"==typeof a.fg)return a.fg();if(n(a))return a.split("");if(fa(a)){for(var b=[],c=a.length,d=0;d<c;d++)b.push(a[d]);return b}b=[];c=0;for(d in a)b[c++]=a[d];return b};function Gj(a){this.Na=void 0;this.Aa={};if(a){var b;if("function"==typeof a.eg)b=a.eg();else if("function"!=typeof a.fg)if(fa(a)||n(a)){b=[];for(var c=a.length,d=0;d<c;d++)b.push(d)}else for(d in b=[],c=0,a)b[c++]=d;else b=void 0;a=Fj(a);for(c=0;c<b.length;c++)this.set(b[c],a[c])}}g=Gj.prototype;g.set=function(a,b){Hj(this,a,b,!1)};g.add=function(a,b){Hj(this,a,b,!0)};
function Hj(a,b,c,d){for(var e=0;e<b.length;e++){var f=b.charAt(e);a.Aa[f]||(a.Aa[f]=new Gj);a=a.Aa[f]}if(d&&void 0!==a.Na)throw Error('The collection already contains the key "'+b+'"');a.Na=c}g.get=function(a){a:{for(var b=this,c=0;c<a.length;c++)if(b=b.Aa[a.charAt(c)],!b){a=void 0;break a}a=b}return a?a.Na:void 0};g.fg=function(){var a=[];Ij(this,a);return a};function Ij(a,b){void 0!==a.Na&&b.push(a.Na);for(var c in a.Aa)Ij(a.Aa[c],b)}
g.eg=function(a){var b=[];if(a){for(var c=this,d=0;d<a.length;d++){var e=a.charAt(d);if(!c.Aa[e])return[];c=c.Aa[e]}Jj(c,a,b)}else Jj(this,"",b);return b};function Jj(a,b,c){void 0!==a.Na&&c.push(b);for(var d in a.Aa)Jj(a.Aa[d],b+d,c)}g.clear=function(){this.Aa={};this.Na=void 0};
g.remove=function(a){for(var b=this,c=[],d=0;d<a.length;d++){var e=a.charAt(d);if(!b.Aa[e])throw Error('The collection does not have the key "'+a+'"');c.push([b,e]);b=b.Aa[e]}a=b.Na;for(delete b.Na;0<c.length;)if(e=c.pop(),b=e[0],e=e[1],b.Aa[e].ii())delete b.Aa[e];else break;return a};g.clone=function(){return new Gj(this)};g.ii=function(){var a;if(a=void 0===this.Na)a:{a=this.Aa;for(var b in a){a=!1;break a}a=!0}return a};function Kj(){this.Jc=new Gj}g=Kj.prototype;g.aa="";g.sg=null;g.Xe=null;g.Qd=0;g.cd=0;function Lj(a,b){var c=!1,d=a.Jc.eg(b);d&&d.length&&(a.cd=0,a.Qd=0,c=a.Jc.get(d[0]),c=Mj(a,c))&&(a.sg=d);return c}function Mj(a,b){var c;b&&(a.cd<b.length&&(c=b[a.cd],a.Xe=b),c&&(c.Hg(),c.select()));return!!c}g.clear=function(){this.aa=""};function Nj(a){var b;b||(b=Oj(a||arguments.callee.caller,[]));return b}
function Oj(a,b){var c=[];if(Xa(b,a))c.push("[...circular reference...]");else if(a&&50>b.length){c.push(Pj(a)+"(");for(var d=a.arguments,e=0;d&&e<d.length;e++){0<e&&c.push(", ");var f;f=d[e];switch(typeof f){case "object":f=f?"object":"null";break;case "string":break;case "number":f=String(f);break;case "boolean":f=f?"true":"false";break;case "function":f=(f=Pj(f))?f:"[fn]";break;default:f=typeof f}40<f.length&&(f=f.substr(0,40)+"...");c.push(f)}b.push(a);c.push(")\n");try{c.push(Oj(a.caller,b))}catch(h){c.push("[exception trying to get caller]\n")}}else a?
c.push("[...long stack...]"):c.push("[end]");return c.join("")}function Pj(a){if(Qj[a])return Qj[a];a=String(a);if(!Qj[a]){var b=/function ([^\(]+)/.exec(a);Qj[a]=b?b[1]:"[Anonymous]"}return Qj[a]}var Qj={};function Rj(a,b,c,d,e){this.reset(a,b,c,d,e)}Rj.prototype.Wh=null;Rj.prototype.Vh=null;var Sj=0;Rj.prototype.reset=function(a,b,c,d,e){"number"==typeof e||Sj++;d||pa();this.Pd=a;this.yk=b;delete this.Wh;delete this.Vh};Rj.prototype.Ki=function(a){this.Pd=a};function Tj(a){this.$e=a;this.di=this.Y=this.Pd=this.Ga=null}function Uj(a,b){this.name=a;this.value=b}Uj.prototype.toString=function(){return this.name};var Vj=new Uj("WARNING",900),Wj=new Uj("INFO",800),Xj=new Uj("CONFIG",700),Yj=new Uj("FINE",500);g=Tj.prototype;g.getName=function(){return this.$e};g.getParent=function(){return this.Ga};g.Dc=function(){this.Y||(this.Y={});return this.Y};g.Ki=function(a){this.Pd=a};
function Zj(a){if(a.Pd)return a.Pd;if(a.Ga)return Zj(a.Ga);La("Root logger has no level set.");return null}g.log=function(a,b,c){if(a.value>=Zj(this).value)for(r(b)&&(b=b()),a=this.Wj(a,b,c,Tj.prototype.log),b="log:"+a.yk,l.console&&(l.console.timeStamp?l.console.timeStamp(b):l.console.markTimeline&&l.console.markTimeline(b)),l.msWriteProfilerMark&&l.msWriteProfilerMark(b),b=this;b;){c=b;var d=a;if(c.di)for(var e=0,f=void 0;f=c.di[e];e++)f(d);b=b.getParent()}};
g.Wj=function(a,b,c,d){var e=new Rj(a,String(b),this.$e);if(c){var f;f=d||arguments.callee.caller;e.Wh=c;var h;try{var k;var m=aa("window.location.href");if(n(c))k={message:c,name:"Unknown error",lineNumber:"Not available",fileName:m,stack:"Not available"};else{var p,q,u=!1;try{p=c.lineNumber||c.em||"Not available"}catch(t){p="Not available",u=!0}try{q=c.fileName||c.filename||c.sourceURL||l.$googDebugFname||m}catch(G){q="Not available",u=!0}k=!u&&c.lineNumber&&c.fileName&&c.stack&&c.message&&c.name?
c:{message:c.message||"Not available",name:c.name||"UnknownError",lineNumber:p,fileName:q,stack:c.stack||"Not available"}}h="Message: "+xa(k.message)+'\nUrl: <a href="view-source:'+k.fileName+'" target="_new">'+k.fileName+"</a>\nLine: "+k.lineNumber+"\n\nBrowser stack:\n"+xa(k.stack+"-> ")+"[end]\n\nJS stack traversal:\n"+xa(Nj(f)+"-> ")}catch(Aa){h="Exception trying to expose exception! You win, we lose. "+Aa}e.Vh=h}return e};g.ge=function(a,b){this.log(Vj,a,b)};
g.info=function(a,b){this.log(Wj,a,b)};var ak={},bk=null;function ck(a){bk||(bk=new Tj(""),ak[""]=bk,bk.Ki(Xj));var b;if(!(b=ak[a])){b=new Tj(a);var c=a.lastIndexOf("."),d=a.substr(c+1),c=ck(a.substr(0,c));c.Dc()[d]=b;b.Ga=c;ak[a]=b}return b};function dk(a){ge.call(this);this.w=a;a=v?"focusout":"blur";this.rk=Wd(this.w,v?"focusin":"focus",this,!v);this.tk=Wd(this.w,a,this,!v)}s(dk,ge);dk.prototype.handleEvent=function(a){var b=new Qd(a.Sb);b.type="focusin"==a.type||"focus"==a.type?"focusin":"focusout";this.dispatchEvent(b)};dk.prototype.ba=function(){dk.m.ba.call(this);ce(this.rk);ce(this.tk);delete this.w};function ek(a,b,c){gh.call(this,a,b,c);this.zd=!0;ph(this,!0);this.Za=this;this.be=new Kj;if(v)try{document.execCommand("BackgroundImageCache",!1,!0)}catch(d){(a=this.ni)&&a.ge("Failed to enable background image cache",void 0)}}s(ek,gh);ek.prototype.Fa=null;ek.prototype.$f=null;var fk=ek.prototype,gk=ck("goog.ui.tree.TreeControl");fk.ni=gk;g=ek.prototype;g.bg=!1;g.Tj=null;g.Yd=!0;g.Og=!0;g.Rc=!0;g.Pg=!0;g.Ja=function(){return this};g.Yc=function(){return 0};g.Hg=function(){};
g.bk=function(){this.bg=!0;mf(this.k(),"focused");this.Za&&this.Za.select()};g.Yj=function(){this.bg=!1;of(this.k(),"focused")};g.hasFocus=function(){return this.bg};g.Ra=function(){return!this.Rc||ek.m.Ra.call(this)};g.Yb=function(a){this.Rc?ek.m.Yb.call(this,a):this.zd=a};g.dg=function(){return zb};g.Fe=function(){var a=nh(this);return a?a.firstChild:null};g.Ee=function(){return null};g.nd=function(){};g.Fd=function(){return ek.m.Fd.call(this)+(this.Rc?"":" "+this.ua.Ih)};
g.Be=function(){var a=this.Ra(),b=this.Sj;if(a&&b)return b;b=this.jk;if(!a&&b)return b;b=this.ua;return a&&b.Hh?b.zc+" "+b.Hh:!a&&b.Dh?b.zc+" "+b.Dh:""};g.Qc=function(a){if(this.Za!=a){var b=!1;this.Za&&(b=this.Za==this.Tj,ph(this.Za,!1));if(this.Za=a)ph(a,!0),b&&a.select();this.dispatchEvent("change")}};function hk(a){function b(a){var h=kh(a);if(h){var k=!d||c==a.getParent()&&!e?a.ua.Bh:a.ua.Ah;h.className=k;if(h=a.Ee())h.className=th(a)}ef(a,b)}var c=a,d=c.Yd,e=c.Pg;b(a)}
g.Pe=function(){ek.m.Pe.call(this);var a=this.k();tf(a,"tree");uf(a,"labelledby",jh(this).id)};g.va=function(){ek.m.va.call(this);var a=this.k();a.className=this.ua.Lh;a.setAttribute("hideFocus","true");a=this.k();a.tabIndex=0;var b=this.Fa=new Gf(a),c=this.$f=new dk(a);cf(this).H(c,"focusout",this.Yj).H(c,"focusin",this.bk).H(b,"key",this.ub).H(a,"mousedown",this.hg).H(a,"click",this.hg).H(a,"dblclick",this.hg);this.Pe()};
g.hb=function(){ek.m.hb.call(this);this.Fa.i();this.Fa=null;this.$f.i();this.$f=null};g.hg=function(a){var b=this.ni;b&&b.log(Yj,"Received event "+a.type,void 0);if(b=ik(this,a))switch(a.type){case "mousedown":b.wg(a);break;case "click":a.preventDefault();break;case "dblclick":b.qi(a)}};
g.ub=function(a){var b=!1,b=this.be,c=!1;switch(a.keyCode){case 40:case 38:if(a.ctrlKey){var c=40==a.keyCode?1:-1,d=b.sg;if(d){var e=null,f=!1;if(b.Xe){var h=b.cd+c;0<=h&&h<b.Xe.length?(b.cd=h,e=b.Xe):f=!0}e||(h=b.Qd+c,0<=h&&h<d.length&&(b.Qd=h),d.length>b.Qd&&(e=b.Jc.get(d[b.Qd])),e&&e.length&&f&&(b.cd=-1==c?e.length-1:0));Mj(b,e)&&(b.sg=d)}c=!0}break;case 8:d=b.aa.length-1;c=!0;0<d?(b.aa=b.aa.substring(0,d),Lj(b,b.aa)):0==d?b.aa="":c=!1;break;case 27:b.aa="",c=!0}if(!(b=c)&&(b=this.Za)){b=this.Za;
c=!0;switch(a.keyCode){case 39:if(a.altKey)break;hf(b)&&(b.Ra()?M(b,0).select():b.Yb(!0));break;case 37:if(a.altKey)break;hf(b)&&b.Ra()&&b.Od?b.Yb(!1):(d=b.getParent(),e=b.Ja(),d&&(e.Rc||d!=e)&&d.select());break;case 40:a:if(hf(b)&&b.Ra())d=M(b,0);else{for(d=b;d!=b.Ja();){e=d.xb;if(null!=e){d=e;break a}d=d.getParent()}d=null}d&&d.select();break;case 38:d=b.Kc;null!=d?d=wh(d):(d=b.getParent(),e=b.Ja(),d=!e.Rc&&d==e||b==e?null:d);d&&d.select();break;default:c=!1}c&&(a.preventDefault(),(e=b.Ja())&&e.be.clear());
b=c}b||(b=this.be,c=!1,a.ctrlKey||a.altKey||(d=String.fromCharCode(a.charCode||a.keyCode).toLowerCase(),(1==d.length&&" "<=d&&"~">=d||"\u0080"<=d&&"\ufffd">=d)&&(" "!=d||b.aa)&&(b.aa+=d,c=Lj(b,b.aa))),b=c);b&&a.preventDefault();return b};function ik(a,b){for(var c=null,d=b.target;null!=d;){if(c=ih[d.id])return c;if(d==a.k())break;d=d.parentNode}return null}g.createNode=function(a){return new xh(a||zb,this.ua,this.tb())};
function vh(a,b){var c=a.be,d=b.Ea();if(d&&!/^[\s\xa0]*$/.test(null==d?"":String(d))){var d=d.toLowerCase(),e=c.Jc.get(d);e?e.push(b):c.Jc.set(d,[b])}}g.removeNode=function(a){var b=this.be,c=a.Ea();if(c&&!/^[\s\xa0]*$/.test(null==c?"":String(c))){var c=c.toLowerCase(),d=b.Jc.get(c);d&&(Ya(d,a),d.length&&b.Jc.remove(c))}};/*

 Visual Blocks Editor

 Copyright 2011 Google Inc.
 https://developers.google.com/blockly/

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
var jk,kk,lk,mk=0,nk={og:19,Lh:"blocklyTreeRoot",Ih:"blocklyHidden",Jh:"",Qf:"blocklyTreeRow",Kh:"blocklyTreeLabel",zc:"blocklyTreeIcon",Of:"blocklyTreeIconOpen",Pf:"blocklyTreeIconNone",Mh:"blocklyTreeSelected"};function ok(a,b){jk=dc("div","blocklyToolboxDiv");jk.setAttribute("dir",C?"RTL":"LTR");b.appendChild(jk);kk=new Ph;a.appendChild(kk.I());K(jk,"mousedown",null,function(a){vd(a)||a.target==jk?zd(!1):zd(!0)})}
function pk(){nk.cleardotPath=me+"1x1.gif";nk.cssCollapsedFolderIcon="blocklyTreeIconClosed"+(C?"Rtl":"Ltr");var a=new qk(zb,nk);lk=a;if(0!=a.Rc){a.Rc=!1;if(a.G){var b=nh(a);b&&(b.className=a.Fd())}a.Za==a&&M(a,0)&&a.Qc(M(a,0))}0!=a.Yd&&(a.Yd=!1,a.G&&hk(a));0!=a.Og&&(a.Og=!1,a.G&&hk(a));a.Qc(null);jk.style.display="block";kk.o(y);rk();a.v(jk);Wd(window,"resize",sk);sk()}
function sk(){var a=jk,b=Ve(Gc),c=Gi();C?(b=tk(0,0,!1),a.style.left=b.x+c.width-a.offsetWidth+"px"):a.style.marginLeft=b.left;a.style.height=c.height+1+"px";mk=a.offsetWidth;C||--mk}
function rk(){function a(c,d){for(var e=0,f;f=c.childNodes[e];e++)if(f.tagName){var h=f.tagName.toUpperCase();if("CATEGORY"==h){h=b.createNode(f.getAttribute("name"));h.vc=[];d.add(h);var k=f.getAttribute("custom");k?h.vc=k:a(f,h)}else"HR"==h?d.add(new uk):"BLOCK"==h&&d.vc.push(f)}}var b=lk;b.Ei();b.vc=[];a(Kc,lk);if(b.vc.length)throw"Toolbox cannot have both blocks and categories in the root level.";De(window,"resize")}function qk(a,b,c){ek.call(this,a,b,c)}s(qk,ek);
qk.prototype.va=function(){qk.m.va.call(this);if(Od){var a=this.k();K(a,"touchstart",this,this.gk)}};qk.prototype.gk=function(a){a.preventDefault();var b=ik(this,a);b&&"touchstart"===a.type&&window.setTimeout(function(){b.wg(a)},1)};qk.prototype.createNode=function(a){return new vk(a?rb(a):zb,this.ua,this.tb())};qk.prototype.Qc=function(a){this.Za!=a&&(ek.prototype.Qc.call(this,a),a&&a.vc&&a.vc.length?kk.show(a.vc):kk.Ub())};
function vk(a,b,c){function d(){De(window,"resize")}gh.call(this,a,b,c);Wd(lk,"expand",d);Wd(lk,"collapse",d)}s(vk,xh);gh.prototype.dg=function(){return xb("span")};vk.prototype.wg=function(){hf(this)&&this.Od?(this.toggle(),this.select()):this.Re()?this.Ja().Qc(null):this.select();qh(this)};vk.prototype.qi=function(){};function uk(){vk.call(this,"",wk)}s(uk,vk);var wk={Qf:"blocklyTreeSeparator"};/*

 Visual Blocks Editor

 Copyright 2012 Google Inc.
 https://developers.google.com/blockly/

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
var xj="VARIABLE";function xk(){var a;a=xe(y);for(var b=Object.create(null),c=0;c<a.length;c++){var d=a[c].$h;if(d)for(var d=d.call(a[c]),e=0;e<d.length;e++){var f=d[e];f&&(b[f.toLowerCase()]=f)}}a=[];for(var h in b)a.push(b[h]);return a}function yk(a,b){for(var c=xe(y),d=0;d<c.length;d++){var e=c[d].Rk;e&&e.call(c[d],a,b)}}
function yj(a,b,c,d){var e=xk();e.sort(wa);e.unshift(null);for(var f=void 0,h=0;h<e.length;h++)if(e[h]!==f){var k=R.variables_get?dd(d,"variables_get"):null;k&&ed(k);var m=R.variables_set?dd(d,"variables_set"):null;m&&ed(m);null===e[h]?f=(k||m).$h()[0]:(k&&kd(k,"VAR").bb(e[h]),m&&kd(m,"VAR").bb(e[h]));m&&a.push(m);k&&a.push(k);k&&m?b.push(c,3*c):b.push(2*c)}}
function zk(){var a=xk(),b="";if(a.length){a.sort(wa);for(var c=0,d="i",e=0,f=!1;!b;){e=0;for(f=!1;e<a.length&&!f;)a[e].toLowerCase()==d&&(f=!0),e++;f?("z"===d[0]?(c++,d="a"):(d=String.fromCharCode(d.charCodeAt(0)+1),"l"==d[0]&&(d=String.fromCharCode(d.charCodeAt(0)+1))),0<c&&(d+=c)):b=d}}else b="i";return b};/*

 Visual Blocks Editor

 Copyright 2012 Google Inc.
 https://developers.google.com/blockly/

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
function Ak(a,b){var c;if(b){var d=this;c=function(a){var c=Bk.call(d,a);a=void 0===c?a:null===c?d.ic():c;b.call(d,a);return c}}else c=Bk;Ak.m.constructor.call(this,Ck,c);a?this.bb(a):this.bb(zk())}s(Ak,P);Ak.prototype.clone=function(){return new Ak(this.ic(),this.Ia)};Ak.prototype.ic=function(){return this.Ea()};Ak.prototype.bb=function(a){this.Na=a;this.T(a)};
function Ck(){var a=xk(),b=this.Ea();b&&-1==a.indexOf(b)&&a.push(b);a.sort(wa);a.push(Dk);a.push(Ek);for(var b=[],c=0;c<a.length;c++)b[c]=[a[c],a[c]];return b}function Bk(a){function b(a,b){zd();var c=window.prompt(a,b);return c&&c.replace(/[\s\xa0]+/g," ").replace(/^ | $/g,"")}if(a==Dk){var c=this.Ea();(a=b(Fk.replace("%1",c),c))&&yk(c,a);return null}if(a==Ek)return(a=b(Gk,""))?(yk(a,a),a):null};/*

 Visual Blocks Editor

 Copyright 2012 Google Inc.
 https://developers.google.com/blockly/

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
var zj="PROCEDURE";function Hk(){for(var a=xe(y),b=[],c=[],d=0;d<a.length;d++){var e=a[d].$l;e&&(e=e.call(a[d]))&&(e[2]?b.push(e):c.push(e))}c.sort(Ik);b.sort(Ik);return[c,b]}function Ik(a,b){var c=a[0].toLowerCase(),d=b[0].toLowerCase();return c>d?1:c<d?-1:0}
function Aj(a,b,c,d){function e(e,f){for(var m=0;m<e.length;m++){var p=dd(d,f);kd(p,"NAME").bb(e[m][0]);for(var q=[],u=0;u<e[m][1].length;u++)q[u]="ARG"+u;p.rm(e[m][1],q);ed(p);a.push(p);b.push(2*c)}}if(R.procedures_defnoreturn){var f=dd(d,"procedures_defnoreturn");ed(f);a.push(f);b.push(2*c)}R.procedures_defreturn&&(f=dd(d,"procedures_defreturn"),ed(f),a.push(f),b.push(2*c));R.procedures_ifreturn&&(f=dd(d,"procedures_ifreturn"),ed(f),a.push(f),b.push(2*c));b.length&&(b[b.length-1]=3*c);f=Hk();e(f[0],
"procedures_callnoreturn");e(f[1],"procedures_callreturn")};var pg=/#(.)(.)(.)/;function mg(a){var b=a[0],c=a[1];a=a[2];b=Number(b);c=Number(c);a=Number(a);if(isNaN(b)||0>b||255<b||isNaN(c)||0>c||255<c||isNaN(a)||0>a||255<a)throw Error('"('+b+","+c+","+a+'") is not a valid RGB color');b=Jk(b.toString(16));c=Jk(c.toString(16));a=Jk(a.toString(16));return"#"+b+c+a}var og=/^#(?:[0-9a-f]{3}){1,2}$/i;function Jk(a){return 1==a.length?"0"+a:a}
function ng(a){var b=0,c=0,d=0,e=Math.floor(a/60),f=a/60-e;a=166.4*.55;var h=166.4*(1-.45*f),f=166.4*(1-.45*(1-f));switch(e){case 1:b=h;c=166.4;d=a;break;case 2:b=a;c=166.4;d=f;break;case 3:b=a;c=h;d=166.4;break;case 4:b=f;c=a;d=166.4;break;case 5:b=166.4;c=a;d=h;break;case 6:case 0:b=166.4,c=f,d=a}return[Math.floor(b),Math.floor(c),Math.floor(d)]}function qg(a,b,c){c=Math.min(Math.max(c,0),1);return[Math.round(c*a[0]+(1-c)*b[0]),Math.round(c*a[1]+(1-c)*b[1]),Math.round(c*a[2]+(1-c)*b[2])]};/*

 Visual Blocks Editor

 Copyright 2012 Google Inc.
 https://developers.google.com/blockly/

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
function dg(a,b){var c=a.getAttribute("class")||"";-1==(" "+c+" ").indexOf(" "+b+" ")&&(c&&(c+=" "),a.setAttribute("class",c+b))}function eg(a,b){var c=a.getAttribute("class");if(-1!=(" "+c+" ").indexOf(" "+b+" ")){for(var c=c.split(/\s+/),d=0;d<c.length;d++)c[d]&&c[d]!=b||(c.splice(d,1),d--);c.length?a.setAttribute("class",c.join(" ")):a.removeAttribute("class")}}
function K(a,b,c,d){function e(a){d.apply(c,arguments)}a.addEventListener(b,e,!1);var f=[[a,b,e]];if(b in Kk)for(var e=function(a){if(1==a.changedTouches.length){var b=a.changedTouches[0];a.clientX=b.clientX;a.clientY=b.clientY}d.apply(c,arguments);a.preventDefault()},h=0,k;k=Kk[b][h];h++)a.addEventListener(k,e,!1),f.push([a,k,e]);return f}var Kk={};"ontouchstart"in document.documentElement&&(Kk={mousedown:["touchstart"],mousemove:["touchmove"],mouseup:["touchend","touchcancel"]});
function J(a){for(;a.length;){var b=a.pop();b[0].removeEventListener(b[1],b[2],!1)}}function Dj(a,b){var c=document;if(c.createEvent)c=c.createEvent("UIEvents"),c.initEvent(b,!0,!0),a.dispatchEvent(c);else if(c.createEventObject)c=c.createEventObject(),a.fireEvent("on"+b,c);else throw"FireEvent: No event creation mechanism.";}function De(a,b){setTimeout(function(){Dj(a,b)},0)}
function Nh(a){var b={x:0,y:0},c=a.getAttribute("x");c&&(b.x=parseInt(c,10));if(c=a.getAttribute("y"))b.y=parseInt(c,10);if(a=(a=a.getAttribute("transform"))&&a.match(/translate\(\s*([-\d.]+)([ ,]\s*([-\d.]+)\s*\))?/))b.x+=parseInt(a[1],10),a[3]&&(b.y+=parseInt(a[3],10));return b}function xd(a){var b=0,c=0;do{var d=Nh(a),b=b+d.x,c=c+d.y;a=a.parentNode}while(a&&a!=Gc);return{x:b,y:c}}function $g(a){a=xd(a);return tk(a.x,a.y,!1)}
function H(a,b,c){a=document.createElementNS("http://www.w3.org/2000/svg",a);for(var d in b)a.setAttribute(d,b[d]);document.body.runtimeStyle&&(a.runtimeStyle=a.currentStyle=a.style);c&&c.appendChild(a);return a}function vd(a){return 2==a.button||a.ctrlKey}
function tk(a,b,c){c&&(a-=window.scrollX||window.pageXOffset,b-=window.scrollY||window.pageYOffset);var d=Gc.createSVGPoint();d.x=a;d.y=b;a=Gc.getScreenCTM();c&&(a=a.inverse());d=d.matrixTransform(a);c||(d.x+=window.scrollX||window.pageXOffset,d.y+=window.scrollY||window.pageYOffset);return d}function wd(a){return tk(a.clientX+(window.scrollX||window.pageXOffset),a.clientY+(window.scrollY||window.pageYOffset),!0)}
function ch(a){if(!a.length)return 0;for(var b=a[0].length,c=1;c<a.length;c++)b=Math.min(b,a[c].length);return b}function dh(a,b){if(!a.length)return 0;if(1==a.length)return a[0].length;for(var c=0,d=b||ch(a),e=0;e<d;e++){for(var f=a[0][e],h=1;h<a.length;h++)if(f!=a[h][e])return c;" "==f&&(c=e+1)}for(h=1;h<a.length;h++)if((f=a[h][e])&&" "!=f)return c;return d}
function eh(a,b){if(!a.length)return 0;if(1==a.length)return a[0].length;for(var c=0,d=b||ch(a),e=0;e<d;e++){for(var f=a[0].substr(-e-1,1),h=1;h<a.length;h++)if(f!=a[h].substr(-e-1,1))return c;" "==f&&(c=e+1)}for(h=1;h<a.length;h++)if((f=a[h].charAt(a[h].length-e-1))&&" "!=f)return c;return d};/*

 Visual Blocks Editor

 Copyright 2012 Google Inc.
 https://developers.google.com/blockly/

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
/*

 Visual Blocks Editor

 Copyright 2013 Google Inc.
 https://developers.google.com/blockly/

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
function Lk(){function a(a){a=a.slice(1).split("&");for(var c=0;c<a.length;c++){var f=a[c].split("=");b[decodeURIComponent(f[0])]=decodeURIComponent(f[1])}}var b={},c=window.location.hash;c&&a(c);(c=window.location.search)&&a(c);return b}var Mk=Lk();function V(a,b,c){if(a.hasOwnProperty(b))return a[b];void 0===c&&console.error(b+" should be present in the options.");return c}
function Nk(a){this.yj=V(a,"clientId");this.ah=Mk.userId;document.getElementById(V(a,"authButtonElementId"));document.getElementById(V(a,"authDivElementId"))}Nk.prototype.start=function(){gapi.load("auth:client,drive-realtime,drive-share",function(){})};
function Ok(a,b,c,d){function e(c){gapi.cc.oa.files.Qe({resource:{mimeType:b,title:a,parents:[{id:c}]}}).Bc(d)}function f(){function a(b){gapi.cc.oa.Qk.Qe({fileId:"appdata",resource:{key:"folderId",value:b}}).Bc(function(){e(b)})}function b(){gapi.cc.oa.files.Qe({resource:{mimeType:"application/vnd.google-apps.folder",title:c}}).Bc(function(b){a(b.id)})}gapi.cc.oa.Qk.get({fileId:"appdata",propertyKey:"folderId"}).Bc(function(d){if(d.error)c?b():a("root");else{var f=d.result.value;gapi.cc.oa.files.get({fileId:f}).Bc(function(a){a.error||
a.labels.tm?b():e(f)})}})}gapi.cc.load("drive","v2",function(){f()})}function Pk(a){this.ri=V(a,"onFileLoaded");this.Ak=V(a,"newFileMimeType","application/vnd.google-apps.drive-sdk");this.ei=V(a,"initializeModel");this.Di=V(a,"registerTypes",function(){});this.oh=V(a,"afterAuth",function(){});this.sj=V(a,"autoCreate",!1);this.Nj=V(a,"defaultTitle","New Realtime File");this.Mj=V(a,"defaultFolderTitle","");this.ph=V(a,"afterCreate",function(){});this.If=new Nk(a)}
function Qk(a,b,c){var d=[];b&&d.push("fileIds="+b.join(","));c&&d.push("userId="+c);c=0==d.length?window.location.pathname:window.location.pathname+"#"+d.join("&");window.history&&window.history.replaceState?window.history.replaceState("Google Drive Realtime API Playground","Google Drive Realtime API Playground",c):window.location.href=c;Mk=Lk();for(var e in b)gapi.oa.Ib.load(b[e],a.ri,a.ei,a.bi)}Pk.prototype.start=function(){var a=this;this.If.start(function(){a.Di&&a.Di();a.oh&&a.oh();a.load()})};
Pk.prototype.bi=function(a){a.type!=gapi.oa.Ib.eh.Ml&&(a.type==gapi.oa.Ib.eh.ol?(alert("An Error happened: "+a.message),window.location.href="/"):a.type==gapi.oa.Ib.eh.vl&&(alert("The file was not found. It does not exist or you do not have read access to the file."),window.location.href="/"))};
Pk.prototype.load=function(){var a=Mk.fileIds;a&&(a=a.split(","));var b=this.If.ah,b=Mk.state;if(a)for(var c in a)gapi.oa.Ib.load(a[c],this.ri,this.ei,this.bi);else{if(b){var d;try{d=JSON.parse(b)}catch(e){d=null}if("open"==d.action){a=d.cm;b=d.ah;Qk(this,a,b);return}}this.sj&&Rk(this)}};function Rk(a){Ok(a.Nj,a.Ak,a.Mj,function(b){b.id?(a.ph&&a.ph(b.id),Qk(a,[b.id],a.If.ah)):(console.error("Error creating file."),console.error(b))})};/*

 Visual Blocks Editor

 Copyright 2014 Google Inc.
 https://developers.google.com/blockly/

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
var ve,Sk,ue=!1,Tk=null,Li=null,Uk=null,Vk=null,Ni=null,Si=!1,Wk=null,Xk=null,Yk=null;function Zk(a){var b=a.Rj;a=a.Rj.length;for(var c=0;c<a;c++){var d=b[c];if(!d.pk){var e=d.target;"value_changed"==d.type&&("xmlDom"==d.zi?$k(function(){al(e,!1);bl(e)}):"relativeX"!=d.zi&&"relativeY"!=d.zi||$k(function(){e.n||al(e,!1);bl(e)}))}}}function cl(a){if(!a.pk){var b=a.newValue;b?al(b,!a.oldValue):(b=a.oldValue,dl(b))}}function $k(a){if(Si)a();else try{Si=!0,a()}finally{Si=!1}}
function al(a,b){$k(function(){var c=$c(a.bh).firstChild;if(c=bd(y,c,!0))b&&te(c.u,c),(b||Xa(ve,c))&&bl(c)})}function bl(a){if(!isNaN(a.kf)&&!isNaN(a.lf)){var b=Gi().width,c=D(a),d=a.kf-c.x;a.moveBy(C?b-d:d,a.lf-c.y)}}function dl(a){$k(function(){a.i(!0,!0,!0)})}function Ti(a){if(a.u==y&&ue&&!Si){a=bi(a);var b=D(a),c=!1,d=Xc(a);d.setAttribute("id",a.id);var e=dc("xml");e.appendChild(d);d=Zc(e);d!=a.bh&&(c=!0,a.bh=d);if(a.kf!=b.x||a.lf!=b.y)a.kf=b.x,a.lf=b.y,c=!0;c&&Ni.set(a.id.toString(),a)}}
function el(a,b){gapi.cc.oa.yi.list({fileId:a}).Bc(function(a){for(var d=0;d<a.items.length;d++){var e=a.items[d];if("owner"==e.lm){b(e.domain);break}}})}
var il={clientId:null,authButtonElementId:"authorizeButton",authDivElementId:"authButtonDiv",initializeModel:function(a){Li=a;var b=a.Wl();a.Zc().set("blocks",b);b=a.Vl();a.Zc().set("topBlocks",b);Xk&&a.Zc().set(Xk,a.Xl(Yk))},autoCreate:!0,defaultTitle:"Realtime Blockly File",defaultFolderTitle:"Realtime Blockly Folder",newFileMimeType:null,onFileLoaded:function(a){Tk=a;a:{for(var b=a.Vj(),c=0;c<b.length;c++){var d=b[c];if(d.qk){Uk=d.om;break a}}Uk=void 0}Li=a.Ye;Ni=Li.Zc().get("blocks");ve=Li.Zc().get("topBlocks");
Li.Zc().addEventListener(gapi.oa.Ib.Af.wl,Zk);Ni.addEventListener(gapi.oa.Ib.Af.Nl,cl);Vk();a.addEventListener(gapi.oa.Ib.Af.ql,fl);a.addEventListener(gapi.oa.Ib.Af.rl,gl);hl();a=ve;for(b=0;b<a.length;b++)c=a.get(b),al(c,!0)},registerTypes:function(){var a=gapi.oa.Ib.Yl;a.im(ki,"Block");ki.prototype.id=a.Kf("id");ki.prototype.bh=a.Kf("xmlDom");ki.prototype.kf=a.Kf("relativeX");ki.prototype.lf=a.Kf("relativeY");a.pm(ki,ki.prototype.initialize)},afterAuth:function(){window.setTimeout(function(){},18E5)},
afterCreate:function(a){var b=gapi.cc.oa.yi.Qe({fileId:a,resource:{type:"anyone",role:"writer",value:"default",withLink:!0}});b.Bc(function(c){c.error&&el(a,function(c){b=gapi.cc.oa.yi.Qe({fileId:a,resource:{type:"domain",role:"writer",value:c,withLink:!0}});b.Bc(function(){})})})}};function jl(){var a=Mc,b=V(a,"chatbox");b&&(Xk=V(b,"elementId"),Yk=V(b,"initText",kl));il.yj=V(a,"clientId");Sk=V(a,"collabElementId")}
function ll(a,b){jl();ue=!0;ml(b);Vk=function(){a();if(Xk){var b=Li.Zc().get(Xk),d=document.getElementById(Xk);gapi.oa.Ib.Zl.Rl(b,d);d.disabled=!1}};Wk=new Pk(il);Wk.start()}
function ml(a){a.style.background="url("+me+"progress.gif) no-repeat center center";var b=Pe(a),c=dc("div");c.id=il.authDivElementId;var d=dc("p",null,nl);c.appendChild(d);d=dc("button",null,"Authorize");d.id=il.Pl;c.appendChild(d);a.appendChild(c);c.style.display="none";c.style.position="relative";c.style.textAlign="center";c.style.border="1px solid";c.style.backgroundColor="#f6f9ff";c.style.borderRadius="15px";c.style.boxShadow="10px 10px 5px #888";c.style.width=b.width/3+"px";a=Pe(c);c.style.left=
(b.width-a.width)/3+"px";c.style.top=(b.height-a.height)/4+"px"}function hl(){if(Sk){var a;a=Sk;a=n(a)?document.getElementById(a):a;hc(a);for(var b=Tk.Vj(),c=0;c<b.length;c++){var d=b[c],e=dc("img",{src:d.gm||me+"anon.jpeg",alt:d.displayName,title:d.displayName+(d.qk?" ("+ol+")":"")});e.style.backgroundColor=d.color;a.appendChild(e)}}}function fl(){hl()}function gl(){hl()}function Mi(a){var b=Uk+"-"+a;return Ni.has(b)?Mi("-"+a):b};/*

 Visual Blocks Editor

 Copyright 2012 Google Inc.
 https://developers.google.com/blockly/

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
/*

 Visual Blocks Editor

 Copyright 2012 Google Inc.
 https://developers.google.com/blockly/

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
function pl(a,b){pl.m.constructor.call(this,a);this.Ia=b}var ql;s(pl,yg);g=pl.prototype;g.clone=function(){return new pl(this.Ea(),this.Ia)};g.dh="text";g.i=function(){Yg==this&&Zg();pl.m.i.call(this)};g.T=function(a){if(null!==a){if(this.Ia){var b=this.Ia(a);null!==b&&void 0!==b&&(a=b)}yg.prototype.T.call(this,a)}};
g.sf=function(a){a=a||!1;if(!a&&(Hb||Jb||Lb)){a=window.prompt(rl,this.Ma);if(this.Ia){var b=this.Ia(a);void 0!==b&&(a=b)}null!==a&&this.T(a)}else{Xg(this,sl(this));var b=ah,c=dc("input","blocklyHtmlInput");ql=c;b.appendChild(c);c.value=c.defaultValue=this.Ma;c.pi=null;tl(this);this.Gi();a||(c.focus(),c.select());c.Fk=K(c,"keyup",this,this.ti);c.Ek=K(c,"keypress",this,this.ti);c.Lk=K(this.j.u.Q,"blocklyWorkspaceChange",this,this.Gi)}};
g.ti=function(a){var b=ql;13==a.keyCode?Zg():27==a.keyCode?(this.T(b.defaultValue),Zg()):(a=b.value,a!==b.pi?(b.pi=a,this.T(a),tl(this)):w&&this.j.v())};function tl(a){var b=!0,c=ql;a.Ia&&(b=a.Ia(c.value));null===b?dg(c,"blocklyInvalidInput"):eg(c,"blocklyInvalidInput")}g.Gi=function(){var a=ah,b=this.Qa.getBBox();a.style.width=b.width+"px";b=$g(this.wc);if(C){var c=this.wc.getBBox();b.x+=c.width;b.x-=a.offsetWidth}b.y+=1;w&&(b.y-=3);a.style.left=b.x+"px";a.style.top=b.y+"px"};
function sl(a){return function(){var b=ql,c=b.value;a.Ia&&(c=a.Ia(c),null===c&&(c=b.defaultValue));a.T(c);a.j.L&&a.j.v();J(b.Fk);J(b.Ek);J(b.Lk);ql=null;ah.style.width="auto"}}function ul(a){a=a.replace(/O/ig,"0");a=a.replace(/,/g,"");a=parseFloat(a||0);return isNaN(a)?null:String(a)};/*

 Visual Blocks Editor

 Copyright 2012 Google Inc.
 https://developers.google.com/blockly/

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
function vl(a){this.$e=a;this.Bf="";this.Zi=new RegExp(this.$b,"g")}vl.prototype.lh=null;function wl(){var a=W,b=[];a.o();for(var c=Wc(y,!0),d=0,e;e=c[d];d++){var f=xl(a,e);ea(f)&&(f=f[0]);f&&(e.K&&a.Ii&&(f=a.Ii(f)),b.push(f))}b=b.join("\n");b=a.finish(b);b=b.replace(/^\s+\n/,"");b=b.replace(/\n\s+$/,"\n");return b=b.replace(/[ \t]+\n/g,"\n")}function yl(a,b){return b+a.replace(/\n(.)/g,"\n"+b+"$1")}
function xl(a,b){if(!b)return"";if(b.disabled)return xl(a,Yc(b));var c=a[b.type];if(!c)throw'Language "'+a.$e+'" does not know how to generate code for block type "'+b.type+'".';c=c.call(b,b);if(ea(c))return[a.Ji(b,c[0]),c[1]];if(n(c))return a.lh&&(c=a.lh.replace(/%1/g,"'"+b.id+"'")+c),a.Ji(b,c);if(null===c)return"";throw"Invalid code generated: "+c;}
function X(a,b,c){var d=W;if(isNaN(c))throw'Expecting valid order from block "'+a.type+'".';a=oj(a,b);if(!a)return"";b=xl(d,a);if(""===b)return"";if(!ea(b))throw'Expecting tuple from value block "'+a.type+'".';d=b[0];b=b[1];if(isNaN(b))throw'Expecting valid order from value block "'+a.type+'".';d&&c<=b&&c!=b&&0!=c&&99!=c&&(d="("+d+")");return d}function zl(a,b){var c=W,d=oj(a,b),e=xl(c,d);if(!n(e))throw'Expecting code from statement block "'+d.type+'".';e&&(e=yl(e,c.$i));return e}
vl.prototype.$i="  ";function Al(a){var b=W;b.Bf+=a+","}vl.prototype.$b="{leCUI8hutHZI4480Dc}";function Bl(a,b){var c=W;if(!c.wd[a]){var d=Uc(c.fe,a);c.Yh[a]=d;c.wd[a]=b.join("\n").replace(c.Zi,d)}return c.Yh[a]};/*

 Visual Blocks Editor

 Copyright 2013 Google Inc.
 https://developers.google.com/blockly/

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
function Cl(){var a=Dl.join("\n"),b=me.replace(/[\\\/]$/,""),a=a.replace(/<<<PATH>>>/g,b),b=document,c=b.createElement("style");c.type="text/css";b.getElementsByTagName("head")[0].appendChild(c);c.styleSheet?c.styleSheet.cssText=a:c.appendChild(b.createTextNode(a))}
var Dl=[".blocklySvg {","  background-color: #fff;","  border: 1px solid #ddd;","  overflow: hidden;","}",".blocklyWidgetDiv {","  position: absolute;","  display: none;","  z-index: 999;","}",".blocklyDraggable {","  cursor: url(<<<PATH>>>/handopen.cur) 8 5, auto;","}",".blocklyResizeSE {","  fill: #aaa;","  cursor: se-resize;","}",".blocklyResizeSW {","  fill: #aaa;","  cursor: sw-resize;","}",".blocklyResizeLine {","  stroke-width: 1;","  stroke: #888;","}",".blocklyHighlightedConnectionPath {",
"  stroke-width: 4px;","  stroke: #fc3;","  fill: none;","}",".blocklyPathLight {","  fill: none;","  stroke-width: 2;","  stroke-linecap: round;","}",".blocklySelected>.blocklyPath {","  stroke-width: 3px;","  stroke: #fc3;","}",".blocklySelected>.blocklyPathLight {","  display: none;","}",".blocklyDragging>.blocklyPath,",".blocklyDragging>.blocklyPathLight {","  fill-opacity: .8;","  stroke-opacity: .8;","}",".blocklyDragging>.blocklyPathDark {","  display: none;","}",".blocklyDisabled>.blocklyPath {",
"  fill-opacity: .5;","  stroke-opacity: .5;","}",".blocklyDisabled>.blocklyPathLight,",".blocklyDisabled>.blocklyPathDark {","  display: none;","}",".blocklyText {","  cursor: default;","  font-family: sans-serif;","  font-size: 11pt;","  fill: #fff;","}",".blocklyNonEditableText>text {","  pointer-events: none;","}",".blocklyNonEditableText>rect,",".blocklyEditableText>rect {","  fill: #fff;","  fill-opacity: .6;","}",".blocklyNonEditableText>text,",".blocklyEditableText>text {","  fill: #000;",
"}",".blocklyEditableText:hover>rect {","  stroke-width: 2;","  stroke: #fff;","}",".blocklyBubbleText {","  fill: #000;","}",".blocklySvg text {","  -moz-user-select: none;","  -webkit-user-select: none;","  user-select: none;","  cursor: inherit;","}",".blocklyHidden {","  display: none;","}",".blocklyFieldDropdown:not(.blocklyHidden) {","  display: block;","}",".blocklyTooltipBackground {","  fill: #ffffc7;","  stroke-width: 1px;","  stroke: #d8d8d8;","}",".blocklyTooltipShadow,",".blocklyDropdownMenuShadow {",
"  fill: #bbb;","  filter: url(#blocklyShadowFilter);","}",".blocklyTooltipText {","  font-family: sans-serif;","  font-size: 9pt;","  fill: #000;","}",".blocklyIconShield {","  cursor: default;","  fill: #00c;","  stroke-width: 1px;","  stroke: #ccc;","}",".blocklyIconGroup:hover>.blocklyIconShield {","  fill: #00f;","  stroke: #fff;","}",".blocklyIconGroup:hover>.blocklyIconMark {","  fill: #fff;","}",".blocklyIconMark {","  cursor: default !important;","  font-family: sans-serif;","  font-size: 9pt;",
"  font-weight: bold;","  fill: #ccc;","  text-anchor: middle;","}",".blocklyWarningBody {","}",".blocklyMinimalBody {","  margin: 0;","  padding: 0;","}",".blocklyCommentTextarea {","  margin: 0;","  padding: 2px;","  border: 0;","  resize: none;","  background-color: #ffc;","}",".blocklyHtmlInput {","  font-family: sans-serif;","  font-size: 11pt;","  border: none;","  outline: none;","  width: 100%","}",".blocklyMutatorBackground {","  fill: #fff;","  stroke-width: 1;","  stroke: #ddd;","}",".blocklyFlyoutBackground {",
"  fill: #ddd;","  fill-opacity: .8;","}",".blocklyColourBackground {","  fill: #666;","}",".blocklyScrollbarBackground {","  fill: #fff;","  stroke-width: 1;","  stroke: #e4e4e4;","}",".blocklyScrollbarKnob {","  fill: #ccc;","}",".blocklyScrollbarBackground:hover+.blocklyScrollbarKnob,",".blocklyScrollbarKnob:hover {","  fill: #bbb;","}",".blocklyInvalidInput {","  background: #faa;","}",".blocklyAngleCircle {","  stroke: #444;","  stroke-width: 1;","  fill: #ddd;","  fill-opacity: .8;","}",".blocklyAngleMarks {",
"  stroke: #444;","  stroke-width: 1;","}",".blocklyAngleGauge {","  fill: #f88;","  fill-opacity: .8;  ","}",".blocklyAngleLine {","  stroke: #f00;","  stroke-width: 2;","  stroke-linecap: round;","}",".blocklyContextMenu {","  border-radius: 4px;","}",".blocklyDropdownMenu {","  padding: 0 !important;","}",".blocklyWidgetDiv .goog-option-selected .goog-menuitem-checkbox,",".blocklyWidgetDiv .goog-option-selected .goog-menuitem-icon {","  background: url(<<<PATH>>>/sprites.png) no-repeat -48px -16px !important;",
"}",".blocklyToolboxDiv {","  background-color: #ddd;","  display: none;","  overflow-x: visible;","  overflow-y: auto;","  position: absolute;","}",".blocklyTreeRoot {","  padding: 4px 0;","}",".blocklyTreeRoot:focus {","  outline: none;","}",".blocklyTreeRow {","  line-height: 22px;","  height: 22px;","  padding-right: 1em;","  white-space: nowrap;","}",'.blocklyToolboxDiv[dir="RTL"] .blocklyTreeRow {',"  padding-right: 0;","  padding-left: 1em !important;","}",".blocklyTreeRow:hover {","  background-color: #e4e4e4;",
"}",".blocklyTreeSeparator {","  height: 0px;","  border-bottom: solid #e5e5e5 1px;","  margin: 5px 0;","}",".blocklyTreeIcon {","  height: 16px;","  width: 16px;","  vertical-align: middle;","  background-image: url(<<<PATH>>>/sprites.png);","}",".blocklyTreeIconClosedLtr {","  background-position: -32px -1px;","}",".blocklyTreeIconClosedRtl {","  background-position: 0px -1px;","}",".blocklyTreeIconOpen {","  background-position: -16px -1px;","}",".blocklyTreeSelected>.blocklyTreeIconClosedLtr {",
"  background-position: -32px -17px;","}",".blocklyTreeSelected>.blocklyTreeIconClosedRtl {","  background-position: 0px -17px;","}",".blocklyTreeSelected>.blocklyTreeIconOpen {","  background-position: -16px -17px;","}",".blocklyTreeIconNone,",".blocklyTreeSelected>.blocklyTreeIconNone {","  background-position: -48px -1px;","}",".blocklyTreeLabel {","  cursor: default;","  font-family: sans-serif;","  font-size: 16px;","  padding: 0 3px;","  vertical-align: middle;","}",".blocklyTreeSelected  {",
"  background-color: #57e !important;","}",".blocklyTreeSelected .blocklyTreeLabel {","  color: #fff;","}",".blocklyWidgetDiv .goog-palette {","  outline: none;","  cursor: default;","}",".blocklyWidgetDiv .goog-palette-table {","  border: 1px solid #666;","  border-collapse: collapse;","}",".blocklyWidgetDiv .goog-palette-cell {","  height: 13px;","  width: 15px;","  margin: 0;","  border: 0;","  text-align: center;","  vertical-align: middle;","  border-right: 1px solid #666;","  font-size: 1px;",
"}",".blocklyWidgetDiv .goog-palette-colorswatch {","  position: relative;","  height: 13px;","  width: 15px;","  border: 1px solid #666;","}",".blocklyWidgetDiv .goog-palette-cell-hover .goog-palette-colorswatch {","  border: 1px solid #FFF;","}",".blocklyWidgetDiv .goog-palette-cell-selected .goog-palette-colorswatch {","  border: 1px solid #000;","  color: #fff;","}",".blocklyWidgetDiv .goog-menu {","  background: #fff;","  border-color: #ccc #666 #666 #ccc;","  border-style: solid;","  border-width: 1px;",
"  cursor: default;","  font: normal 13px Arial, sans-serif;","  margin: 0;","  outline: none;","  padding: 4px 0;","  position: absolute;","  z-index: 20000;","}",".blocklyWidgetDiv .goog-menuitem {","  color: #000;","  font: normal 13px Arial, sans-serif;","  list-style: none;","  margin: 0;","  padding: 4px 7em 4px 28px;","  white-space: nowrap;","}",".blocklyWidgetDiv .goog-menuitem.goog-menuitem-rtl {","  padding-left: 7em;","  padding-right: 28px;","}",".blocklyWidgetDiv .goog-menu-nocheckbox .goog-menuitem,",
".blocklyWidgetDiv .goog-menu-noicon .goog-menuitem {","  padding-left: 12px;","}",".blocklyWidgetDiv .goog-menu-noaccel .goog-menuitem {","  padding-right: 20px;","}",".blocklyWidgetDiv .goog-menuitem-content {","  color: #000;","  font: normal 13px Arial, sans-serif;","}",".blocklyWidgetDiv .goog-menuitem-disabled .goog-menuitem-accel,",".blocklyWidgetDiv .goog-menuitem-disabled .goog-menuitem-content {","  color: #ccc !important;","}",".blocklyWidgetDiv .goog-menuitem-disabled .goog-menuitem-icon {",
"  opacity: 0.3;","  -moz-opacity: 0.3;","  filter: alpha(opacity=30);","}",".blocklyWidgetDiv .goog-menuitem-highlight,",".blocklyWidgetDiv .goog-menuitem-hover {","  background-color: #d6e9f8;","  border-color: #d6e9f8;","  border-style: dotted;","  border-width: 1px 0;","  padding-bottom: 3px;","  padding-top: 3px;","}",".blocklyWidgetDiv .goog-menuitem-checkbox,",".blocklyWidgetDiv .goog-menuitem-icon {","  background-repeat: no-repeat;","  height: 16px;","  left: 6px;","  position: absolute;",
"  right: auto;","  vertical-align: middle;","  width: 16px;","}",".blocklyWidgetDiv .goog-menuitem-rtl .goog-menuitem-checkbox,",".blocklyWidgetDiv .goog-menuitem-rtl .goog-menuitem-icon {","  left: auto;","  right: 6px;","}",".blocklyWidgetDiv .goog-option-selected .goog-menuitem-checkbox,",".blocklyWidgetDiv .goog-option-selected .goog-menuitem-icon {","  background: url(//ssl.gstatic.com/editor/editortoolbar.png) no-repeat -512px 0;","}",".blocklyWidgetDiv .goog-menuitem-accel {","  color: #999;",
"  direction: ltr;","  left: auto;","  padding: 0 6px;","  position: absolute;","  right: 0;","  text-align: right;","}",".blocklyWidgetDiv .goog-menuitem-rtl .goog-menuitem-accel {","  left: 0;","  right: auto;","  text-align: left;","}",".blocklyWidgetDiv .goog-menuitem-mnemonic-hint {","  text-decoration: underline;","}",".blocklyWidgetDiv .goog-menuitem-mnemonic-separator {","  color: #999;","  font-size: 12px;","  padding-left: 4px;","}",".blocklyWidgetDiv .goog-menuseparator {","  border-top: 1px solid #ccc;",
"  margin: 4px 0;","  padding: 0;","}",""];/*

 Visual Blocks Editor

 Copyright 2011 Google Inc.
 https://developers.google.com/blockly/

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
function El(a,b){function c(){Fl(a);Gl()}if(!jc(document,a))throw"Error: container is not in current document.";b&&Hl(b);if(Lc){var d=document.getElementById("realtime");d&&(d.style.display="block");ll(c,a)}else c()}
function Hl(a){var b=!!a.readOnly;if(b)var c=!1,d=!1,e=!1,f=!1,h=!1,k=null;else(c=a.toolbox)?("string"!=typeof c&&"undefined"==typeof XSLTProcessor&&(c=c.outerHTML),"string"==typeof c&&(c=$c(c))):c=null,k=c,c=Boolean(k&&k.getElementsByTagName("category").length),d=a.trashcan,void 0===d&&(d=c),e=a.collapse,void 0===e&&(e=c),f=a.comments,void 0===f&&(f=c),h=a.disable,void 0===h&&(h=c);if(k&&!c)var m=!1;else m=a.scrollbars,void 0===m&&(m=!0);var p=a.sounds;void 0===p&&(p=!0);var q=!!a.realtime,u=q?a.realtimeOptions:
void 0;C=!!a.rtl;Ic=e;Hc=f;Jc=h;F=b;Nc=a.maxBlocks||Infinity;a.media?me=a.media:a.path&&(me=a.path+"lib/blockly/media/");Oc=c;Pc=m;Fc=d;Qc=p;Kc=k;Lc=q;Mc=u}
function Fl(a){a.setAttribute("dir","LTR");$e=C;Cl();var b=H("svg",{xmlns:"http://www.w3.org/2000/svg","xmlns:html":"http://www.w3.org/1999/xhtml","xmlns:xlink":"http://www.w3.org/1999/xlink",version:"1.1","class":"blocklySvg"},null),c=H("defs",{},b),d,e;d=H("filter",{id:"blocklyEmboss"},c);H("feGaussianBlur",{"in":"SourceAlpha",stdDeviation:1,result:"blur"},d);e=H("feSpecularLighting",{"in":"blur",surfaceScale:1,specularConstant:.5,specularExponent:10,"lighting-color":"white",result:"specOut"},d);
H("fePointLight",{x:-5E3,y:-1E4,z:2E4},e);H("feComposite",{"in":"specOut",in2:"SourceAlpha",operator:"in",result:"specOut"},d);H("feComposite",{"in":"SourceGraphic",in2:"specOut",operator:"arithmetic",k1:0,k2:1,k3:1,k4:0},d);d=H("filter",{id:"blocklyTrashcanShadowFilter"},c);H("feGaussianBlur",{"in":"SourceAlpha",stdDeviation:2,result:"blur"},d);H("feOffset",{"in":"blur",dx:1,dy:1,result:"offsetBlur"},d);d=H("feMerge",{},d);H("feMergeNode",{"in":"offsetBlur"},d);H("feMergeNode",{"in":"SourceGraphic"},
d);d=H("filter",{id:"blocklyShadowFilter"},c);H("feGaussianBlur",{stdDeviation:2},d);c=H("pattern",{id:"blocklyDisabledPattern",patternUnits:"userSpaceOnUse",width:10,height:10},c);H("rect",{width:10,height:10,fill:"#aaa"},c);H("path",{d:"M 0 0 L 10 10 M 10 0 L 0 10",stroke:"#cc0"},c);y=new pe(Il,Jl);b.appendChild(y.I());y.tg=Nc;F||(Oc?ok(b,a):(y.qa=new Ph,c=y.qa,d=c.I(),c.ud=!1,ic(d),Kl(function(){if(0==Be){var a=y.Tb();if(0>a.qb||a.qb+a.Va>a.za+a.Lb||a.Pb<(C?a.Oa:0)||a.Pb+a.Wc>(C?a.V:a.V+a.Oa))for(var b=
Wc(y,!1),c=0,d;d=b[c];c++){var e=D(d),q=vg(d),u=a.Lb+25-q.height-e.y;0<u&&d.moveBy(0,u);u=a.Lb+a.za-25-e.y;0>u&&d.moveBy(0,u);u=25+a.Oa-e.x-(C?0:q.width);0<u&&d.moveBy(u,0);u=a.Oa+a.V-25-e.x+(C?q.width:0);0>u&&d.moveBy(u,0);d.ec&&!F&&50<(C?e.x-a.V:-e.x)&&d.i(!1,!0)}}})));b.appendChild(yi());a.appendChild(b);Gc=b;Ui();ah=dc("div","blocklyWidgetDiv");ah.style.direction=C?"rtl":"ltr";document.body.appendChild(ah)}
function Gl(){K(Gc,"mousedown",null,Ll);K(Gc,"contextmenu",null,Ml);K(ah,"contextmenu",null,Ml);Rc||(K(window,"resize",document,Ui),K(document,"keydown",null,Nl),document.addEventListener("mouseup",Ol,!1),Lb&&K(window,"orientationchange",document,function(){De(window,"resize")}),Rc=!0);if(Kc)if(Oc)pk();else{y.qa.o(y);y.qa.show(Kc.childNodes);y.scrollX=y.qa.O;C&&(y.scrollX*=-1);var a="translate("+y.scrollX+", 0)";y.Q.setAttribute("transform",a);y.ld.setAttribute("transform",a)}Pc&&(y.qc=new od(y),
y.qc.resize());se();if(Qc){Pl([me+"click.mp3",me+"click.wav",me+"click.ogg"],"click");Pl([me+"delete.mp3",me+"delete.ogg",me+"delete.wav"],"delete");var b=[],a=function(){for(;b.length;)J(b.pop());for(var a in Ql){var d=Ql[a];d.volume=.01;d.play();d.pause();if(Lb||Kb)break}};b.push(K(document,"mousemove",null,a));b.push(K(document,"touchstart",null,a))}};/*

 Visual Blocks Editor

 Copyright 2013 Google Inc.
 https://developers.google.com/blockly/

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
var ah=null,Yg=null,Rl=null;function Xg(a,b){Zg();Yg=a;Rl=b;ah.style.display="block"}function Zg(){Yg&&(ah.style.display="none",Rl&&Rl(),Rl=Yg=null,hc(ah))}function bh(a,b,c,d){b<d.y&&(b=d.y);C?a>c.width+d.x&&(a=c.width+d.x):a<d.x&&(a=d.x);ah.style.left=a+"px";ah.style.top=b+"px"};/*

 Visual Blocks Editor

 Copyright 2012 Google Inc.
 https://developers.google.com/blockly/

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
/*

 Visual Blocks Editor

 Copyright 2013 Google Inc.
 https://developers.google.com/blockly/

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
function Sl(a,b){var c;if(b){var d=this;c=function(a){a=Tl.call(d,a);null!==a&&b.call(d,a);return a}}else c=Tl;this.Xg=H("tspan",{},null);this.Xg.appendChild(document.createTextNode("\u00b0"));Sl.m.constructor.call(this,a,c)}s(Sl,pl);Sl.prototype.clone=function(){return new Sl(this.Ea(),this.Ia)};
Sl.prototype.sf=function(){Sl.m.sf.call(this,Hb||Jb||Lb);var a=ah;if(a.firstChild){var a=H("svg",{xmlns:"http://www.w3.org/2000/svg","xmlns:html":"http://www.w3.org/1999/xhtml","xmlns:xlink":"http://www.w3.org/1999/xlink",version:"1.1",height:"100px",width:"100px"},a),b=H("circle",{cx:50,cy:50,r:49,"class":"blocklyAngleCircle"},a);this.Cd=H("path",{"class":"blocklyAngleGauge"},a);this.Ve=H("line",{x1:50,y1:50,"class":"blocklyAngleLine"},a);for(var c=0;360>c;c+=15)H("line",{x1:99,y1:50,x2:99-(0==c%
45?10:5),y2:50,"class":"blocklyAngleMarks",transform:"rotate("+c+", 50, 50)"},a);a.style.marginLeft="-35px";K(a,"click",this,Zg);K(b,"mousemove",this,this.df);K(this.Cd,"mousemove",this,this.df);Ul(this)}};Sl.prototype.df=function(a){var b=this.Cd.ownerSVGElement.getBoundingClientRect(),c=a.clientX-b.left-50;a=a.clientY-b.top-50;b=Math.atan(-a/c);isNaN(b)||(b=180*b/Math.PI,0>c?b+=180:0<a&&(b+=360),b=15*Math.round(b/15),360<=b&&(b-=360),b=String(b),ql.value=b,this.T(b))};
Sl.prototype.T=function(a){Sl.m.T.call(this,a);Ul(this);C?this.ma.insertBefore(this.Xg,this.ma.firstChild):this.ma.appendChild(this.Xg);this.kd.width=0};
function Ul(a){if(a.Cd){var b;b=Number(a.Ea())*Math.PI/180;if(isNaN(b))a.Cd.setAttribute("d","M 50, 50"),a.Ve.setAttribute("x2",50),a.Ve.setAttribute("y2",50);else{var c=50+49*Math.cos(b),d=50+-49*Math.sin(b);a.Cd.setAttribute("d","M 50, 50 h 49 A 49,49 0 "+(b>Math.PI?1:0)+" 0 "+c+","+d+" z");a.Ve.setAttribute("x2",c);a.Ve.setAttribute("y2",d)}}}function Tl(a){a=ul(a);null!==a&&(a%=360,0>a&&(a+=360),a=String(a));return a};/*

 Visual Blocks Editor

 Copyright 2011 Google Inc.
 https://developers.google.com/blockly/

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
var me="https://blockly-demo.appspot.com/static/media/",ke=64,le=92,ne="sprites.png",Xh=[,2,1,4,3],Ql=Object.create(null),L=null,F=!1,Uh=null,Vh=null,jj=5,zh=20,$h=250,kj=30,y=null,Vl=null,Wl=null;function Gi(){return{width:Gc.vh,height:Gc.uh}}function Ui(){var a=Gc,b=a.parentNode,c=b.offsetWidth,b=b.offsetHeight;a.vh!=c&&(a.setAttribute("width",c+"px"),a.vh=c);a.uh!=b&&(a.setAttribute("height",b+"px"),a.uh=b);y.qc&&y.qc.resize()}
function Ll(a){Ui();Ri();zd();var b=a.target&&a.target.nodeName&&"svg"==a.target.nodeName.toLowerCase();!F&&L&&b&&Ce();a.target==Gc&&vd(a)?Xl(a):(F||b)&&y.qc&&(y.Xf=!0,y.Tg=a.clientX,y.Ug=a.clientY,y.al=y.Tb(),y.cl=y.scrollX,y.dl=y.scrollY,"mouseup"in Kk&&(Wl=K(document,"mouseup",null,Ol)),Sc=K(document,"mousemove",null,Yl))}function Ol(){Lh(!1);y.Xf=!1;Wl&&(J(Wl),Wl=null);Sc&&(J(Sc),Sc=null)}
function Yl(a){if(y.Xf){yd();var b=y.al,c=y.cl+(a.clientX-y.Tg),d=y.dl+(a.clientY-y.Ug),c=Math.min(c,-b.Pb),d=Math.min(d,-b.qb),c=Math.max(c,b.V-b.Pb-b.Wc),d=Math.max(d,b.za-b.qb-b.Va);y.qc.set(-c-b.Pb,-d-b.qb);a.stopPropagation()}}
function Nl(a){if(!Kh(a))if(27==a.keyCode)zd();else if(8==a.keyCode||46==a.keyCode)try{L&&L.ec&&!F&&(zd(),L.i(!0,!0))}finally{a.preventDefault()}else if(a.altKey||a.ctrlKey||a.metaKey)if(L&&L.ec&&!F&&L.Hb&&!F&&L.u==y&&(zd(),67==a.keyCode?Zl():88==a.keyCode&&(Zl(),L.i(!0,!0))),86==a.keyCode&&Vl){a=y;var b=Vl;if(!(b.getElementsByTagName("block").length>=Ee(a))){var c=bd(a,b),d=parseInt(b.getAttribute("x"),10),b=parseInt(b.getAttribute("y"),10);if(!isNaN(d)&&!isNaN(b)){C&&(d=-d);do for(var e=!1,f=xe(a),
h=0,k;k=f[h];h++)k=D(k),1>=Math.abs(d-k.x)&&1>=Math.abs(b-k.y)&&(d=C?d-zh:d+zh,b+=2*zh,e=!0);while(e);c.moveBy(d,b)}c.select()}}}function Ri(){Oi&&(J(Oi),Oi=null);Pi&&(J(Pi),Pi=null);var a=L;if(2==Be&&a){var b=D(a);di(a,b.x-a.Pi,b.y-a.Qi);delete a.ze;ij(a,!1);a.v();ie(a.Pa,$h,a);De(window,"resize")}a&&re(a.u);Be=0;Ej()}function Zl(){var a=L,b=Xc(a);nd(b);a=D(a);b.setAttribute("x",C?-a.x:a.x);b.setAttribute("y",a.y);Vl=b}
function Xl(a){if(!F){var b=[];if(Ic){for(var c=!1,d=!1,e=Wc(y,!1),f=0;f<e.length;f++)for(var h=e[f];h;)h.isCollapsed()?c=!0:d=!0,h=Yc(h);d={enabled:d};d.text=$l;d.pb=function(){for(var a=0,b=0;b<e.length;b++)for(var c=e[b];c;)setTimeout(c.Vd.bind(c,!0),a),c=Yc(c),a+=10};b.push(d);c={enabled:c};c.text=am;c.pb=function(){for(var a=0,b=0;b<e.length;b++)for(var c=e[b];c;)setTimeout(c.Vd.bind(c,!1),a),c=Yc(c),a+=10};b.push(c)}yh.show(a,b)}}function Ml(a){Kh(a)||a.preventDefault()}
function zd(a){Ci();Zg();!a&&kk&&kk.ud&&lk.Qc(null)}function yd(){if(window.getSelection){var a=window.getSelection();a&&a.removeAllRanges&&(a.removeAllRanges(),window.setTimeout(function(){try{window.getSelection().removeAllRanges()}catch(a){}},0))}}function Kh(a){return"textarea"==a.target.type||"text"==a.target.type}
function Pl(a,b){if(window.Audio&&a.length){for(var c,d=new window.Audio,e=0;e<a.length;e++){var f=a[e],h=f.match(/\.(\w+)$/);if(h&&d.canPlayType("audio/"+h[1])){c=new window.Audio(f);break}}c&&c.play&&(Ql[b]=c)}}function Qi(a,b){var c=Ql[a];c&&(c=Rb&&9===Rb||Lb||Jb?c:c.cloneNode(),c.volume=void 0===b?1:b,c.play())}function Lh(a){if(!F){var b="";a&&(b="url("+me+"handclosed.cur) 7 3, auto");L&&(O(L).style.cursor=b);Gc.style.cursor=b}}
function Il(){var a=Gi();a.width-=mk;var b=a.width-I,c=a.height-I;try{var d=y.Q.getBBox()}catch(e){return null}if(y.qc)var f=Math.min(d.x-b/2,d.x+d.width-b),b=Math.max(d.x+d.width+b/2,d.x+b),h=Math.min(d.y-c/2,d.y+d.height-c),c=Math.max(d.y+d.height+c/2,d.y+c);else f=d.x,b=f+d.width,h=d.y,c=h+d.height;return{za:a.height,V:a.width,Va:c-h,Wc:b-f,Lb:-y.scrollY,Oa:-y.scrollX,qb:h,Pb:f,gb:0,fb:C?0:mk}}
function Jl(a){if(!y.qc)throw"Attempt to set main workspace scroll without scrollbars.";var b=Il();ga(a.x)&&(y.scrollX=-b.Wc*a.x-b.Pb);ga(a.y)&&(y.scrollY=-b.Va*a.y-b.qb);a="translate("+(y.scrollX+b.fb)+","+(y.scrollY+b.gb)+")";y.Q.setAttribute("transform",a);y.ld.setAttribute("transform",a)}function Wi(a){a()}function Kl(a){return K(y.Q,"blocklyWorkspaceChange",null,a)}window.Blockly||(window.Blockly={});window.Blockly.getMainWorkspace=function(){return y};window.Blockly.addChangeListener=Kl;
window.Blockly.removeChangeListener=function(a){J(a)};var Zi="Adicionar Coment\u00e1rio",nl="Por favor autorize este aplicativo para permitir que o seu trabalho seja gravado e que ele seja compartilhado por voc\u00ea.",rl="Mudar valor:",kl="Converse com o seu colaborador digitando nesta caixa!",$l="Recolher Blocos",cj="Recolher Bloco",fj="Remover Bloco",gj="Remover %1 Blocos",ej="Desabilitar Bloco",Xi="Duplicar",dj="Habilitar Bloco",am="Expandir Blocos",bj="Expandir Bloco",$i="Entrada externa",hj="Ajuda",aj="Entradas Internas",ol="Eu",Ek="Nova vari\u00e1vel...",
Gk="Nome da nova vari\u00e1vel:",Yi="Remover Coment\u00e1rio",Dk="Renomear vari\u00e1vel...",Fk="Renomear todas as vari\u00e1veis '%1' para:";function bm(a,b){var c;c=a.className;for(var d=c=n(c)&&c.match(/\S+/g)||[],e=ab(arguments,1),f=0;f<e.length;f++)Xa(d,e[f])||d.push(e[f]);a.className=c.join(" ")};var cm={ace:"\u0628\u0647\u0633\u0627 \u0627\u0686\u064a\u0647",af:"Afrikaans",ar:"\u0627\u0644\u0639\u0631\u0628\u064a\u0629",az:"Az\u0259rbaycanca","be-tarask":"Tara\u0161kievica",br:"Brezhoneg",ca:"Catal\u00e0",cdo:"\u95a9\u6771\u8a9e",cs:"\u010cesky",da:"Dansk",de:"Deutsch",el:"\u0395\u03bb\u03bb\u03b7\u03bd\u03b9\u03ba\u03ac",en:"English",es:"Espa\u00f1ol",eu:"Euskara",fa:"\u0641\u0627\u0631\u0633\u06cc",fi:"Suomi",fo:"F\u00f8royskt",fr:"Fran\u00e7ais",frr:"Frasch",gl:"Galego",hak:"\u5ba2\u5bb6\u8a71",
he:"\u05e2\u05d1\u05e8\u05d9\u05ea",hi:"\u0939\u093f\u0928\u094d\u0926\u0940",hrx:"Hunsrik",hu:"Magyar",ia:"Interlingua",id:"Bahasa Indonesia",is:"\u00cdslenska",it:"Italiano",ja:"\u65e5\u672c\u8a9e",ka:"\u10e5\u10d0\u10e0\u10d7\u10e3\u10da\u10d8",km:"\u1797\u17b6\u179f\u17b6\u1781\u17d2\u1798\u17c2\u179a",ko:"\ud55c\uad6d\uc5b4",ksh:"Ripoar\u0117sch",ky:"\u041a\u044b\u0440\u0433\u044b\u0437\u0447\u0430",la:"Latine",lb:"L\u00ebtzebuergesch",lt:"Lietuvi\u0173",lv:"Latvie\u0161u",mg:"Malagasy",ml:"\u0d2e\u0d32\u0d2f\u0d3e\u0d33\u0d02",
mk:"\u041c\u0430\u043a\u0435\u0434\u043e\u043d\u0441\u043a\u0438",mr:"\u092e\u0930\u093e\u0920\u0940",ms:"Bahasa Melayu",mzn:"\u0645\u0627\u0632\u0650\u0631\u0648\u0646\u06cc",nb:"Norsk Bokm\u00e5l",nl:"Nederlands, Vlaams",oc:"Lenga d'\u00f2c",pa:"\u092a\u0902\u091c\u093e\u092c\u0940",pl:"Polski",pms:"Piemont\u00e8is",ps:"\u067e\u069a\u062a\u0648",pt:"Portugu\u00eas","pt-br":"Portugu\u00eas Brasileiro",ro:"Rom\u00e2n\u0103",ru:"\u0420\u0443\u0441\u0441\u043a\u0438\u0439",sc:"Sardu",sco:"Scots",si:"\u0dc3\u0dd2\u0d82\u0dc4\u0dbd",
sk:"Sloven\u010dina",sr:"\u0421\u0440\u043f\u0441\u043a\u0438",sv:"Svenska",sw:"Kishwahili",th:"\u0e20\u0e32\u0e29\u0e32\u0e44\u0e17\u0e22",tl:"Tagalog",tlh:"tlhIngan Hol",tr:"T\u00fcrk\u00e7e",uk:"\u0423\u043a\u0440\u0430\u0457\u043d\u0441\u044c\u043a\u0430",vi:"Ti\u1ebfng Vi\u1ec7t","zh-hans":"\u7c21\u9ad4\u4e2d\u6587","zh-hant":"\u6b63\u9ad4\u4e2d\u6587"},dm="ace ar fa he mzn ps".split(" "),Cc=window.BlocklyGamesLang,em=window.BlocklyGamesLanguages,Dc=!!window.location.pathname.match(/\.html$/);
function fm(a,b){var c=window.location.search.match(new RegExp("[?&]"+a+"=([^&]+)"));return c?decodeURIComponent(c[1].replace(/\+/g,"%20")):b}var B,gm=Number(fm("level","NaN"));B=isNaN(gm)?1:Math.min(Math.max(1,gm),10);
function hm(){document.title=document.getElementById("title").textContent;document.head.parentElement.setAttribute("dir",-1!=dm.indexOf(Cc)?"rtl":"ltr");document.head.parentElement.setAttribute("lang",Cc);for(var a=[],b=0;b<em.length;b++){var c=em[b];a.push([cm[c],c])}a.sort(function(a,b){return a[0]>b[0]?1:a[0]<b[0]?-1:0});for(var d=document.getElementById("languageMenu"),b=d.options.length=0;b<a.length;b++){var e=a[b],c=e[1],e=new Option(e[0],c);c==Cc&&(e.selected=!0);d.options.add(e)}1>=d.options.length&&
(d.style.display="none");for(b=1;10>=b;b++)a=document.getElementById("level"+b),c=!!im(b),a&&c&&bm(a,"level_done");(b=document.querySelector('meta[name="viewport"]'))&&725>screen.availWidth&&b.setAttribute("content","width=725, initial-scale=.35, user-scalable=no");setTimeout(jm,1)}function im(a){var b=km,c;try{c=window.localStorage[b+a]}catch(d){}return c}
function lm(a){var b;(b=document.getElementById(a))?(b=b.textContent,b=b.replace(/\\n/g,"\n")):b=null;return null===b?"[Unknown message: "+a+"]":b}function mm(a,b){"string"==typeof a&&(a=document.getElementById(a));a.addEventListener("click",b,!0);a.addEventListener("touchend",b,!0)}
function jm(){if(!Dc){window.GoogleAnalyticsObject="GoogleAnalyticsFunction";var a=function(){(a.q=a.q||[]).push(arguments)};window.GoogleAnalyticsFunction=a;a.l=1*new Date;var b=document.createElement("script");b.async=1;b.src="//www.google-analytics.com/analytics.js";document.head.appendChild(b);a("create","UA-50448074-1","auto");a("send","pageview")}};var Y={Xc:null,o:function(){hm();var a=document.getElementById("linkButton");"BlocklyStorage"in window?(BlocklyStorage.HTTPREQUEST_ERROR=lm("Games_httpRequestError"),BlocklyStorage.LINK_ALERT=lm("Games_linkAlert"),BlocklyStorage.HASH_ERROR=lm("Games_hashError"),BlocklyStorage.XML_ERROR=lm("Games_xmlError"),BlocklyStorage.alert=uc.hl,a&&mm(a,BlocklyStorage.link)):a&&(a.style.display="none");document.getElementById("languageMenu").addEventListener("change",Y.xj,!0)},dm:function(a){document.body.innerHTML=
a;a=document.getElementById("blockly");a.style.height=window.innerHeight+"px";El(a,{media:"lib/blockly/media/",readOnly:!0,mm:-1!=dm.indexOf(Cc),scrollbars:!1});a=fm("xml","");Y.Mg("<xml>"+a+"</xml>")},vk:function(a,b){if("BlocklyStorage"in window&&1<window.location.hash.length)BlocklyStorage.retrieveXml(window.location.hash.substring(1));else{var c=null;try{c=window.sessionStorage.mi}catch(d){}c&&delete window.sessionStorage.mi;var e=im(B),f=b&&im(B-1);(c=c||e||f||a)&&Y.Mg(c)}},Mg:function(a){Y.Xc?Y.Xc.setValue(a,
-1):(a=$c(a),ad(y,a))},Wk:function(){if(void 0!=typeof Ec&&window.localStorage){var a=km+B;if(Y.Xc)var b=Y.Xc.getValue();else b=Vc(y),b=Zc(b);window.localStorage[a]=b}},Oe:function(){window.location=(Dc?"index.html":"./")+"?lang="+Cc},xj:function(){if(window.sessionStorage){if(Y.Xc)var a=Y.Xc.getValue();else a=Vc(y),a=Zc(a);window.sessionStorage.mi=a}var a=document.getElementById("languageMenu"),a=encodeURIComponent(a.options[a.selectedIndex].value),b=window.location.search,b=1>=b.length?"?lang="+
a:b.match(/[?&]lang=[^&]*/)?b.replace(/([?&]lang=)[^&]*/,"$1"+a):b.replace(/\?/,"?lang="+a+"&");window.location=window.location.protocol+"//"+window.location.host+window.location.pathname+b},Le:function(a){if(a){var b=a.match(/^block_id_(\d+)$/);b&&(a=b[1])}Ae(a)},il:function(a){return a.replace(/(,\s*)?'block_id_\d+'\)/g,")").trimRight()},Cb:function(a){if("click"==a.type&&"touchend"==Y.Cb.Dg&&Y.Cb.Cg+2E3>Date.now()||Y.Cb.Dg==a.type&&Y.Cb.Cg+400>Date.now())return a.preventDefault(),a.stopPropagation(),
!0;Y.Cb.Dg=a.type;Y.Cb.Cg=Date.now();return!1}};Y.Cb.Dg=null;Y.Cb.Cg=0;Y.lk=function(){var a=document.createElement("script");a.setAttribute("type","text/javascript");a.setAttribute("src","lib/blockly/js-read-only/JS-Interpreter/compiled.js");document.head.appendChild(a)};
Y.nk=function(){var a=document.createElement("link");a.setAttribute("rel","stylesheet");a.setAttribute("type","text/css");a.setAttribute("href","lib/blockly/common/prettify.css");document.head.appendChild(a);a=document.createElement("script");a.setAttribute("type","text/javascript");a.setAttribute("src","lib/blockly/common/prettify.js");document.head.appendChild(a)};window.BlocklyInterface=Y;Y.setCode=Y.Mg;var Z={Hc:!1,Ph:null,xe:null,rf:function(a,b,c,d,e,f){function h(){Z.Hc&&(k.style.visibility="visible",k.style.zIndex=10,m.style.visibility="hidden")}Z.Hc&&Z.Eb(!1);zd(!0);Z.Hc=!0;Z.Ph=b;Z.xe=f;var k=document.getElementById("dialog");f=document.getElementById("dialogShadow");var m=document.getElementById("dialogBorder"),p;for(p in e)k.style[p]=e[p];d&&(f.style.visibility="visible",f.style.opacity=.3,f.style.zIndex=9,d=document.createElement("div"),d.id="dialogHeader",k.appendChild(d),Z.Sf=K(d,"mousedown",
null,Z.Oj));k.appendChild(a);a.className=a.className.replace("dialogHiddenContent","");c&&b?(Z.We(b,!1,.2),Z.We(k,!0,.8),setTimeout(h,175)):h()},Qh:0,Rh:0,Oj:function(a){Z.Vf();if(!vd(a)){var b=document.getElementById("dialog");Z.Qh=b.offsetLeft-a.clientX;Z.Rh=b.offsetTop-a.clientY;Z.Uf=K(document,"mouseup",null,Z.Vf);Z.Tf=K(document,"mousemove",null,Z.Pj);a.stopPropagation()}},Pj:function(a){var b=document.getElementById("dialog"),c=Z.Qh+a.clientX;a=Z.Rh+a.clientY;a=Math.max(a,0);a=Math.min(a,window.innerHeight-
b.offsetHeight);c=Math.max(c,0);c=Math.min(c,window.innerWidth-b.offsetWidth);b.style.left=c+"px";b.style.top=a+"px"},Vf:function(){Z.Uf&&(J(Z.Uf),Z.Uf=null);Z.Tf&&(J(Z.Tf),Z.Tf=null)},Eb:function(a){function b(){d.style.zIndex=-1;d.style.visibility="hidden";document.getElementById("dialogBorder").style.visibility="hidden"}if(Z.Hc){Z.Vf();Z.Sf&&(J(Z.Sf),Z.Sf=null);Z.Hc=!1;Z.xe&&Z.xe();Z.xe=null;var c=!1===a?null:Z.Ph;a=document.getElementById("dialog");var d=document.getElementById("dialogShadow");
d.style.opacity=0;c?(Z.We(a,!1,.8),Z.We(c,!0,.2),setTimeout(b,175)):b();a.style.visibility="hidden";a.style.zIndex=-1;for((c=document.getElementById("dialogHeader"))&&c.parentNode.removeChild(c);a.firstChild;)c=a.firstChild,c.className+=" dialogHiddenContent",document.body.appendChild(c)}},We:function(a,b,c){function d(){e.style.width=f.width+"px";e.style.height=f.height+"px";e.style.left=f.x+"px";e.style.top=f.y+"px";e.style.opacity=c}if(a){var e=document.getElementById("dialogBorder"),f=Z.Uj(a);
b?(e.className="dialogAnimate",setTimeout(d,1)):(e.className="",d());e.style.visibility="visible"}},Uj:function(a){if(a.getBBox){var b=a.getBBox(),c=b.height,b=b.width;a=$g(a);var d=a.x,e=a.y}else{c=a.offsetHeight;b=a.offsetWidth;e=d=0;do d+=a.offsetLeft,e+=a.offsetTop,a=a.offsetParent;while(a)}return{height:c,width:b,x:d,y:e}},hl:function(a){var b=document.getElementById("containerStorage");b.textContent="";a=a.split("\n");for(var c=0;c<a.length;c++){var d=document.createElement("p");d.appendChild(document.createTextNode(a[c]));
b.appendChild(d)}b=document.getElementById("dialogStorage");a=document.getElementById("linkButton");Z.rf(b,a,!0,!0,{width:"50%",left:"25%",top:"5em"},Z.fl);Z.Zk()},nh:function(){if(!im(B))if(Z.Hc||0!=Be)setTimeout(Z.nh,15E3);else{var a=document.getElementById("dialogAbort"),b=document.getElementById("abortCancel");b.addEventListener("click",Z.Eb,!0);b.addEventListener("touchend",Z.Eb,!0);b=document.getElementById("abortOk");b.addEventListener("click",Y.Oe,!0);b.addEventListener("touchend",Y.Oe,!0);
Z.rf(a,null,!1,!0,{width:"40%",left:"30%",top:"3em"},function(){document.body.removeEventListener("keydown",Z.mh,!0)});document.body.addEventListener("keydown",Z.mh,!0)}},Aj:function(){var a=document.getElementById("dialogDone");if(y){var b=document.getElementById("dialogLinesText");b.textContent="";var c=wl(),c=Y.il(c),d=c.split("\n").length,e=document.getElementById("containerCode");e.textContent=c;"function"==typeof prettyPrintOne&&(c=e.innerHTML,c=prettyPrintOne(c,"js"),e.innerHTML=c);c=1==d?
lm("Games_linesOfCode1"):lm("Games_linesOfCode2").replace("%1",d);b.appendChild(document.createTextNode(c))}c=10>B?lm("Games_nextLevel").replace("%1",B+1):lm("Games_finalLevel");b=document.getElementById("doneCancel");b.addEventListener("click",Z.Eb,!0);b.addEventListener("touchend",Z.Eb,!0);b=document.getElementById("doneOk");b.addEventListener("click",Y.vg,!0);b.addEventListener("touchend",Y.vg,!0);Z.rf(a,null,!1,!0,{width:"40%",left:"30%",top:"3em"},function(){document.body.removeEventListener("keydown",
Z.zh,!0)});document.body.addEventListener("keydown",Z.zh,!0);document.getElementById("dialogDoneText").textContent=c},Oh:function(a){!Z.Hc||13!=a.keyCode&&27!=a.keyCode&&32!=a.keyCode||(Z.Eb(!0),a.stopPropagation(),a.preventDefault())},Zk:function(){document.body.addEventListener("keydown",Z.Oh,!0)},fl:function(){document.body.removeEventListener("keydown",Z.Oh,!0)},zh:function(a){if(13==a.keyCode||27==a.keyCode||32==a.keyCode)Z.Eb(!0),a.stopPropagation(),a.preventDefault(),27!=a.keyCode&&Y.vg()},
mh:function(a){if(13==a.keyCode||27==a.keyCode||32==a.keyCode)Z.Eb(!0),a.stopPropagation(),a.preventDefault(),27!=a.keyCode&&Y.Oe()}};window.BlocklyDialogs=Z;Z.hideDialog=Z.Eb;/*

 Visual Blocks Language

 Copyright 2012 Google Inc.
 https://developers.google.com/blockly/

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
var W=new vl("JavaScript");Al("Blockly,break,case,catch,continue,debugger,default,delete,do,else,finally,for,function,if,in,instanceof,new,return,switch,this,throw,try,typeof,var,void,while,with,class,enum,export,extends,import,super,implements,interface,let,package,private,protected,public,static,yield,const,null,true,false,Array,ArrayBuffer,Boolean,Date,decodeURI,decodeURIComponent,encodeURI,encodeURIComponent,Error,eval,EvalError,Float32Array,Float64Array,Function,Infinity,Int16Array,Int32Array,Int8Array,isFinite,isNaN,Iterator,JSON,Math,NaN,Number,Object,parseFloat,parseInt,RangeError,ReferenceError,RegExp,StopIteration,String,SyntaxError,TypeError,Uint16Array,Uint32Array,Uint8Array,Uint8ClampedArray,undefined,uneval,URIError,applicationCache,closed,Components,content,_content,controllers,crypto,defaultStatus,dialogArguments,directories,document,frameElement,frames,fullScreen,globalStorage,history,innerHeight,innerWidth,length,location,locationbar,localStorage,menubar,messageManager,mozAnimationStartTime,mozInnerScreenX,mozInnerScreenY,mozPaintCount,name,navigator,opener,outerHeight,outerWidth,pageXOffset,pageYOffset,parent,performance,personalbar,pkcs11,returnValue,screen,screenX,screenY,scrollbars,scrollMaxX,scrollMaxY,scrollX,scrollY,self,sessionStorage,sidebar,status,statusbar,toolbar,top,URL,window,addEventListener,alert,atob,back,blur,btoa,captureEvents,clearImmediate,clearInterval,clearTimeout,close,confirm,disableExternalCapture,dispatchEvent,dump,enableExternalCapture,escape,find,focus,forward,GeckoActiveXObject,getAttention,getAttentionWithCycleCount,getComputedStyle,getSelection,home,matchMedia,maximize,minimize,moveBy,moveTo,mozRequestAnimationFrame,open,openDialog,postMessage,print,prompt,QueryInterface,releaseEvents,removeEventListener,resizeBy,resizeTo,restore,routeEvent,scroll,scrollBy,scrollByLines,scrollByPages,scrollTo,setCursor,setImmediate,setInterval,setResizable,setTimeout,showModalDialog,sizeToContent,stop,unescape,updateCommands,XPCNativeWrapper,XPCSafeJSObjectWrapper,onabort,onbeforeunload,onblur,onchange,onclick,onclose,oncontextmenu,ondevicemotion,ondeviceorientation,ondragdrop,onerror,onfocus,onhashchange,onkeydown,onkeypress,onkeyup,onload,onmousedown,onmousemove,onmouseout,onmouseover,onmouseup,onmozbeforepaint,onpaint,onpopstate,onreset,onresize,onscroll,onselect,onsubmit,onunload,onpageshow,onpagehide,Image,Option,Worker,Event,Range,File,FileReader,Blob,BlobBuilder,Attr,CDATASection,CharacterData,Comment,console,DocumentFragment,DocumentType,DomConfiguration,DOMError,DOMErrorHandler,DOMException,DOMImplementation,DOMImplementationList,DOMImplementationRegistry,DOMImplementationSource,DOMLocator,DOMObject,DOMString,DOMStringList,DOMTimeStamp,DOMUserData,Entity,EntityReference,MediaQueryList,MediaQueryListListener,NameList,NamedNodeMap,Node,NodeFilter,NodeIterator,NodeList,Notation,Plugin,PluginArray,ProcessingInstruction,SharedWorker,Text,TimeRanges,Treewalker,TypeInfo,UserDataHandler,Worker,WorkerGlobalScope,HTMLDocument,HTMLElement,HTMLAnchorElement,HTMLAppletElement,HTMLAudioElement,HTMLAreaElement,HTMLBaseElement,HTMLBaseFontElement,HTMLBodyElement,HTMLBRElement,HTMLButtonElement,HTMLCanvasElement,HTMLDirectoryElement,HTMLDivElement,HTMLDListElement,HTMLEmbedElement,HTMLFieldSetElement,HTMLFontElement,HTMLFormElement,HTMLFrameElement,HTMLFrameSetElement,HTMLHeadElement,HTMLHeadingElement,HTMLHtmlElement,HTMLHRElement,HTMLIFrameElement,HTMLImageElement,HTMLInputElement,HTMLKeygenElement,HTMLLabelElement,HTMLLIElement,HTMLLinkElement,HTMLMapElement,HTMLMenuElement,HTMLMetaElement,HTMLModElement,HTMLObjectElement,HTMLOListElement,HTMLOptGroupElement,HTMLOptionElement,HTMLOutputElement,HTMLParagraphElement,HTMLParamElement,HTMLPreElement,HTMLQuoteElement,HTMLScriptElement,HTMLSelectElement,HTMLSourceElement,HTMLSpanElement,HTMLStyleElement,HTMLTableElement,HTMLTableCaptionElement,HTMLTableCellElement,HTMLTableDataCellElement,HTMLTableHeaderCellElement,HTMLTableColElement,HTMLTableRowElement,HTMLTableSectionElement,HTMLTextAreaElement,HTMLTimeElement,HTMLTitleElement,HTMLTrackElement,HTMLUListElement,HTMLUnknownElement,HTMLVideoElement,HTMLCanvasElement,CanvasRenderingContext2D,CanvasGradient,CanvasPattern,TextMetrics,ImageData,CanvasPixelArray,HTMLAudioElement,HTMLVideoElement,NotifyAudioAvailableEvent,HTMLCollection,HTMLAllCollection,HTMLFormControlsCollection,HTMLOptionsCollection,HTMLPropertiesCollection,DOMTokenList,DOMSettableTokenList,DOMStringMap,RadioNodeList,SVGDocument,SVGElement,SVGAElement,SVGAltGlyphElement,SVGAltGlyphDefElement,SVGAltGlyphItemElement,SVGAnimationElement,SVGAnimateElement,SVGAnimateColorElement,SVGAnimateMotionElement,SVGAnimateTransformElement,SVGSetElement,SVGCircleElement,SVGClipPathElement,SVGColorProfileElement,SVGCursorElement,SVGDefsElement,SVGDescElement,SVGEllipseElement,SVGFilterElement,SVGFilterPrimitiveStandardAttributes,SVGFEBlendElement,SVGFEColorMatrixElement,SVGFEComponentTransferElement,SVGFECompositeElement,SVGFEConvolveMatrixElement,SVGFEDiffuseLightingElement,SVGFEDisplacementMapElement,SVGFEDistantLightElement,SVGFEFloodElement,SVGFEGaussianBlurElement,SVGFEImageElement,SVGFEMergeElement,SVGFEMergeNodeElement,SVGFEMorphologyElement,SVGFEOffsetElement,SVGFEPointLightElement,SVGFESpecularLightingElement,SVGFESpotLightElement,SVGFETileElement,SVGFETurbulenceElement,SVGComponentTransferFunctionElement,SVGFEFuncRElement,SVGFEFuncGElement,SVGFEFuncBElement,SVGFEFuncAElement,SVGFontElement,SVGFontFaceElement,SVGFontFaceFormatElement,SVGFontFaceNameElement,SVGFontFaceSrcElement,SVGFontFaceUriElement,SVGForeignObjectElement,SVGGElement,SVGGlyphElement,SVGGlyphRefElement,SVGGradientElement,SVGLinearGradientElement,SVGRadialGradientElement,SVGHKernElement,SVGImageElement,SVGLineElement,SVGMarkerElement,SVGMaskElement,SVGMetadataElement,SVGMissingGlyphElement,SVGMPathElement,SVGPathElement,SVGPatternElement,SVGPolylineElement,SVGPolygonElement,SVGRectElement,SVGScriptElement,SVGStopElement,SVGStyleElement,SVGSVGElement,SVGSwitchElement,SVGSymbolElement,SVGTextElement,SVGTextPathElement,SVGTitleElement,SVGTRefElement,SVGTSpanElement,SVGUseElement,SVGViewElement,SVGVKernElement,SVGAngle,SVGColor,SVGICCColor,SVGElementInstance,SVGElementInstanceList,SVGLength,SVGLengthList,SVGMatrix,SVGNumber,SVGNumberList,SVGPaint,SVGPoint,SVGPointList,SVGPreserveAspectRatio,SVGRect,SVGStringList,SVGTransform,SVGTransformList,SVGAnimatedAngle,SVGAnimatedBoolean,SVGAnimatedEnumeration,SVGAnimatedInteger,SVGAnimatedLength,SVGAnimatedLengthList,SVGAnimatedNumber,SVGAnimatedNumberList,SVGAnimatedPreserveAspectRatio,SVGAnimatedRect,SVGAnimatedString,SVGAnimatedTransformList,SVGPathSegList,SVGPathSeg,SVGPathSegArcAbs,SVGPathSegArcRel,SVGPathSegClosePath,SVGPathSegCurvetoCubicAbs,SVGPathSegCurvetoCubicRel,SVGPathSegCurvetoCubicSmoothAbs,SVGPathSegCurvetoCubicSmoothRel,SVGPathSegCurvetoQuadraticAbs,SVGPathSegCurvetoQuadraticRel,SVGPathSegCurvetoQuadraticSmoothAbs,SVGPathSegCurvetoQuadraticSmoothRel,SVGPathSegLinetoAbs,SVGPathSegLinetoHorizontalAbs,SVGPathSegLinetoHorizontalRel,SVGPathSegLinetoRel,SVGPathSegLinetoVerticalAbs,SVGPathSegLinetoVerticalRel,SVGPathSegMovetoAbs,SVGPathSegMovetoRel,ElementTimeControl,TimeEvent,SVGAnimatedPathData,SVGAnimatedPoints,SVGColorProfileRule,SVGCSSRule,SVGExternalResourcesRequired,SVGFitToViewBox,SVGLangSpace,SVGLocatable,SVGRenderingIntent,SVGStylable,SVGTests,SVGTextContentElement,SVGTextPositioningElement,SVGTransformable,SVGUnitTypes,SVGURIReference,SVGViewSpec,SVGZoomAndPan");
W.me=0;W.rd=1;W.Il=1;W.Mb=2;W.Gl=3;W.Dl=3;W.cj=4;W.zl=4;W.Kl=4;W.kh=4;W.Jl=4;W.Ll=4;W.El=4;W.ej=5;W.oe=5;W.sd=5;W.gh=6;W.fj=6;W.Bl=7;W.jh=8;W.Fl=8;W.Hl=8;W.hh=9;W.yl=10;W.Cl=11;W.Al=12;W.ih=13;W.dj=14;W.ne=15;W.xl=16;W.ac=17;W.Nb=99;W.o=function(){W.wd=Object.create(null);W.Yh=Object.create(null);W.fe?W.fe.reset():W.fe=new Tc(W.Bf);for(var a=[],b=xk(),c=0;c<b.length;c++)a[c]="var "+W.fe.getName(b[c],xj)+";";W.wd.variables=a.join("\n")};
W.finish=function(a){var b=[],c;for(c in W.wd)b.push(W.wd[c]);return b.join("\n\n")+"\n\n\n"+a};W.Ii=function(a){return a+";\n"};W.hm=function(a){a=a.replace(/\\/g,"\\\\").replace(/\n/g,"\\\n").replace(/'/g,"\\'");return"'"+a+"'"};
W.Ji=function(a,b){var c="";if(!a.K||!a.K.t){var d=qj(a);d&&(c+=yl(d,"// ")+"\n");for(var e=0;e<a.S.length;e++)if(1==a.S[e].type){var f=E(a.S[e].p);if(f){for(var d=[],f=Rh(f),h=0;h<f.length;h++){var k=qj(f[h]);k&&d.push(k)}d.length&&d.push("");(d=d.join("\n"))&&(c+=yl(d,"// "))}}}e=xl(W,a.D&&E(a.D));return c+b+e};/*

 Visual Blocks Language

 Copyright 2012 Google Inc.
 https://developers.google.com/blockly/

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
W.xk={};W.math_number=function(a){return[parseFloat(T(a,"NUM")),W.me]};W.math_arithmetic=function(a){var b={ADD:[" + ",W.gh],MINUS:[" - ",W.fj],MULTIPLY:[" * ",W.ej],DIVIDE:[" / ",W.oe],POWER:[null,W.ac]}[T(a,"OP")],c=b[0],b=b[1],d=X(a,"A",b)||"0";a=X(a,"B",b)||"0";return c?[d+c+a,b]:["Math.pow("+d+", "+a+")",W.Mb]};
W.math_single=function(a){var b=T(a,"OP"),c;if("NEG"==b)return a=X(a,"NUM",W.kh)||"0","-"==a[0]&&(a=" "+a),["-"+a,W.kh];a="SIN"==b||"COS"==b||"TAN"==b?X(a,"NUM",W.oe)||"0":X(a,"NUM",W.Nb)||"0";switch(b){case "ABS":c="Math.abs("+a+")";break;case "ROOT":c="Math.sqrt("+a+")";break;case "LN":c="Math.log("+a+")";break;case "EXP":c="Math.exp("+a+")";break;case "POW10":c="Math.pow(10,"+a+")";break;case "ROUND":c="Math.round("+a+")";break;case "ROUNDUP":c="Math.ceil("+a+")";break;case "ROUNDDOWN":c="Math.floor("+
a+")";break;case "SIN":c="Math.sin("+a+" / 180 * Math.PI)";break;case "COS":c="Math.cos("+a+" / 180 * Math.PI)";break;case "TAN":c="Math.tan("+a+" / 180 * Math.PI)"}if(c)return[c,W.Mb];switch(b){case "LOG10":c="Math.log("+a+") / Math.log(10)";break;case "ASIN":c="Math.asin("+a+") / Math.PI * 180";break;case "ACOS":c="Math.acos("+a+") / Math.PI * 180";break;case "ATAN":c="Math.atan("+a+") / Math.PI * 180";break;default:throw"Unknown math operator: "+b;}return[c,W.oe]};
W.math_constant=function(a){return{PI:["Math.PI",W.rd],E:["Math.E",W.rd],GOLDEN_RATIO:["(1 + Math.sqrt(5)) / 2",W.oe],SQRT2:["Math.SQRT2",W.rd],SQRT1_2:["Math.SQRT1_2",W.rd],INFINITY:["Infinity",W.me]}[T(a,"CONSTANT")]};
W.math_number_property=function(a){var b=X(a,"NUMBER_TO_CHECK",W.sd)||"0",c=T(a,"PROPERTY"),d;if("PRIME"==c)return d=Bl("math_isPrime",["function "+W.$b+"(n) {","  // https://en.wikipedia.org/wiki/Primality_test#Naive_methods","  if (n == 2 || n == 3) {","    return true;","  }","  // False if n is NaN, negative, is 1, or not whole.","  // And false if n is divisible by 2 or 3.","  if (isNaN(n) || n <= 1 || n % 1 != 0 || n % 2 == 0 || n % 3 == 0) {","    return false;","  }","  // Check all the numbers of form 6k +/- 1, up to sqrt(n).",
"  for (var x = 6; x <= Math.sqrt(n) + 1; x += 6) {","    if (n % (x - 1) == 0 || n % (x + 1) == 0) {","      return false;","    }","  }","  return true;","}"])+"("+b+")",[d,W.Mb];switch(c){case "EVEN":d=b+" % 2 == 0";break;case "ODD":d=b+" % 2 == 1";break;case "WHOLE":d=b+" % 1 == 0";break;case "POSITIVE":d=b+" > 0";break;case "NEGATIVE":d=b+" < 0";break;case "DIVISIBLE_BY":a=X(a,"DIVISOR",W.sd)||"0",d=b+" % "+a+" == 0"}return[d,W.hh]};
W.math_change=function(a){var b=X(a,"DELTA",W.gh)||"0";a=W.fe.getName(T(a,"VAR"),xj);return a+" = (typeof "+a+" == 'number' ? "+a+" : 0) + "+b+";\n"};W.math_round=W.math_single;W.math_trig=W.math_single;
W.math_on_list=function(a){var b=T(a,"OP");switch(b){case "SUM":a=X(a,"LIST",W.rd)||"[]";a+=".reduce(function(x, y) {return x + y;})";break;case "MIN":a=X(a,"LIST",W.ac)||"[]";a="Math.min.apply(null, "+a+")";break;case "MAX":a=X(a,"LIST",W.ac)||"[]";a="Math.max.apply(null, "+a+")";break;case "AVERAGE":b=Bl("math_mean",["function "+W.$b+"(myList) {","  return myList.reduce(function(x, y) {return x + y;}) / myList.length;","}"]);a=X(a,"LIST",W.Nb)||"[]";a=b+"("+a+")";break;case "MEDIAN":b=Bl("math_median",
["function "+W.$b+"(myList) {","  var localList = myList.filter(function (x) {return typeof x == 'number';});","  if (!localList.length) return null;","  localList.sort(function(a, b) {return b - a;});","  if (localList.length % 2 == 0) {","    return (localList[localList.length / 2 - 1] + localList[localList.length / 2]) / 2;","  } else {","    return localList[(localList.length - 1) / 2];","  }","}"]);a=X(a,"LIST",W.Nb)||"[]";a=b+"("+a+")";break;case "MODE":b=Bl("math_modes",["function "+W.$b+"(values) {",
"  var modes = [];","  var counts = [];","  var maxCount = 0;","  for (var i = 0; i < values.length; i++) {","    var value = values[i];","    var found = false;","    var thisCount;","    for (var j = 0; j < counts.length; j++) {","      if (counts[j][0] === value) {","        thisCount = ++counts[j][1];","        found = true;","        break;","      }","    }","    if (!found) {","      counts.push([value, 1]);","      thisCount = 1;","    }","    maxCount = Math.max(thisCount, maxCount);","  }",
"  for (var j = 0; j < counts.length; j++) {","    if (counts[j][1] == maxCount) {","        modes.push(counts[j][0]);","    }","  }","  return modes;","}"]);a=X(a,"LIST",W.Nb)||"[]";a=b+"("+a+")";break;case "STD_DEV":b=Bl("math_standard_deviation",["function "+W.$b+"(numbers) {","  var n = numbers.length;","  if (!n) return null;","  var mean = numbers.reduce(function(x, y) {return x + y;}) / n;","  var variance = 0;","  for (var j = 0; j < n; j++) {","    variance += Math.pow(numbers[j] - mean, 2);",
"  }","  variance = variance / n;","  return Math.sqrt(variance);","}"]);a=X(a,"LIST",W.Nb)||"[]";a=b+"("+a+")";break;case "RANDOM":b=Bl("math_random_list",["function "+W.$b+"(list) {","  var x = Math.floor(Math.random() * list.length);","  return list[x];","}"]);a=X(a,"LIST",W.Nb)||"[]";a=b+"("+a+")";break;default:throw"Unknown operator: "+b;}return[a,W.Mb]};W.math_modulo=function(a){var b=X(a,"DIVIDEND",W.sd)||"0";a=X(a,"DIVISOR",W.sd)||"0";return[b+" % "+a,W.sd]};
W.math_constrain=function(a){var b=X(a,"VALUE",W.ac)||"0",c=X(a,"LOW",W.ac)||"0";a=X(a,"HIGH",W.ac)||"Infinity";return["Math.min(Math.max("+b+", "+c+"), "+a+")",W.Mb]};W.math_random_int=function(a){var b=X(a,"FROM",W.ac)||"0";a=X(a,"TO",W.ac)||"0";return[Bl("math_random_int",["function "+W.$b+"(a, b) {","  if (a > b) {","    // Swap a and b to ensure a is smaller.","    var c = a;","    a = b;","    b = c;","  }","  return Math.floor(Math.random() * (b - a + 1) + a);","}"])+"("+b+", "+a+")",W.Mb]};
W.math_random_float=function(){return["Math.random()",W.Mb]};/*

 Visual Blocks Language

 Copyright 2012 Google Inc.
 https://developers.google.com/blockly/

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
W.wk={};W.controls_if=function(a){for(var b=0,c=X(a,"IF"+b,W.Nb)||"false",d=zl(a,"DO"+b),e="if ("+c+") {\n"+d+"}",b=1;b<=a.Ba;b++)c=X(a,"IF"+b,W.Nb)||"false",d=zl(a,"DO"+b),e+=" else if ("+c+") {\n"+d+"}";a.Wa&&(d=zl(a,"ELSE"),e+=" else {\n"+d+"}");return e+"\n"};W.logic_compare=function(a){var b={EQ:"==",NEQ:"!=",LT:"<",LTE:"<=",GT:">",GTE:">="}[T(a,"OP")],c="=="==b||"!="==b?W.hh:W.jh,d=X(a,"A",c)||"0";a=X(a,"B",c)||"0";return[d+" "+b+" "+a,c]};
W.logic_operation=function(a){var b="AND"==T(a,"OP")?"&&":"||",c="&&"==b?W.ih:W.dj,d=X(a,"A",c);a=X(a,"B",c);if(d||a){var e="&&"==b?"true":"false";d||(d=e);a||(a=e)}else a=d="false";return[d+" "+b+" "+a,c]};W.logic_negate=function(a){var b=W.cj;return["!"+(X(a,"BOOL",b)||"true"),b]};W.logic_boolean=function(a){return["TRUE"==T(a,"BOOL")?"true":"false",W.me]};W.logic_null=function(){return["null",W.me]};
W.logic_ternary=function(a){var b=X(a,"IF",W.ne)||"false",c=X(a,"THEN",W.ne)||"null";a=X(a,"ELSE",W.ne)||"null";return[b+" ? "+c+" : "+a,W.ne]};/*

 Visual Blocks Editor

 Copyright 2012 Google Inc.
 https://developers.google.com/blockly/

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
R.wk={};
R.controls_if={o:function(){this.J="https://github.com/google/blockly/wiki/IfElse";this.C(210);S(U(this,"IF0").P("Boolean"),"se");S(nj(this,3,"DO0"),"fa\u00e7a");ii(this,!0);ji(this,!0);pj(this);var a=this;this.B(function(){if(a.Ba||a.Wa){if(!a.Ba&&a.Wa)return"Se um valor \u00e9 verdadeiro, ent\u00e3o realize o primeiro bloco de instru\u00e7\u00f5es.  Sen\u00e3o, realize o segundo bloco de instru\u00e7\u00f5es.";if(a.Ba&&!a.Wa)return"Se o primeiro valor \u00e9 verdadeiro, ent\u00e3o realize o primeiro bloco de instru\u00e7\u00f5es.  Sen\u00e3o, se o segundo valor \u00e9 verdadeiro, realize o segundo bloco de instru\u00e7\u00f5es.";if(a.Ba&&
a.Wa)return"Se o primeiro valor \u00e9 verdadeiro, ent\u00e3o realize o primeiro bloco de instru\u00e7\u00f5es.  Sen\u00e3o, se o segundo valor \u00e9 verdadeiro, realize o segundo bloco de instru\u00e7\u00f5es.  Se nenhum dos blocos for verdadeiro, realize o \u00faltimo bloco de instru\u00e7\u00f5es."}else return"Se um valor \u00e9 verdadeiro, ent\u00e3o realize algumas instru\u00e7\u00f5es.";return""});this.Wa=this.Ba=0},Ze:function(){if(!this.Ba&&!this.Wa)return null;var a=document.createElement("mutation");
this.Ba&&a.setAttribute("elseif",this.Ba);this.Wa&&a.setAttribute("else",1);return a},Wf:function(a){this.Ba=parseInt(a.getAttribute("elseif"),10);this.Wa=parseInt(a.getAttribute("else"),10);for(a=1;a<=this.Ba;a++)S(U(this,"IF"+a).P("Boolean"),"sen\u00e3o se"),S(nj(this,3,"DO"+a),"fa\u00e7a");this.Wa&&S(nj(this,3,"ELSE"),"sen\u00e3o")},Lj:function(a){var b=dd(a,"controls_if_if");ed(b);for(var c=ld(b,"STACK").p,d=1;d<=this.Ba;d++){var e=dd(a,"controls_if_elseif");ed(e);md(c,e.F);c=e.D}this.Wa&&(a=
dd(a,"controls_if_else"),ed(a),md(c,a.F));return b},zj:function(a){this.Wa&&mj(this,"ELSE");this.Wa=0;for(var b=this.Ba;0<b;b--)mj(this,"IF"+b),mj(this,"DO"+b);this.Ba=0;for(a=oj(a,"STACK");a;){switch(a.type){case "controls_if_elseif":this.Ba++;var b=S(U(this,"IF"+this.Ba).P("Boolean"),"sen\u00e3o se"),c=nj(this,3,"DO"+this.Ba);S(c,"fa\u00e7a");a.Wi&&md(b.p,a.Wi);a.Zd&&md(c.p,a.Zd);break;case "controls_if_else":this.Wa++;b=nj(this,3,"ELSE");S(b,"sen\u00e3o");a.Zd&&md(b.p,a.Zd);break;default:throw"Unknown block type.";
}a=a.D&&E(a.D)}},Ig:function(a){a=oj(a,"STACK");for(var b=1;a;){switch(a.type){case "controls_if_elseif":var c=ld(this,"IF"+b),d=ld(this,"DO"+b);a.Wi=c&&c.p.t;a.Zd=d&&d.p.t;b++;break;case "controls_if_else":d=ld(this,"ELSE");a.Zd=d&&d.p.t;break;default:throw"Unknown block type.";}a=a.D&&E(a.D)}}};R.controls_if_if={o:function(){this.C(210);S(lj(this),"se");nj(this,3,"STACK");this.B("Acrescente, remova ou reordene se\u00e7\u00f5es para reconfigurar este bloco.");this.contextMenu=!1}};
R.controls_if_elseif={o:function(){this.C(210);S(lj(this),"sen\u00e3o se");ii(this,!0);ji(this,!0);this.B("Acrescente uma condi\u00e7\u00e3o para o bloco se.");this.contextMenu=!1}};R.controls_if_else={o:function(){this.C(210);S(lj(this),"sen\u00e3o");ii(this,!0);this.B("Acrescente uma condi\u00e7\u00e3o final para o bloco se.");this.contextMenu=!1}};
R.logic_compare={o:function(){var a=C?[["=","EQ"],["\u2260","NEQ"],[">","LT"],["\u2265","LTE"],["<","GT"],["\u2264","GTE"]]:[["=","EQ"],["\u2260","NEQ"],["<","LT"],["\u2264","LTE"],[">","GT"],["\u2265","GTE"]];this.J="https://pt.wikipedia.org/wiki/Inequa%C3%A7%C3%A3o";this.C(210);Q(this,"Boolean");U(this,"A");S(U(this,"B"),new P(a),"OP");fd(this,!0);var b=this;this.B(function(){return{EQ:"Retorna verdadeiro se ambas as entradas forem iguais.",NEQ:"Retorna verdadeiro se ambas as entradas forem diferentes.",
LT:"Retorna verdadeiro se a primeira entrada for menor que a segunda entrada.",LTE:"Retorna verdadeiro se a primeira entrada for menor ou igual \u00e0 segunda entrada.",GT:"Retorna verdadeiro se a primeira entrada for maior que a segunda entrada.",GTE:"Retorna verdadeiro se a primeira entrada for maior ou igual \u00e0 segunda entrada."}[T(b,"OP")]})}};
R.logic_operation={o:function(){this.J="https://github.com/google/blockly/wiki/Logic#logical-operations";this.C(210);Q(this,"Boolean");U(this,"A").P("Boolean");S(U(this,"B").P("Boolean"),new P([["e","AND"],["ou","OR"]]),"OP");fd(this,!0);var a=this;this.B(function(){return{AND:"Retorna verdadeiro se ambas as entradas forem verdadeiras.",OR:"Retorna verdadeiro se uma das estradas for verdadeira."}[T(a,"OP")]})}};
R.logic_negate={o:function(){this.J="https://github.com/google/blockly/wiki/Logic#not";this.C(210);Q(this,"Boolean");this.vb("n\u00e3o %1",["BOOL","Boolean",1],1);this.B("Retorna verdadeiro se a entrada for falsa.  Retorna falsa se a entrada for verdadeira.")}};R.logic_boolean={o:function(){this.J="https://github.com/google/blockly/wiki/Logic#values";this.C(210);Q(this,"Boolean");S(lj(this),new P([["verdadeiro","TRUE"],["falso","FALSE"]]),"BOOL");this.B("Retorna verdadeiro ou falso.")}};
R.logic_null={o:function(){this.J="https://en.wikipedia.org/wiki/Nullable_type";this.C(210);Q(this);S(lj(this),"nulo");this.B("Retorna nulo.")}};R.logic_ternary={o:function(){this.J="https://en.wikipedia.org/wiki/%3F:";this.C(210);S(U(this,"IF").P("Boolean"),"teste");S(U(this,"THEN"),"se verdadeiro");S(U(this,"ELSE"),"se falso");Q(this);this.B('Avalia a condi\u00e7\u00e3o em "teste". Se a condi\u00e7\u00e3o for verdadeira retorna o valor "se verdadeiro", sen\u00e3o retorna o valor "se falso".')}};/*

 Visual Blocks Editor

 Copyright 2012 Google Inc.
 https://developers.google.com/blockly/

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
R.xk={};R.math_number={o:function(){this.J="https://pt.wikipedia.org/wiki/N%C3%BAmero";this.C(230);S(lj(this),new pl("0",ul),"NUM");Q(this,"Number");this.B("Um n\u00famero.")}};
R.math_arithmetic={o:function(){this.J="https://pt.wikipedia.org/wiki/Aritm%C3%A9tica";this.C(230);Q(this,"Number");U(this,"A").P("Number");S(U(this,"B").P("Number"),new P([["+","ADD"],["-","MINUS"],["\u00d7","MULTIPLY"],["\u00f7","DIVIDE"],["^","POWER"]]),"OP");fd(this,!0);var a=this;this.B(function(){return{ADD:"Retorna a soma dos dois n\u00fameros.",MINUS:"Retorna a diferen\u00e7a entre os dois n\u00fameros.",MULTIPLY:"Retorna o produto dos dois n\u00fameros.",DIVIDE:"Retorna o quociente da divis\u00e3o dos dois n\u00fameros.",
POWER:"Retorna o primeiro n\u00famero elevado \u00e0 pot\u00eancia do segundo n\u00famero."}[T(a,"OP")]})}};
R.math_single={o:function(){this.J="https://pt.wikipedia.org/wiki/Raiz_quadrada";this.C(230);Q(this,"Number");this.vb("%1 %2",["OP",new P([["raiz quadrada","ROOT"],["absoluto","ABS"],["-","NEG"],["ln","LN"],["log10","LOG10"],["e^","EXP"],["10^","POW10"]])],["NUM","Number",1],1);var a=this;this.B(function(){return{ROOT:"Retorna a raiz quadrada de um n\u00famero.",ABS:"Retorna o valor absoluto de um n\u00famero.",NEG:"Retorna o oposto de um n\u00famero.",LN:"Retorna o logaritmo natural de um n\u00famero.",
LOG10:"Retorna o logaritmo em base 10 de um n\u00famero.",EXP:"Retorna o n\u00famero e elevado \u00e0 pot\u00eancia de um n\u00famero.",POW10:"Retorna 10 elevado \u00e0 pot\u00eancia de um n\u00famero."}[T(a,"OP")]})}};
R.math_trig={o:function(){this.J="https://pt.wikipedia.org/wiki/Fun%C3%A7%C3%A3o_trigonom%C3%A9trica";this.C(230);Q(this,"Number");S(U(this,"NUM").P("Number"),new P([["sin","SIN"],["cos","COS"],["tan","TAN"],["asin","ASIN"],["acos","ACOS"],["atan","ATAN"]]),"OP");var a=this;this.B(function(){return{SIN:"Retorna o seno de um grau (n\u00e3o radiano).",COS:"Retorna o cosseno de um grau (n\u00e3o radiano).",TAN:"Retorna a tangente de um grau (n\u00e3o radiano).",ASIN:"Retorna o arco seno de um n\u00famero.",
ACOS:"Retorna o arco cosseno de um n\u00famero.",ATAN:"Retorna o arco tangente de um n\u00famero."}[T(a,"OP")]})}};R.math_constant={o:function(){this.J="https://pt.wikipedia.org/wiki/Anexo:Lista_de_constantes_matem%C3%A1ticas";this.C(230);Q(this,"Number");S(lj(this),new P([["\u03c0","PI"],["e","E"],["\u03c6","GOLDEN_RATIO"],["sqrt(2)","SQRT2"],["sqrt(\u00bd)","SQRT1_2"],["\u221e","INFINITY"]]),"CONSTANT");this.B("Retorna uma das constantes comuns: \u03c0 (3.141\u2026), e (2.718\u2026), \u03c6 (1.618\u2026), sqrt(2) (1.414\u2026), sqrt(\u00bd) (0.707\u2026), ou \u221e (infinito).")}};
R.math_number_property={o:function(){this.C(230);U(this,"NUMBER_TO_CHECK").P("Number");var a=new P([["\u00e9 par","EVEN"],["\u00e9 \u00edmpar","ODD"],["\u00e9 primo","PRIME"],["\u00e9 inteiro","WHOLE"],["\u00e9 positivo","POSITIVE"],["\u00e9 negativo","NEGATIVE"],["\u00e9 divis\u00edvel por","DIVISIBLE_BY"]],function(a){this.j.Vi("DIVISIBLE_BY"==a)});S(lj(this),a,"PROPERTY");fd(this,!0);Q(this,"Boolean");this.B("Verifica se um n\u00famero \u00e9 par, \u00edmpar, inteiro, positivo, negativo, ou se \u00e9 divis\u00edvel por outro n\u00famero.  Retorna verdadeiro ou falso.")},
Ze:function(){var a=document.createElement("mutation"),b="DIVISIBLE_BY"==T(this,"PROPERTY");a.setAttribute("divisor_input",b);return a},Wf:function(a){a="true"==a.getAttribute("divisor_input");this.Vi(a)},Vi:function(a){var b=ld(this,"DIVISOR");a?b||U(this,"DIVISOR").P("Number"):b&&mj(this,"DIVISOR")}};
R.math_change={o:function(){this.J="https://pt.wikipedia.org/wiki/Adi%C3%A7%C3%A3o";this.C(230);this.vb("alterar %1 por %2",["VAR",new Ak("item")],["DELTA","Number",1],1);ii(this,!0);ji(this,!0);var a=this;this.B(function(){return'Soma um n\u00famero \u00e0 vari\u00e1vel "%1".'.replace("%1",T(a,"VAR"))})},$h:function(){return[T(this,"VAR")]},Rk:function(a,b){var c=T(this,"VAR");a.toLowerCase()==c.toLowerCase()&&kd(this,"VAR").bb(b)}};
R.math_round={o:function(){this.J="https://pt.wikipedia.org/wiki/Arredondamento";this.C(230);Q(this,"Number");S(U(this,"NUM").P("Number"),new P([["arredonda","ROUND"],["arredonda para cima","ROUNDUP"],["arredonda para baixo","ROUNDDOWN"]]),"OP");this.B("Arredonda um n\u00famero para cima ou para baixo.")}};
R.math_on_list={o:function(){var a=this;this.J="";this.C(230);Q(this,"Number");var b=new P([["soma de uma lista","SUM"],["menor da lista","MIN"],["maior da lista","MAX"],["m\u00e9dia da lista","AVERAGE"],["mediana da lista","MEDIAN"],["moda da lista","MODE"],["desvio padr\u00e3o da lista","STD_DEV"],["item aleat\u00f3rio da lista","RANDOM"]],function(b){"MODE"==b?a.K.P("Array"):a.K.P("Number")});S(U(this,"LIST").P("Array"),b,"OP");this.B(function(){return{SUM:"Retorna a soma de todos os n\u00fameros na lista.",
MIN:"Retorna o menor n\u00famero da lista.",MAX:"Retorna o maior n\u00famero da lista.",AVERAGE:"Retorna a m\u00e9dia aritm\u00e9tica dos n\u00fameros da lista.",MEDIAN:"Retorna a mediana dos n\u00fameros da lista.",MODE:"Retorna uma lista do(s) item(ns) mais comum(ns) da lista.",STD_DEV:"Retorna o desvio padr\u00e3o dos n\u00fameros da lista.",RANDOM:"Retorna um elemento aleat\u00f3rio da lista."}[T(a,"OP")]})}};
R.math_modulo={o:function(){this.J="https://pt.wikipedia.org/wiki/Opera%C3%A7%C3%A3o_m%C3%B3dulo";this.C(230);Q(this,"Number");this.vb("resto da divis\u00e3o de %1 \u00f7 %2",["DIVIDEND","Number",1],["DIVISOR","Number",1],1);fd(this,!0);this.B("Retorna o resto da divis\u00e3o de dois n\u00fameros.")}};
R.math_constrain={o:function(){this.J="https://en.wikipedia.org/wiki/Clamping_%28graphics%29";this.C(230);Q(this,"Number");this.vb("restringe %1 inferior %2 superior %3",["VALUE","Number",1],["LOW","Number",1],["HIGH","Number",1],1);fd(this,!0);this.B("Restringe um n\u00famero entre os limites especificados (inclusivo).")}};
R.math_random_int={o:function(){this.J="https://pt.wikipedia.org/wiki/Gerador_de_n%C3%BAmeros_pseudoaleat%C3%B3rios";this.C(230);Q(this,"Number");this.vb("inteiro aleat\u00f3rio entre %1 e %2",["FROM","Number",1],["TO","Number",1],1);fd(this,!0);this.B("Retorna um n\u00famero inteiro entre os dois limites informados, inclusivo.")}};
R.math_random_float={o:function(){this.J="https://pt.wikipedia.org/wiki/Gerador_de_n%C3%BAmeros_pseudoaleat%C3%B3rios";this.C(230);Q(this,"Number");S(lj(this),"fra\u00e7\u00e3o aleat\u00f3ria");this.B("Retorna uma fra\u00e7\u00e3o aleat\u00f3ria entre 0.0 (inclusivo) e 1.0 (exclusivo).")}};R.bird_noWorm={o:function(){this.C(330);S(lj(this),lm("Bird_noWorm"));Q(this,"Boolean");this.B(lm("Bird_noWormTooltip"))}};W.bird_noWorm=function(){return["noWorm()",W.Mb]};R.bird_heading={o:function(){this.C(290);S(S(lj(this),lm("Bird_heading")),new Sl("90"),"ANGLE");ii(this,!0);this.B(lm("Bird_headingTooltip"))}};W.bird_heading=function(a){return"heading("+parseFloat(T(a,"ANGLE"))+", 'block_id_"+a.id+"');\n"};
R.bird_position={o:function(){this.C(330);S(lj(this),new P([["x","X"],["y","Y"]]),"XY");Q(this,"Number");this.B(lm("Bird_positionTooltip"))}};W.bird_position=function(a){return["get"+T(a,"XY").charAt(0)+"()",W.Mb]};
R.bird_compare={o:function(){this.J="https://pt.wikipedia.org/wiki/Inequa%C3%A7%C3%A3o";var a=C?[[">","LT"],["<","GT"]]:[["<","LT"],[">","GT"]];this.C(210);Q(this,"Boolean");U(this,"A").P("Number");S(U(this,"B").P("Number"),new P(a),"OP");fd(this,!0);var b=this;this.B(function(){return{ul:"Retorna verdadeiro se a primeira entrada for menor que a segunda entrada.",sl:"Retorna verdadeiro se a primeira entrada for maior que a segunda entrada."}[T(b,"OP")]})}};
W.bird_compare=function(a){var b="LT"==T(a,"OP")?"<":">",c=W.jh,d=X(a,"A",c)||"0";a=X(a,"B",c)||"0";return[d+" "+b+" "+a,c]};R.bird_and={o:function(){this.J="https://github.com/google/blockly/wiki/Logic#logical-operations";this.C(210);Q(this,"Boolean");U(this,"A").P("Boolean");S(U(this,"B").P("Boolean"),"and","AND");fd(this,!0);this.B("Retorna verdadeiro se ambas as entradas forem verdadeiras.")}};
W.bird_and=function(a){var b=W.ih,c=X(a,"A",b);a=X(a,"B",b);c||a?(c||(c="true"),a||(a="true")):a=c="false";return[c+" && "+a,b]};R.bird_ifElse={o:function(){this.J="https://github.com/google/blockly/wiki/IfElse";this.C(210);S(U(this,"CONDITION"),"se").P("Boolean");S(nj(this,3,"DO"),"fa\u00e7a");S(nj(this,3,"ELSE"),"sen\u00e3o");hd(this,!1);this.B("Se um valor \u00e9 verdadeiro, ent\u00e3o realize o primeiro bloco de instru\u00e7\u00f5es.  Sen\u00e3o, realize o segundo bloco de instru\u00e7\u00f5es.")}};
W.bird_ifElse=function(a){var b=X(a,"CONDITION",W.Nb)||"false",c=zl(a,"DO");a=zl(a,"ELSE");return"if ("+b+") {\n"+c+"} else {\n"+a+"}\n"};R.controls_if.Dk=R.controls_if.o;R.controls_if.o=function(){this.Dk();ii(this,!1);ji(this,!1);hd(this,!1)};function $(a,b,c,d){this.je=a;this.ke=b;this.x1=c;this.y1=d}$.prototype.clone=function(){return new $(this.je,this.ke,this.x1,this.y1)};var km="bird";Y.vg=function(){10>B?window.location=window.location.protocol+"//"+window.location.host+window.location.pathname+"?lang="+Cc+"&level="+(B+1):Y.Oe()};
var nm=[void 0,{start:new x(20,20),Jb:90,eb:new x(50,50),Ya:new x(80,80),Ha:[]},{start:new x(20,20),Jb:0,eb:new x(80,20),Ya:new x(80,80),Ha:[new $(0,50,60,50)]},{start:new x(20,70),Jb:270,eb:new x(50,20),Ya:new x(80,70),Ha:[new $(50,50,50,100)]},{start:new x(20,80),Jb:0,eb:new x(50,80),Ya:new x(80,20),Ha:[new $(0,0,65,65)]},{start:new x(80,80),Jb:270,eb:new x(50,20),Ya:new x(20,20),Ha:[new $(0,100,65,35)]},{start:new x(20,40),Jb:0,eb:new x(80,20),Ya:new x(20,80),Ha:[new $(0,59,50,59)]},{start:new x(80,
80),Jb:180,eb:new x(80,20),Ya:new x(20,20),Ha:[new $(0,70,40,70),new $(70,50,100,50)]},{start:new x(20,25),Jb:90,eb:new x(80,25),Ya:new x(80,75),Ha:[new $(50,0,50,25),new $(75,50,100,50)]},{start:new x(80,70),Jb:180,eb:new x(20,20),Ya:new x(80,20),Ha:[new $(0,69,31,100),new $(40,50,71,0),new $(80,50,100,50)]},{start:new x(20,20),Jb:90,eb:new x(80,50),Ya:new x(20,20),Ha:[new $(40,60,60,60),new $(40,60,60,30),new $(60,30,100,30)]}][B],om=[],pm=1;
function qm(){var a=document.getElementById("svgBird");nm.Ha.push(new $(-5,-5,-5,105));nm.Ha.push(new $(-5,105,105,105));nm.Ha.push(new $(105,105,105,-5));nm.Ha.push(new $(105,-5,-5,-5));for(var b=0;b<nm.Ha.length;b++){var c=nm.Ha[b],d=document.createElementNS("http://www.w3.org/2000/svg","line");d.setAttribute("x1",c.je/100*400);d.setAttribute("y1",400*(1-c.ke/100));d.setAttribute("x2",c.x1/100*400);d.setAttribute("y2",400*(1-c.y1/100));d.setAttribute("stroke","#CCB");d.setAttribute("stroke-width",
10);d.setAttribute("stroke-linecap","round");a.appendChild(d)}b=document.createElementNS("http://www.w3.org/2000/svg","image");b.setAttribute("id","nest");b.setAttributeNS("http://www.w3.org/1999/xlink","xlink:href","lib/blockly/bird/nest.png");b.setAttribute("height",100);b.setAttribute("width",100);a.appendChild(b);b=document.createElementNS("http://www.w3.org/2000/svg","image");b.setAttribute("id","worm");b.setAttributeNS("http://www.w3.org/1999/xlink","xlink:href","lib/blockly/bird/worm.png");b.setAttribute("height",
100);b.setAttribute("width",100);a.appendChild(b);b=document.createElementNS("http://www.w3.org/2000/svg","clipPath");b.setAttribute("id","birdClipPath");c=document.createElementNS("http://www.w3.org/2000/svg","rect");c.setAttribute("id","clipRect");c.setAttribute("width",120);c.setAttribute("height",120);b.appendChild(c);a.appendChild(b);b=document.createElementNS("http://www.w3.org/2000/svg","image");b.setAttribute("id","bird");b.setAttributeNS("http://www.w3.org/1999/xlink","xlink:href","lib/blockly/bird/birds-120.png");
b.setAttribute("height",480);b.setAttribute("width",1440);b.setAttribute("clip-path","url(#birdClipPath)");a.appendChild(b);b=document.createElementNS("http://www.w3.org/2000/svg","rect");b.setAttribute("class","edges");b.setAttribute("width",400);b.setAttribute("height",400);a.appendChild(b);for(var b=3<B,c=4<B,d=1,e=.1;.9>e;e+=.1){if(b){var f=document.createElementNS("http://www.w3.org/2000/svg","line");f.setAttribute("class","edges");f.setAttribute("x1",400*e);f.setAttribute("y1",400);f.setAttribute("x2",
400*e);f.setAttribute("y2",400-9*d);a.appendChild(f)}c&&(f=document.createElementNS("http://www.w3.org/2000/svg","line"),f.setAttribute("class","edges"),f.setAttribute("x1",0),f.setAttribute("y1",400*e),f.setAttribute("x2",9*d),f.setAttribute("y2",400*e),a.appendChild(f));2==d&&(b&&(f=document.createElementNS("http://www.w3.org/2000/svg","text"),f.setAttribute("class","edgeX"),f.setAttribute("x",400*e+2),f.setAttribute("y",396),f.appendChild(document.createTextNode(Math.round(100*e))),a.appendChild(f)),
c&&(f=document.createElementNS("http://www.w3.org/2000/svg","text"),f.setAttribute("class","edgeY"),f.setAttribute("x",3),f.setAttribute("y",400*e-2),f.appendChild(document.createTextNode(Math.round(100-100*e))),a.appendChild(f)));d=1==d?2:1}}
window.addEventListener("load",function(){function a(){c.style.top=Math.max(10,d.offsetTop-window.pageYOffset)+"px";c.style.left=b?"10px":"420px";c.style.width=window.innerWidth-440+"px"}document.body.innerHTML=Bc();Y.o();var b=-1!=dm.indexOf(Cc),c=document.getElementById("blockly"),d=document.getElementById("visualization");window.addEventListener("scroll",function(){a();De(window,"resize")});window.addEventListener("resize",a);a();var e=document.getElementById("toolbox");El(document.getElementById("blockly"),
{media:"lib/blockly/media/",rtl:b,toolbox:e,trashcan:!0});Pl(["lib/blockly/bird/quack.ogg","lib/blockly/bird/quack.mp3"],"quack");Pl(["lib/blockly/bird/whack.mp3","lib/blockly/bird/whack.ogg"],"whack");Pl(["lib/blockly/bird/worm.mp3","lib/blockly/bird/worm.ogg"],"worm");Al("noWorm,heading,getX,getY");qm();e="";e=1==B?'<xml>  <block type="bird_heading" x="70" y="70"></block></xml>':5>B?'<xml>  <block type="bird_ifElse" x="70" y="70"></block></xml>':'<xml>  <block type="controls_if" x="70" y="70"></block></xml>';Y.vk(e,!1);rm();mm("runButton",sm);mm("resetButton",tm);setTimeout(function(){Kl(function(){um()});
um()},5E3);8<B&&setTimeout(Z.nh,3E5);setTimeout(Y.lk,1);setTimeout(Y.nk,1)});
function um(){if(0==Be&&vc.result!=vm&&!im(B)){var a=Zc(Vc(y)),b=Wc(y.qa.s,!0),c=document.getElementById("dialogHelp"),d=null,e=null;if(1==B){if(-1!=a.indexOf(">90<")||-1==a.indexOf("bird_heading"))e={width:"370px",top:"140px"},e[C?"right":"left"]="215px",d=Wc(y,!0),d=d.length?O(d[0]):O(b[0])}else if(2==B)-1==a.indexOf("bird_noWorm")&&(e={width:"350px",top:"170px"},e[C?"right":"left"]="180px",d=O(b[1]));else if(4==B)-1==a.indexOf("bird_compare")&&(e={width:"350px",top:"230px"},e[C?"right":"left"]=
"180px",d=O(b[2]));else if(5==B){if(-1==a.indexOf("mutation else")){d=Wc(y,!1);for(e=0;(b=d[e])&&"controls_if"!=b.type;e++);b.wb.A()?(c=document.getElementById("dialogMutatorHelp"),d=b.wb.qa.se[1],a=$g(d),e={width:"340px",top:a.y+60+"px"},e.left=a.x-(C?310:0)+"px"):(a=$g(O(b)),e={width:"340px",top:a.y+100+"px"},e.left=a.x-(C?350:0)+"px",d=O(b))}}else if(6==B){if(-1==a.indexOf("mutation")){d=Wc(y,!1);for(e=0;(b=d[e])&&"controls_if"!=b.type;e++);a=$g(O(b));e={width:"350px",top:a.y+220+"px"};e.left=
a.x-(C?350:0)+"px";d=O(b)}}else 8==B&&-1==a.indexOf("bird_and")&&(e={width:"350px",top:"360px"},e[C?"right":"left"]="450px",d=O(b[4]));e?c.parentNode!=document.getElementById("dialog")&&Z.rf(c,d,!0,!1,e,null):Z.Eb(!1)}}
function rm(){for(var a=0;a<om.length;a++)window.clearTimeout(om[a]);om=[];A=nm.start.clone();yc=xc=nm.Jb;zc=!1;pm=1;wm();a=document.getElementById("worm");a.setAttribute("x",nm.eb.x/100*400-50);a.setAttribute("y",400*(1-nm.eb.y/100)-50);a.style.visibility="visible";a=document.getElementById("nest");a.setAttribute("x",nm.Ya.x/100*400-50);a.setAttribute("y",400*(1-nm.Ya.y/100)-50)}
function sm(a){if(!Y.Cb(a)){a=document.getElementById("runButton");var b=document.getElementById("resetButton");b.style.minWidth||(b.style.minWidth=a.offsetWidth+"px");a.style.display="none";b.style.display="inline";ze(y,!0);rm();xm()}}function tm(a){Y.Cb(a)||(document.getElementById("runButton").style.display="inline",document.getElementById("resetButton").style.display="none",ze(y,!1),rm())}var vm=1;
function ym(a,b){var c;c=function(a,b){var c=a.valueOf(),h=b.toString(),k=c*Math.PI/180;A.x+=Math.cos(k);A.y+=Math.sin(k);xc=c;Ac.push(["move",A.x,A.y,xc,h]);if(zc&&15>Xb(A,nm.Ya))throw Ac.push(["play","quack",null]),zm(nm.Ya),Ac.push(["finish",null]),!0;!zc&&15>Xb(A,nm.eb)&&(zm(nm.eb),Ac.push(["worm",null]),Ac.push(["play","worm",null]),zc=!0);a:{for(c=0;h=nm.Ha[c];c++){var k=A,m=void 0;k instanceof x?(m=k.y,k=k.x):m=void 0;var p=h.je,q=h.ke,u=h.x1-h.je,t=h.y1-h.ke,k=Math.min(Math.max(((k-p)*(h.x1-
p)+(m-q)*(h.y1-q))/(u*u+t*t),0),1),m=h.je,p=h.ke;if(6>Xb(new x(m+k*(h.x1-m),p+k*(h.y1-p)),A)){c=!0;break a}}c=!1}if(c)throw Ac.push(["play","whack",null]),!1;};a.setProperty(b,"heading",a.createNativeFunction(c));a.setProperty(b,"noWorm",a.createNativeFunction(function(){return a.createPrimitive(!zc)}));a.setProperty(b,"getX",a.createNativeFunction(function(){return a.createPrimitive(A.x)}));a.setProperty(b,"getY",a.createNativeFunction(function(){return a.createPrimitive(A.y)}))}
function xm(){if("Interpreter"in window){Ac=[];var a=wl(),b=a.indexOf("if ("),c=a.indexOf("}\n");-1!=b&&-1!=c&&(a=a.substring(b,c+2));b=0;a=new Interpreter("while(true) {\n"+a+"}",ym);try{for(c=1E5;a.step();)if(0==c--)throw Infinity;b=-1}catch(d){Infinity===d?b=2:!0===d?b=vm:!1===d?b=-2:(b=-2,window.alert(d))}wc=b==vm?10:15;rm();om.push(setTimeout(Am,1))}else setTimeout(xm,250)}
function Am(){om=[];var a=Ac.shift();a?(Y.Le(a.pop()),"move"==a[0]||"goto"==a[0]?(A.x=a[1],A.y=a[2],xc=a[3],pm="move"==a[0]?2:1,wm()):"worm"==a[0]?document.getElementById("worm").style.visibility="hidden":"finish"==a[0]?(pm=3,wm(),Y.Wk(),Z.Aj()):"play"==a[0]&&Qi(a[1],.5),om.push(setTimeout(Am,5*wc))):Y.Le(null)}
function wm(){var a;a=Wb(yc)-Wb(xc);180<a?a-=360:-180>=a&&(a=360+a);10>=Math.abs(a)?yc=xc:(yc-=10*(0==a?0:0>a?-1:1),yc=Wb(yc));var b=(14-Math.round(yc/360*12))%12,c=yc%30;15<=c&&(c-=30);var c=-1*c,d;if(1==pm)d=0;else if(3==pm)d=3;else if(2==pm)d=Math.round(Date.now()/100)%3;else throw"Unknown pose.";a=A.x/100*400-60;var e=400*(1-A.y/100)-60,f=document.getElementById("bird");f.setAttribute("x",a-120*b);f.setAttribute("y",e-120*d);f.setAttribute("transform","rotate("+c+", "+(a+60)+", "+(e+60)+")");
b=document.getElementById("clipRect");b.setAttribute("x",a);b.setAttribute("y",e)}function zm(a){var b=Math.round(Xb(A,a));a=Wb(180*Math.atan2(a.y-A.y,a.x-A.x)/Math.PI);for(var c=a*Math.PI/180,d=0;d<b;d++)A.x+=Math.cos(c),A.y+=Math.sin(c),Ac.push(["goto",A.x,A.y,a,null])};
